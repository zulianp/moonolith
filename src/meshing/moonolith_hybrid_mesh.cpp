#include "moonolith_hybrid_mesh.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_box.hpp"
#include "moonolith_dual_graph.hpp"
#include "moonolith_intersection_2.hpp"

#include "moonolith_matrix_io.hpp"
#include "moonolith_eps_canvas.hpp"
#include "moonolith_func_to_color.hpp"

#include <unordered_map>
#include <algorithm>
#include <cmath>

namespace moonolith {

	std::vector<Real> asd(const std::vector<Real> &polygon)
	{
		std::vector<Real> res(2);
		res[0]=0;
		res[1]=0;

		const Integer n_vertices = polygon.size()/2;
		assert(n_vertices>2);

		for(Integer i=0; i<n_vertices; ++i){
			res[0]+=polygon[2*i];
			res[1]+=polygon[2*i+1];
		}

		res[0] /= n_vertices;
		res[1] /= n_vertices;

		return res;
	}


	static void copy_points(const Mesh &mesh, const Integer element_handle, std::vector<Real> &points)
	{
		const Integer n_nodes = mesh.n_nodes(element_handle);
		const Integer dim = mesh.dim;

		const Integer * e = mesh.element(element_handle);

		points.resize(n_nodes * dim);

		for(Integer k = 0; k < n_nodes; ++k) {
			const Real * p_ptr = mesh.point(e[k]);
			std::copy(p_ptr, p_ptr + n_nodes, points.begin() + k * dim);
		}
	}

	static void build_boxes(const Mesh &mesh, std::vector<Box> &el_boxes, Box &mesh_box)
	{
		const Integer dim = mesh.dim;
		const Integer n_elements = mesh.n_elements();

		el_boxes.resize(n_elements);
		mesh_box.reset(dim);
		std::vector<Real> p(dim, 0);

		for(Integer i = 0; i < n_elements; ++i) {
			el_boxes[i].reset(dim);

			const Integer n_nodes = mesh.n_nodes(i);
			const Integer *e = mesh.element(i);

			for(Integer k = 0; k < n_nodes; ++k) {
				const Real * p_ptr = mesh.point(e[k]);
				std::copy(p_ptr, p_ptr + n_nodes, p.begin());

				el_boxes[i] += p;
				mesh_box += el_boxes[i];
			}
		}
	}

	class Intersection {
	public:
		Integer element_handle;
		std::vector<Real> points;
		// std::vector<Integer> neighs;
	};

	static void make_polygon_from_axis_aligned_box(const std::vector<Real> &box_min, const std::vector<Real> &box_max, std::vector<Real> &polygon)
	{
		polygon.resize(4 * 2);

		//p1
		polygon[0] = box_min[0];
		polygon[1] = box_min[1];

		//p2
		polygon[2] = box_max[0];
		polygon[3] = box_min[1];

		//p3
		polygon[4] = box_max[0];
		polygon[5] = box_max[1];

		//p4
		polygon[6] = box_min[0];
		polygon[7] = box_max[1];
	}

	static void make_polygon_from_hash_grid_cell(const HashGrid &hash_grid, const Integer hash, std::vector<Real> &polygon)
	{
		std::vector<Real> cell_min, cell_max;
		std::vector<Integer> tensor_index;
		hash_grid.hash_to_tensor_index(hash, tensor_index);
		hash_grid.coord(tensor_index, cell_min);

		std::transform(tensor_index.begin(), 
			tensor_index.end(), 
			tensor_index.begin(),
			[](const Integer val) -> Integer { return val + 1; });

		hash_grid.coord(tensor_index, cell_max);

		make_polygon_from_axis_aligned_box(cell_min, cell_max, polygon);
	}


	//static 
	void draw_grid_function(const Box &box, 
		const std::vector<Integer> &dims, 
		const std::vector<Real> &fun, 
		EPSCanvas &canvas)
	{
		assert(dims.size() == 2);

		std::vector<Real> h(2);
		h[0] = (box.get_max(0) - box.get_min(0))/(dims[0] - 0);
		h[1] = (box.get_max(1) - box.get_min(1))/(dims[1] - 0);

		std::vector<Real> fire;
		func_to_fire(fun, fire);

		std::vector<Real> polygon(4*2);
		for(Integer i = 0; i < dims[0]; ++i) {
			for(Integer j = 0; j < dims[1]; ++j) {
				const Integer hash = i * dims[1] + j;

				//p1
				polygon[0] = box.get_min(0) + i 	* h[0];
				polygon[1] = box.get_min(1) + j 	* h[1];

				//p2
				polygon[2] = box.get_min(0) + (i+1) * h[0];
				polygon[3] = box.get_min(1) + j 	* h[1];

				//p3
				polygon[4] = box.get_min(0) + (i+1) * h[0];
				polygon[5] = box.get_min(1) + (j+1) * h[1];

				//p4
				polygon[6] = box.get_min(0) + i 	* h[0];
				polygon[7] = box.get_min(1) + (j+1) * h[1];

				canvas.set_color(fire[hash*3], fire[hash*3+1], fire[hash*3+2]);
				canvas.fill_polygon(polygon);
			}
		}
	}




	bool HybridMesh::build(const std::vector<Integer> &dims, const Mesh &surface)
	{

		static const bool draw_mesh = true;
		std::vector<Box> el_boxes;
		Box mesh_box;

		build_boxes(surface, el_boxes, mesh_box);
		hash_grid.reset(mesh_box, dims);

		std::vector< std::vector<Integer> > el_to_cell_hashes;
		if(!hash_grid.find_intersecting_cells(el_boxes, el_to_cell_hashes)) {
			return false;
		}

		std::unordered_map<Integer, std::shared_ptr< std::vector<Intersection> > > intersections;

		IntersectConvexPolygonsWork work;
		work.reserve(2);

		std::vector<Integer> tensor_index;
		tensor_index.reserve(dims.size());

		EPSCanvas canvas;
		
		std::vector<Real> line, isect, cell, cell_min, cell_max;
		for(std::size_t i = 0; i < el_to_cell_hashes.size(); ++i) {
			if(el_to_cell_hashes[i].empty()) continue;

			const Integer element_handle = i;

			copy_points(surface, element_handle, line);

			if(draw_mesh) {
				canvas.set_line_width(0.03);
				canvas.set_color(0.5, 1.0, 0);
				canvas.draw_line(line[0], line[1], line[2], line[3]);
				canvas.stroke();
			}

			const auto &cell_hashes = el_to_cell_hashes[i];
			for(std::size_t k = 0; k < cell_hashes.size(); ++k) {
				const Integer hash = cell_hashes[k];

				make_polygon_from_hash_grid_cell(hash_grid, hash, cell);

				if(!clip_polygon(line, cell, isect, 1e-8, work)) {
					continue;
				}

				if(draw_mesh) {
					canvas.set_line_width(0.02);
					canvas.set_color(1, 0.5, 0);
					canvas.stroke_polygon(cell);
					auto bary = asd(cell);
					canvas.draw_text(bary[0],bary[1],0.5,"Courier", std::to_string(hash));
				}

				auto &i_ptr = intersections[hash];
				if(!i_ptr) {
					i_ptr = std::make_shared< std::vector<Intersection> >();
				}

				i_ptr->push_back({element_handle, isect});


				if(draw_mesh) {
					canvas.set_line_width(0.01);
					canvas.set_color(0.0,.0, 1.);
					if(isect.size() == 2) {
						canvas.draw_circle(isect[0], isect[1], 0.5);
					} else {
						for(std::size_t i = 0; i < isect.size()/2; ++i) {
							Integer ip1 = (i + 1) % (isect.size()/2);
							canvas.draw_line(isect[i*2], isect[i*2+1], isect[ip1*2], isect[ip1*2+1]);
						}
					}

					canvas.stroke();
				}
			}
		}



		// DualGraph dg;
		// dg.init_unordered(surface);
		HashGrid cell_grid;
		Box cell_box(surface.dim);
		std::vector<Integer> cell_grid_dims(surface.dim);
		std::fill(cell_grid_dims.begin(), cell_grid_dims.end(), 50);

		
		std::vector<Box> isect_boxes;
		std::vector< std::vector<Integer> > cell_grid_hashes;
		std::vector<Real> n(surface.dim);


		std::vector<Real> inside_grid;

		const Integer n_samples = 10;
		const Real delta_samples = 1.0/n_samples;

		for(auto it : intersections) {
			const Integer hash = it.first;

			EPSCanvas cell_canvas;
			bool has_intersection = false;

			hash_grid.hash_to_tensor_index(hash, tensor_index);
			hash_grid.coord(tensor_index, cell_box.get_min());

			std::transform(tensor_index.begin(), 
				tensor_index.end(), 
				tensor_index.begin(),
				[](const Integer val) -> Integer { return val + 1; });

			hash_grid.coord(tensor_index, cell_box.get_max());
			cell_grid.reset(cell_box, cell_grid_dims);

			inside_grid.resize(cell_grid.n_cells());
			std::fill(inside_grid.begin(), inside_grid.end(), 0.0);

			const auto &i_vec = *it.second;

			isect_boxes.clear();
			isect_boxes.reserve(i_vec.size());

			// canvas.set_color(0, 0, 0);
			for(auto &i_vec_it : i_vec) {
				const Integer current_element = i_vec_it.element_handle;
				isect_boxes.push_back(el_boxes[current_element]);
				
				// for(Integer k = 0; k < surface.n_nodes(current_element); ++k)
				// {
				// 	const Real *p = surface.point(el[k]);
				// 	const Real *n = surface.normal(normals[k]);

				// 	canvas.stroke_line(p[0],p[1],p[0]+n[0],p[1]+n[1]);
				// }

			}



			

			

			if(hash == 342)
			{
				std::cout<<"bla"<<std::endl;
			}

			make_polygon_from_hash_grid_cell(hash_grid, hash, cell);
			Integer flags = 0;
			Integer mask = 1;

			for(std::size_t i = 0; i < cell.size()/2; ++i) //loop over facets
			{
				const Integer ip1 = (i+1 >= cell.size()/2) ? 0 : i+1;

				const Real cix = cell[2*i];
				const Real ciy = cell[2*i+1];

				const Real cip1x = cell[2*ip1];
				const Real cip1y = cell[2*ip1+1];

				bool facet_inside = false;

				for(Integer j = 0; j<=n_samples; ++j) //sample facet
				{
					const Real t = j * delta_samples;

					const Real sx = (1-t)*cix + t*cip1x;
					const Real sy = (1-t)*ciy + t*cip1y;

					for(auto &i_vec_it : i_vec) //loop over intersecting elements
					{
						const Integer current_element = i_vec_it.element_handle;

						const Integer *normals = surface.normal_indices(current_element);
						const Integer *el = surface.element(current_element);

						for(Integer k = 0; k < surface.n_nodes(current_element); ++k) //loop over nodes of the intersecting element
						{
							const Real *p = surface.point(el[k]);
							const Real *n = surface.normal(normals[k]);

							const Real dx = sx - p[0]; //segment from element to sample
							const Real dy = sy - p[1];

							if(n[0]*dx+n[1]*dy < 0) //sample point is inside therefore all cell edges is pointing inside, todo add check if projection is inside element
							{
								facet_inside = true;
								break;
							}
						}

						if(facet_inside) //since for an intersecting element this is inside, useless to check others
							break;
					}

					if(facet_inside) //useless to check other sample points
						break;
				}

				if(!facet_inside)
					flags = flags | mask;

				mask = mask << 1;
			}

			mask = 1;
			canvas.set_color(0, 1, 0);

			for(std::size_t i = 0; i < cell.size()/2; ++i)
			{
				if(flags & mask)
				{
					const Integer ip1 = (i+1 >= cell.size()/2) ? 0 : i+1;
					
					canvas.stroke_line(cell[2*i], cell[2*i+1], cell[2*ip1], cell[2*ip1+1]);
				}

				mask = mask << 1;
			}






			/////////////////////////////////////////////////
			//////////////////////////Geodesic distance for concave
			std::vector< std::vector<Integer> > cell_grid_hashes;
			if(!cell_grid.find_intersecting_cells(isect_boxes, cell_grid_hashes)) {
				assert(false && "should never happen");
				std::cerr << "[Error] no Intersection found in inside/outside test grid" << std::endl;
				continue;
			}

			assert(isect_boxes.size() == i_vec.size());
			assert(isect_boxes.size() == cell_grid_hashes.size());

			for(std::size_t i = 0; i < cell_grid_hashes.size(); ++i) {
				copy_points(surface, i_vec[i].element_handle, line);

				n[0] = line[3] - line[1];
				n[1] = -(line[2] - line[0]);

				Real norm_n = sqrt(n[0] * n[0] + n[1] * n[1]);
				
				n[0] /= norm_n;
				n[1] /= norm_n;

				const Real d = n[0] * line[0] + n[1] * line[1];

				for(std::size_t j = 0; j < cell_grid_hashes[i].size(); ++j) {
					make_polygon_from_hash_grid_cell(cell_grid, cell_grid_hashes[i][j], cell);	

					if(polygon_intersects_plane(n, d, cell)) {
						const Integer hash_ij = cell_grid_hashes[i][j];
						assert(hash_ij < static_cast<Integer>(inside_grid.size()));
						inside_grid[hash_ij] = 1.;
						has_intersection = true;
					}
				}
			}

			if(draw_mesh) {
				canvas.write("hybrid_mesh.eps");
			}

			// if(draw_mesh && has_intersection) {
			// 	draw_grid_function(cell_box, cell_grid_dims, inside_grid, cell_canvas);
			// 	cell_canvas.write("cell_" + std::to_string(hash) + ".eps");
			// }
		}

		return true;
	}
}
