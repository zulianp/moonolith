#ifndef MOONOLITH_TETRAHEDRALIZER_HPP
#define MOONOLITH_TETRAHEDRALIZER_HPP


#include "moonolith_mesh.hpp"


namespace moonolith {
	struct TetrahedralizerOptions
	{
		bool allow_surface_refinement;
		bool preserve_edges;
		bool convex_hull;
		
		double maximum_volume;
		int allowed_steiner_points;

		bool mark_bounday;

		bool verbose;


		TetrahedralizerOptions(const bool convex_hull=false, const bool preserve_edges = false)
		{
			allow_surface_refinement = true;
			this->preserve_edges = preserve_edges;

			maximum_volume=-1;
			allowed_steiner_points=-1;

			mark_bounday = false;

			this->convex_hull=convex_hull;

			verbose=false;
		}
	};

	


	void tetrahedralize(const Mesh &in, Mesh &out, const TetrahedralizerOptions &opts = TetrahedralizerOptions(true, true));
	// void tetrahedralize(const Mesh &in, const std::vector<Real> &points, Mesh &out, const TetrahedralizerOptions &opts = TetrahedralizerOptions());
	void tetrahedralize_points(const std::vector<Real> &boundary_points, Mesh &out, const TetrahedralizerOptions &opts = TetrahedralizerOptions(true, false));
	// void tetrahedralize_points(const std::vector<Real> &boundary_points, const std::vector<Real> &points, Mesh &out, const TetrahedralizerOptions &opts = TetrahedralizerOptions(true));
}


#endif //MOONOLITH_TETRAHEDRALIZER_HPP
