#ifndef MOONOLITH_TRIANGULATOR_HPP
#define MOONOLITH_TRIANGULATOR_HPP

#include "moonolith_mesh.hpp"

namespace moonolith
{
	
	struct TriangulatorOptions { 

		bool allow_boundary_refinement;
		bool allow_edge_refinement;
		bool constrain_angle;
		bool convex_hull;
		bool add_third_dimension;
		double maximum_area;
		int allowed_steiner_points;

		bool verbose;

		TriangulatorOptions(const bool convex_hull=false)
		{
			add_third_dimension = false;

			allow_boundary_refinement=true;
			allow_edge_refinement=true;

			constrain_angle=true;

			maximum_area=-1;
			allowed_steiner_points=-1;

			this->convex_hull=convex_hull;

			verbose=false;
		}
	};


	void triangulate(const std::vector<Real> &polygon, Mesh &mesh, const TriangulatorOptions &opts = TriangulatorOptions());
	void triangulate(const std::vector<Real> &polygon, const std::vector<Real> &points, Mesh &mesh, const TriangulatorOptions &opts = TriangulatorOptions());
	void triangulate_points(const std::vector<Real> &boundary_points, Mesh &mesh, const TriangulatorOptions &opts = TriangulatorOptions(true));
	void triangulate_points(const std::vector<Real> &boundary_points, const std::vector<Real> &points, Mesh &mesh, const TriangulatorOptions &opts = TriangulatorOptions(true));

}
#endif // MOONOLITH_TRIANGULATOR_HPP
