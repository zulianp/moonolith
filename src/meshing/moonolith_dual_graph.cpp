#include "moonolith_dual_graph.hpp"
#include "moonolith_mesh.hpp"

namespace moonolith {
	bool DualGraph::init_unordered(const Mesh &mesh)
	{
		const Integer n_nodes = mesh.n_nodes();
		const Integer n_elements = mesh.n_elements();
		Integer el_index_size = 0;

		std::vector< std::vector< Integer> > node_2_element(n_nodes);

		el_ptr.resize(n_elements + 1);
		el_ptr[0] = 0;

		for(Integer i = 0; i < n_elements; ++i) {
			const Integer n_nodes = mesh.n_nodes(i);
			const Integer n_sides = mesh.n_sides(i);

			el_ptr[i+1] = el_ptr[i] + n_sides;
			el_index_size += n_sides;

			const Integer * e = mesh.element(i);
			for(Integer k = 0; k < n_nodes; ++k) {
				node_2_element[e[k]].push_back(i);
			}
		}

		el_index.resize(el_index_size);
		std::fill(el_index.begin(), el_index.end(), invalid_index<Integer>::value());
		std::vector<Integer> el_offset(n_elements, 0);

		for(Integer i = 0; i < n_nodes; ++i) {
			const auto &elements = node_2_element[i];

			for(std::size_t e_i = 0; e_i < elements.size(); ++e_i) {
				const Integer e = elements[e_i];

				for(std::size_t e_i_adj = 0; e_i_adj < elements.size(); ++e_i_adj) {
					if(e_i == e_i_adj) continue;

					const Integer e_adj = elements[e_i_adj];

					bool must_add = true;

					for(Integer k = 0; k < el_offset[e]; ++k) {
						const Integer k_e = el_ptr[e] + k; 
						if(e_adj == el_index[k_e]) {
							must_add = false;
						}
					}

					if(must_add) {
						el_index[el_ptr[e] + el_offset[e]] = e_adj;
						++el_offset[e];
					}
				}
			}
		}

		return true;
	}

	bool DualGraph::are_adjacent(const Integer i, const Integer j) const
	{
		for(Integer k = el_ptr[i]; k < el_ptr[i+1]; ++k) {
			if(el_index[k] == j) return true;
		}

		return false;
	}

	void DualGraph::describe(std::ostream &os) const
	{
		const Integer n_elements = el_ptr.size() - 1;
		os << "\nn_elements: " << n_elements << std::endl;

		for(Integer i = 0; i < n_elements; ++i) {
			for(Integer k = el_ptr[i]; k < el_ptr[i + 1]; ++k) {
				if(el_index[k] == invalid_index<Integer>::value()) {
					os << "_ ";
				} else {
					os << el_index[k] << " ";
				}
			}

			os << "\n";
		}
	}
}
