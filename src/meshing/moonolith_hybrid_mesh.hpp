#ifndef MOONOLITH_HYBRID_MESH_HPP
#define MOONOLITH_HYBRID_MESH_HPP 

#include "moonolith_mesh.hpp"
#include "moonolith_hash_grid.hpp"

#include <vector>

namespace moonolith {

	/**
	 * @brief a mixed structured-polygonal mesh
	 */
	class HybridMesh {
	public:
		bool build(const std::vector<Integer> &dims, const Mesh &surface);

		class Cell {
		public:
			Integer begin_hash, end_hash;
		};

		HashGrid hash_grid;
		Mesh boundary_mesh;
		std::vector<Cell> cells;
	};
}

#endif //MOONOLITH_HYBRID_MESH_HPP
