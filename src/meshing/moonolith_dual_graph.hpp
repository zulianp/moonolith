#ifndef MOONOLITH_DUAL_GRAPH_HPP
#define MOONOLITH_DUAL_GRAPH_HPP 

#include <iostream>
#include <vector>

namespace moonolith {
	class Mesh;

	class DualGraph {
	public:
		inline DualGraph() {}
		bool init_unordered(const Mesh &mesh);
		void describe(std::ostream &os = std::cout) const;

		bool are_adjacent(const Integer i, const Integer j) const;

		std::vector<Integer> el_ptr;
		std::vector<Integer> el_index;
	};
}

#endif //MOONOLITH_DUAL_GRAPH_HPP
