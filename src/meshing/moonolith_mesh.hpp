#ifndef MOONOLITH_MESH_HPP
#define MOONOLITH_MESH_HPP 

#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>

namespace moonolith {

	//Enum from libmesh
	enum ElemType { 
		EDGE2 = 0, 
		EDGE3, 
		EDGE4, 
		TRI3, 
		TRI6, 
		QUAD4, 
		QUAD8, 
		QUAD9, 
		TET4, 
		TET10, 
		HEX8, 
		HEX20, 
		HEX27, 
		PRISM6, 
		PRISM15, 
		PRISM18, 
		PYRAMID5, 
		PYRAMID13, 
		PYRAMID14, 
		INFEDGE2, 
		// INFQUAD4, INFQUAD6, INFHEX8, INFHEX16, 
		// INFHEX18, INFPRISM6, INFPRISM12, NODEELEM, 
		// REMOTEELEM, TRI3SUBDIVISION, 
		// TRISHELL3, 
		// QUADSHELL4, 
		POLYGON,
		POLYHEDRON,
		INVALID_ELEM 
	};

	template<typename T>
	struct invalid_index {
		inline static constexpr int value()
		{
			return std::numeric_limits<T>::max();
		}
	};

	template<>
	struct invalid_index<int> {
		inline static constexpr int value()
		{
			return -1;
		}
	};

	template<>
	struct invalid_index<long> {
		inline static constexpr int value()
		{
			return -1;
		}
	};


	class SideSet {
	public:
		Integer tag;
		std::vector<Integer> element;
		std::vector<Integer> side_index;

		void describe(std::ostream &os = std::cout) const;

		SideSet()
		: tag(0) {}


		bool operator== (const SideSet &other) const
		{
			return 
			other.tag == this->tag &&
			other.element == this->element &&
			other.side_index == this->side_index;

		}
	};

	/**
	 * @brief Basic Mesh class with basic fields such as points and elements,
	 * and additional information for mesh-formats such as .obj, .e (exodus-ii), etc..
	 */
	 class Mesh {
	 public:
	 	Integer add_node(const Real x, const Real y);
	 	Integer add_node(const Real x, const Real y, const Real z);

	 	Integer add_normal(const Real x, const Real y);
	 	Integer add_normal(const Real x, const Real y, const Real z);
	 	Integer add_texture(const Real u, const Real v);

		//TODO add default type (for lazy users ;-))
	 	Integer add_element(const std::vector<Integer> &nodes, const ElemType type);

	 	Integer add_element(
	 		const std::vector<Integer> &nodes,
	 		const std::vector<Integer> &normals,
	 		const std::vector<Integer> &textures,
	 		const ElemType type);

	 	inline const Integer * element(const Integer handle) const
	 	{
	 		return &el_index[el_ptr[handle]];
	 	}

	 	inline const Integer * normal_indices(const Integer handle) const
	 	{
	 		return &normal_index[el_ptr[handle]];
	 	}

	 	inline Real * point(const Integer node)
	 	{
	 		return &points[node * dim];
	 	}

	 	inline const Real * point(const Integer node) const
	 	{
	 		return &points[node * dim];
	 	}

	 	inline Real * normal(const Integer node)
	 	{
	 		return &normals[node * dim];
	 	}

	 	inline const Real * normal(const Integer node) const
	 	{
	 		return &normals[node * dim];
	 	}

	 	inline Integer n_nodes(const Integer handle) const
	 	{
	 		return el_ptr[handle + 1] - el_ptr[handle]; 
	 	}

	 	inline Integer n_nodes() const
	 	{
	 		if(points.empty()) return 0;

	 		assert(dim > 0);

	 		return points.size() / dim;
	 	}

	 	Integer n_sides(const Integer element_handle) const;

	 	inline Integer n_normals() const
	 	{
	 		return normals.size() / dim;
	 	}

	 	inline Integer n_textures() const
	 	{
	 		return textures.size() / 2;
	 	}

	 	inline Integer n_elements() const
	 	{
	 		return el_ptr.size() - 1;
	 	}

	 	inline Integer n_side_sets() const
	 	{
	 		return side_sets.size();
	 	}

	 	inline bool has_node_sets() const
	 	{
	 		return !node_sets.empty();
	 	}


	 	inline bool valid() const
	 	{
	 		return dim > 0 && n_nodes() > 2 && n_elements() > 0;
	 	}

	 	bool operator== (const Mesh &other) const;


	 	void describe(std::ostream &os) const;

	 	inline friend std::ostream &operator << (std::ostream &os, const Mesh &mesh)
	 	{
	 		mesh.describe(os);
	 		return os;
	 	}

	 	void set_uniform_elem_type_flags();

	 	Mesh();

	 	void clear();
		///////////////////////////////////////////////
		//geometric data
		///////////////////////////////////////////////

	 	Integer dim;
	 	std::vector<Real> points;
	 	std::vector<Real> normals;
	 	std::vector<Real> textures;

		///////////////////////////////////////////////
		//connectivity in compressed storage
		///////////////////////////////////////////////

		///can be skipped for triangulations
	 	std::vector<Integer> el_ptr;

	 	std::vector<Integer> el_index;
	 	std::vector<Integer> normal_index;
	 	std::vector<Integer> texture_index;

		///////////////////////////////////////////////
		// finite-elements
		///////////////////////////////////////////////

	 	std::vector<Integer> elem_type;
	 	std::vector<Integer> blocks;
	 	std::vector<SideSet> side_sets;
	 	std::vector<Integer> node_sets;


		///////////////////////////////////////////////
		// meta
		///////////////////////////////////////////////

		// Allows to identify if the mesh has a unique element type
		// which enables certain assumptions, algorithms, and optimzations
		// or assertion checking
	 	bool has_uniform_elem_type;
	 	Integer uniform_elem_type;
	 };
	}

#endif //MOONOLITH_MESH_HPP
