#include "moonolith_mesh.hpp"
#include <assert.h>
#include <iomanip>

namespace moonolith {
	void SideSet::describe(std::ostream &os) const
	{
		os << "tag: " << tag << "\n";
		os << "element -> side:\n";

		assert(element.size() == side_index.size());

		for(std::size_t i = 0; i < element.size(); ++i) {
			os << element[i] << " -> " << side_index[i] << "\n";
		}
	}

	Mesh::Mesh()
	: dim(0), has_uniform_elem_type(true), uniform_elem_type(ElemType::INVALID_ELEM)
	{}

	Integer Mesh::add_node(const Real x, const Real y)
	{
		assert(dim == 0 || dim == 2);
		dim = 2;
		
		const Integer handle = points.size() / 2;
		
		points.push_back(x);
		points.push_back(y);

		return handle;
	}

	Integer Mesh::add_node(const Real x, const Real y, const Real z)
	{
		assert(dim == 0 || dim == 3);
		dim = 3;
		
		const Integer handle = points.size() / 3;
		
		points.push_back(x);
		points.push_back(y);
		points.push_back(z);

		return handle;
	}

	Integer Mesh::add_normal(const Real x, const Real y)
	{
		assert(dim == 0 || dim == 2);	
		const Integer handle = normals.size() / 2;
		
		normals.push_back(x);
		normals.push_back(y);
		
		return handle;
	}


	Integer Mesh::add_normal(const Real x, const Real y, const Real z)
	{
		assert(dim == 0 || dim == 3);

		const Integer handle = normals.size() / 3;

		normals.push_back(x);
		normals.push_back(y);
		normals.push_back(z);

		return handle;
	}

	Integer Mesh::add_texture(const Real u, const Real v)
	{
		const Integer handle = textures.size() / 2;
		
		textures.push_back(u);
		textures.push_back(v);

		return handle;
	}

	Integer Mesh::add_element(const std::vector<Integer> &nodes, const ElemType type)
	{
		if(el_ptr.empty()) {
			el_ptr.push_back(0);
		}

		const Integer handle = el_ptr.size() - 1;

		el_ptr.push_back(el_ptr.back() + nodes.size());
		el_index.insert(el_index.end(), nodes.begin(), nodes.end());
		
		elem_type.push_back(type);

		assert(elem_type.size() == el_ptr.size() - 1);
		return handle;
	}

	Integer Mesh::add_element(const std::vector<Integer> &nodes, const std::vector<Integer> &normals, const std::vector<Integer> &textures, const ElemType type)
	{
		const Integer handle = add_element(nodes, type);

		if(!normals.empty())
		{
			assert(normals.size()==nodes.size());
			normal_index.insert(normal_index.end(), normals.begin(), normals.end());
		}


		if(!textures.empty())
		{
			assert(textures.size()==nodes.size());
			texture_index.insert(texture_index.end(), textures.begin(), textures.end());
		}

		return handle;
	}


	Integer Mesh::n_sides(const Integer element_handle) const
	{
		switch(elem_type[element_handle])
		{
			case ElemType::EDGE2: { return 2; }
			case ElemType::EDGE3: { return 2; }
			case ElemType::EDGE4: { return 2; }
			case ElemType::TRI3: { return 3; }
			case ElemType::TRI6: { return 3; }
			case ElemType::QUAD4: { return 4; }
			case ElemType::QUAD8: { return 4; }
			case ElemType::QUAD9: { return 4; }
			case ElemType::TET4: { return 4; }
			case ElemType::TET10: { return 4; }
			case ElemType::HEX8: { return 6; }
			case ElemType::HEX20: { return 6; }
			case ElemType::HEX27: { return 6; }
			case ElemType::PRISM6: { return 5; }
			case ElemType::PRISM15: { return 5; }
			case ElemType::PRISM18: { return 5; }
			case ElemType::PYRAMID5: { return 5; }
			case ElemType::PYRAMID13: { return 5; }
			case ElemType::PYRAMID14: { return 5; }
			case ElemType::POLYGON: { return n_nodes(element_handle); }
			default: {
				assert(false && "unsupported element");
				std::cerr << "unsupported element: " << element_handle << std::endl;
				return -1;
			}
		}
	}

	void Mesh::set_uniform_elem_type_flags()
	{
		if(!elem_type.empty()) {
			auto e_it = elem_type.begin();
			
			uniform_elem_type = *e_it++;
			has_uniform_elem_type = (uniform_elem_type != ElemType::INVALID_ELEM);
			const Integer n_nodes_x_el = n_nodes(0);

			if(!has_uniform_elem_type) return;

			for(; e_it != elem_type.end(); ++e_it) {
				if(uniform_elem_type != *e_it) {
					has_uniform_elem_type = false;
					uniform_elem_type = ElemType::INVALID_ELEM;
					return;
				} else if(uniform_elem_type == ElemType::POLYGON || 
						  uniform_elem_type == ElemType::POLYHEDRON) {

					if(n_nodes_x_el != n_nodes(std::distance(elem_type.begin(), e_it))) {
						has_uniform_elem_type = false;
						uniform_elem_type = ElemType::INVALID_ELEM;
						return;
					}
				}
			}
		} else {
			has_uniform_elem_type = false;
			uniform_elem_type = ElemType::INVALID_ELEM;
		}
	}

	void Mesh::clear()
	{
		dim = 0;
		points.clear();
		normals.clear();
		textures.clear();
		
		el_ptr.clear();

		el_index.clear();
		normal_index.clear();
		texture_index.clear();

		elem_type.clear();
		blocks.clear();
		side_sets.clear();
		node_sets.clear();
	}

	bool Mesh::operator== (const Mesh &other) const
	{
		return 
			other.dim == this->dim &&
			other.points == this->points &&
			other.normals == this->normals &&
			other.textures == this->textures &&
			other.el_ptr == this->el_ptr &&
			other.el_index == this->el_index &&
			other.normal_index == this->normal_index &&
			other.texture_index == this->texture_index &&
			other.elem_type == this->elem_type &&
			other.blocks == this->blocks &&
			other.side_sets == this->side_sets &&
			other.node_sets == this->node_sets;
	}

	void Mesh::describe(std::ostream &os) const
	{
		static const int n_fills = 18;
		os << "\n================================\n";
		os << "                Mesh\n";
		os << "-------------------------------\n";
		os << std::internal << std::setfill(' ');

		os << std::setw(n_fills) << "dim: "			<< dim 									<< "\n";
		os << std::setw(n_fills) << "n points: "	<< (points.size()/dim)					<< "\n";
		os << std::setw(n_fills) << "n elements: "	<< n_elements()							<< "\n";

		os << "-------------------------------\n";
		os << std::setw(n_fills) << "n normals: "	<< n_normals()							<< "\n";
		os << std::setw(n_fills) << "n textures: " 	<< n_textures()		  					<< "\n";

		os << "--------------------------------\n";
		os << std::setw(n_fills) << "n side sets: " << n_side_sets()						<< "\n";
		os << std::setw(n_fills) << "has node sets: " << (has_node_sets()? "yes" : "no" ) 	<< "\n";

		os << "================================\n";

	}
}
