#include "moonolith_tetrahedralizer.hpp"

#include "tetgen.hpp"

#include <sstream>


namespace moonolith {

	void assign_values(const std::vector<Real> &boundary_points, const TetrahedralizerOptions &opts, tetgen::tetgenio &in)
	{
		const Integer n_points = boundary_points.size() / 3;
		assert(std::size_t(n_points * 3) == boundary_points.size());

		in.numberofpoints = n_points;
		in.pointlist = new Real[in.numberofpoints * 3];

		if(opts.mark_bounday)
			in.pointmarkerlist=new int[in.numberofpoints];

		for(std::size_t i = 0; i < boundary_points.size(); ++i) {
			in.pointlist[i] = boundary_points[i];
		}
	}

	void assign_values(const Mesh &mesh, const TetrahedralizerOptions &opts, tetgen::tetgenio &in)
	{
		const Integer n_points = mesh.n_nodes();
		const Integer n_faces  = mesh.n_elements();

		in.numberofpoints = n_points;
		in.pointlist = new double[in.numberofpoints*3];

		if(opts.mark_bounday)
			in.pointmarkerlist=new int[in.numberofpoints];

		in.numberoffacets = n_faces;
		in.facetlist = new tetgen::tetgenio::facet[in.numberoffacets];

		if(opts.mark_bounday)
			in.facetmarkerlist=new int[in.numberoffacets];

		int index=0;
		for(Integer i = 0; i<n_points; ++i)
		{
			const Real *p=mesh.point(i);

			for(Integer k=0; k<mesh.dim; ++k){
				in.pointlist[index*3+k] = p[k];
			}

			if(opts.mark_bounday)
			{
				in.pointmarkerlist[index]=1; //always assume bounday
			}

			++index;
		}

		//TODO?
		// if(points.columns()>0)
		// {
		// 	strategy->setUse_addin(true);
		// 	strategy->setNumberOfAdditionalPoints(points.columns());
		// 	int index=0;
		// 	for(Integer i=0;i<points.columns();++i)
		// 	{
		// 		const Matrix p=points.col(i);
		// 		strategy->setAddotionalPointAt(index, p[0], p[1], p[2],true);
		// 		++index;
		// 	}
		// }

		index=0;
		for(Integer i = 0; i < n_faces; ++i)
		{
			const Integer *face = mesh.element(i);
			const Integer n_nodes = mesh.n_nodes(i);

			tetgen::tetgenio::facet &f = in.facetlist[index];
			f.numberofpolygons=1;
			f.polygonlist = new tetgen::tetgenio::polygon[f.numberofpolygons];
			f.numberofholes = 0;
			f.holelist = NULL;

			tetgen::tetgenio::polygon &p=f.polygonlist[0];
			p.numberofvertices=n_nodes;
			p.vertexlist=new int[p.numberofvertices];

			for(Integer j=0; j < n_nodes; ++j)
				p.vertexlist[j]=face[j];


			if(opts.mark_bounday)
				in.facetmarkerlist[index]=1; //always assume bounday

			++index;
		}
	}


	void tetrahedralize(tetgen::tetgenio &in, const TetrahedralizerOptions &opts, tetgen::tetgenio &out)
	{
		//http://wias-berlin.de/software/tetgen/1.5/doc/manual/manual005.html#sec36
		std::stringstream buf;
		buf.precision(100);
		buf.setf(std::ios::fixed, std::ios::floatfield);

		if(!opts.verbose)
			buf<<"Q";
		else
			buf<<"V";

		buf<< "zT0.000000000001";

		if(opts.convex_hull)
			buf<<"p";

		if(!opts.allow_surface_refinement)
			buf<<"Y";

		if(opts.allowed_steiner_points >= 0)
			buf<<"S"<<opts.allowed_steiner_points;

		if(opts.maximum_volume>0)
			buf<< "qa" << opts.maximum_volume;

		if(opts.preserve_edges)
			buf<<"M";

		// char * swichtes = "QzT0.000000000001V";
		// char * swichtes = "zpq1.414a0.1V";

		char* str = new char[buf.str().size() + 1];
		strcpy(str, buf.str().c_str());

		try {
			tetgen::tetrahedralize(str, &in, &out, NULL);
		} catch(const std::exception &e) {
			std::cerr << e.what() << std::endl;
			delete[] str;
		} catch(const int) { //messages are delivered by tetgen
			delete[] str;
		}

		delete[] str;
	}

	void fill_mesh(const tetgen::tetgenio &out, Mesh &mesh)
	{
		assert(out.numberoftetrahedra > 0);
		assert(out.numberofcorners == 4);

		mesh.clear();
		mesh.dim = 3;

		mesh.points.resize(out.numberofpoints*3);
		std::copy(out.pointlist, out.pointlist + out.numberofpoints*3, mesh.points.begin());

		std::vector<Integer> nodes(out.numberofcorners);
		for(Integer i = 0; i < out.numberoftetrahedra; ++i) 
		{
			const Integer offset = i * out.numberofcorners;
			
			for(Integer j = 0; j < out.numberofcorners; ++j) {
				nodes[j] = out.tetrahedronlist[offset+j];
			}

			mesh.add_element(nodes, ElemType::TET4);
		}
	}

	void tetrahedralize(const Mesh &in_mesh, Mesh &out_mesh, const TetrahedralizerOptions &opts)
	{
		tetgen::tetgenio in, out;

		assert(in_mesh.dim == 3);

		assign_values(in_mesh, opts, in);
		tetrahedralize(in, opts, out);
		fill_mesh(out, out_mesh);
	}

	void tetrahedralize_points(const std::vector<Real> &boundary_points, Mesh &out_mesh, const TetrahedralizerOptions &opts)
	{
		tetgen::tetgenio in, out;

		assign_values(boundary_points, opts, in);
		tetrahedralize(in, opts, out);
		fill_mesh(out, out_mesh);
	}
}

