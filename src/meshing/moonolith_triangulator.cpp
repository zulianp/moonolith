#include "moonolith_triangulator.hpp"

#include "triangle.hpp"

#include <sstream>
#include <cstring>

namespace moonolith {
	static const double SQRT3_OVER_4 = 0.43301270189222;
	typedef struct trianglelib::triangulateio triangulateio;

	void free_in(triangulateio &in)
	{
		free(in.pointlist);
		free(in.pointmarkerlist);

		free(in.segmentlist);
		free(in.segmentmarkerlist);
	}

	void free_out(triangulateio &out)
	{
		free(out.pointlist);
		free(out.pointmarkerlist);

		free(out.trianglelist);
		free(out.triangleattributelist);

		free(out.segmentlist);
		free(out.segmentmarkerlist);

		free(out.edgelist);
		free(out.edgemarkerlist);

	}

	void fill_mesh(triangulateio &triangulation, const TriangulatorOptions &opts, Mesh &mesh)
	{
		mesh.clear();
		mesh.dim = opts.add_third_dimension ? 3 : 2;

		mesh.points.resize(triangulation.numberofpoints * mesh.dim);
		mesh.node_sets.resize(triangulation.numberofpoints);

		for (int i = 0; i < triangulation.numberofpoints; ++i) { 
			mesh.points[i*mesh.dim + 0] = triangulation.pointlist[i * 2 ];
			mesh.points[i*mesh.dim + 1] = triangulation.pointlist[i * 2 + 1];
			
			if(opts.add_third_dimension) {
				mesh.points[i*mesh.dim + 2] = 0;
			}

			mesh.node_sets[i] = triangulation.pointmarkerlist[i]; //1 for boundary nodes
		}

		std::vector<Integer> face(3);
		for(int i=0;i<triangulation.numberoftriangles;++i)
		{
			face[0] = triangulation.trianglelist[i * 3];
			face[1] = triangulation.trianglelist[i * 3+1];
			face[2] = triangulation.trianglelist[i * 3+2];

			mesh.add_element(face, ElemType::TRI3);
		}

		mesh.uniform_elem_type = ElemType::TRI3;
		mesh.has_uniform_elem_type = true;
	}



	void triangulate_values(triangulateio &mid, triangulateio &out, const TriangulatorOptions &opts)
	{
		out.pointlist = (double *) NULL;
		out.pointmarkerlist = (int *) NULL;

		out.trianglelist = (int *) NULL;
		out.triangleattributelist = (double *) NULL;

		out.segmentlist = (int *) NULL;
		out.segmentmarkerlist = (int *) NULL;

		out.edgelist=(int *) NULL;
		out.edgemarkerlist = (int *) NULL;

		std::stringstream buf;
		buf.precision(100);
		buf.setf(std::ios::fixed, std::ios::floatfield);

		if(!opts.verbose)
			buf<<"Q";
		else
			buf<<"V";

		buf<< "ez";

		if(!opts.allow_edge_refinement)
			buf<<"YY";
		else if(!opts.allow_boundary_refinement)
			buf<<"Y";

		if(opts.constrain_angle)
			buf<<"q";

		if(!opts.convex_hull)
			buf<<"p";


		if(opts.allowed_steiner_points>=0)
			buf<<"S"<<opts.allowed_steiner_points;

		if(opts.maximum_area>0)
			buf<< "a" << opts.maximum_area*opts.maximum_area*SQRT3_OVER_4;


		char* str = new char[buf.str().size() + 1];
		strcpy(str, buf.str().c_str());

		trianglelib::triangulate(str, &mid, &out, (triangulateio *) NULL);

		delete[] str;
	}

	
	void assign_values(const std::vector<Real> &boundaries, const std::vector<Real> &points, const bool is_polygon, triangulateio &in)
	{
		const int n_boundaries = boundaries.size()/2;
		const int n_points=n_boundaries + points.size()/2;
		const int n_edges=is_polygon?n_boundaries:0; //TODO add internal edges

		in.numberofpoints = n_points;
		
		in.numberofpointattributes = 0;
		in.pointattributelist = NULL;

		in.pointlist = (double *) malloc(in.numberofpoints * 2 * sizeof(double));
		in.pointmarkerlist = (int *) malloc(in.numberofpoints * sizeof(int));


		in.numberofsegments = n_edges;
		in.segmentlist = (int *) malloc(in.numberofsegments * 2 * sizeof(int));
		in.segmentmarkerlist = (int *) malloc(in.numberofsegments * sizeof(int));


		for(int i=0;i<n_boundaries;++i)
		{
			in.pointlist[2*i]=boundaries[2*i];
			in.pointlist[2*i+1]=boundaries[2*i+1];

			in.pointmarkerlist[i] = 1;

			if(is_polygon)
			{
				in.segmentmarkerlist[i] = 1; //1 because the edge and the point are boundary

				in.segmentlist[2*i]=i;

				if(i==n_boundaries-1)
					in.segmentlist[2*i+1]=0;
				else
					in.segmentlist[2*i+1]=i+1;
			}
		}

		const int points_offset = n_boundaries + points.size()/2;
		for(int i = n_boundaries;i < points_offset; ++i)
		{
			in.pointlist[2*i]=points[(i-n_boundaries)*2];
			in.pointlist[2*i+1]=points[(i-n_boundaries)*2+1];

			in.pointmarkerlist[i] = 0; //TODO it is assumed that is not boundary
		}


// int index=0; //TODO internal edges
// int eIndex=0;

// for(SizeType i=0;i<SizeType(_curves.size());++i)
// {
// 	const Matrix &curve=_curves[i];
// 	const int nPoints=curve.columns();

// 	for(SizeType j=0;j<nPoints;++j)
// 	{
// 		const int pointIndex=index+bPointsOffset;
// 		++index;

// 		in.pointlist[2*pointIndex]=curve(0, j);
// 		in.pointlist[2*pointIndex+1]=curve(1, j);

// 		in.pointmarkerlist[pointIndex] = 0;

// 		if(j<nPoints-1){
// 			const int edgeIndex=eIndex+bSize;
// 			++eIndex;
// 			in.segmentmarkerlist[edgeIndex] = 0; //0 because the edge and the point are not boundary

// 			in.segmentlist[2*edgeIndex]=pointIndex;
// 			in.segmentlist[2*edgeIndex+1]=pointIndex+1;
// 		}
// 	}
// }


		in.numberofholes = 0;
		in.numberofregions = 0;
	}

	void generate(const std::vector<Real> &boundaries, const std::vector<Real> &points, const bool is_polygon, TriangulatorOptions opts, Mesh &mesh) 
	{

		if(!is_polygon && !opts.convex_hull)
		{
			std::cerr<<"[Warning] impossible to mesh set of points without convex_hull set to true, flag resetted"<<std::endl;
			opts.convex_hull=true;
		}

		triangulateio out;
		triangulateio in;

		assign_values(boundaries, points, is_polygon, in);
		triangulate_values(in, out, opts);

		free_in(in);

		fill_mesh(out, opts, mesh);
		free_out(out);
	}


	void triangulate(const std::vector<Real> &polygon, Mesh &mesh, const TriangulatorOptions &opts)
	{
		generate(polygon, std::vector<Real>(), true, opts, mesh);
	}


	void triangulate(const std::vector<Real> &polygon, const std::vector<Real> &points, Mesh &mesh, const TriangulatorOptions &opts)
	{
		generate(polygon, points, true, opts, mesh);
	}


	void triangulate_points(const std::vector<Real> &boundary_points, Mesh &mesh, const TriangulatorOptions &opts)
	{
		generate(boundary_points, std::vector<Real>(), false, opts, mesh);
	}


	void triangulate_points(const std::vector<Real> &boundary_points, const std::vector<Real> &points, Mesh &mesh, const TriangulatorOptions &opts)
	{
		generate(boundary_points, points, false, opts, mesh);
	}

}



