#include "moonolith.hpp"

#include <stdlib.h>
#include <iostream>

#ifndef NDEBUG
#include "moonolith_test.hpp"
#endif

int main(int argc, char *argv[]) {
#ifdef NDEBUG
	(void)(argc);
	(void)(argv);
#else
	moonolith::TestRunner runner;
	runner.run(argc, argv);
#endif	
	return EXIT_SUCCESS;
}
