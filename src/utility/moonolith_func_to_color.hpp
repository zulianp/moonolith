#ifndef MOONOLITH_FUNC_TO_COLOR_HPP
#define MOONOLITH_FUNC_TO_COLOR_HPP

#include <vector>

namespace moonolith
{
	void func_to_hsv(const std::vector<Real> &func, std::vector<Real> &color, const bool invert = false);
	void func_to_hsv(const std::vector<Real> &func, const Real min, const Real max, std::vector<Real> &color, const bool invert = false);
	

	void func_to_rgb(const std::vector<Real> &func, 
		const Real fromR, const Real fromG, const Real fromB,
		const Real toR, const Real toG, const Real toB,
		std::vector<Real> &color, const bool invert = false);
	void func_to_rgb(const std::vector<Real> &func, const Real min, const Real max, 
		const Real fromR, const Real fromG, const Real fromB,
		const Real toR, const Real toG, const Real toB,
		std::vector<Real> &color, const bool invert = false);



	// void func_to_rgb_white(const std::vector<Real> &func,
	// 	const Real fromR, const Real fromG, const Real fromB,
	// 	std::vector<Real> &color, const bool invert = false)
	// {
	// 	func_to_rgb(func,
	// 		fromR,fromG,fromB,
	// 		1,1,1,
	// 	color, invert);
	// }
	// void func_to_rgb_white(const std::vector<Real> &func, const Real min, const Real max, 
	// 	const Real fromR, const Real fromG, const Real fromB,
	// 	std::vector<Real> &color, const bool invert = false)
	// {
	// 	func_to_rgb(func,min,max,fromR,fromG,fromB,1,1,1,color, invert);
	// }

	void func_to_fire(const std::vector<Real> &func, std::vector<Real> &color, const bool invert = false);
	void func_to_fire(const std::vector<Real> &func, const Real min, const Real max, std::vector<Real> &color, const bool invert = false);
}

#endif //MOONOLITH_FUNC_TO_COLOR_HPP

