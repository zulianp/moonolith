#include "moonolith_test.hpp"


#ifdef WITH_CPPUNIT
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#endif //WITH_CPPUNIT

namespace moonolith {

#ifdef WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE

	int TestRegistry::add_test_unit(const std::string &unit_name, TestRegistry::RunTest run_test)
	{
		units_[unit_name] = run_test;
		return 0;
	}

	TestRegistry &TestRegistry::instance()
	{
		static TestRegistry instance;
		return instance;
	}

	void TestRegistry::run_all()
	{
		for(std::map<std::string, RunTest>::const_iterator it = units_.begin(); it != units_.end(); ++it) {
			try {
				it->second();
			} catch(std::exception &ex) {
				std::cerr << "[Failure] in " << it->first << " " << ex.what() << std::endl;
			}
		}
	}

#endif //WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE


	TestRunner::TestRunner()
	{}

	TestRunner::~TestRunner()
	{}

	void TestRunner::run(int argc, char **argv) const {
	#ifdef WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE
		TestRegistry::instance().run_all();
		return;
	#endif //WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE

	#ifndef WITH_CPPUNIT
		(void)(argc);
		(void)(argv);
	#else
		CppUnit::TextUi::TestRunner runner;
		if(argc > 1) {    
			const std::string opt(argv[1]);
			if (opt == "-v") {
					// informs test-listener about testresults
				CppUnit::TestResult testresult;

					// register listener for collecting the test-results
				CppUnit::TestResultCollector collectedresults;
				testresult.addListener(&collectedresults);

					// register listener for per-test progress output
				CppUnit::BriefTestProgressListener progress;
				testresult.addListener(&progress);

				CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
				runner.addTest( registry.makeTest() );
				runner.run( testresult );

				CppUnit::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
				compileroutputter.write();
				return;

			} else if(opt == "-t" && argc > 2) {
				std::string testSuiteName = argv[2];
				CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry(testSuiteName);
				runner.addTest( registry.makeTest() );
				runner.run();
				return;
			}
		}

		CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
		runner.addTest( registry.makeTest() );
		runner.run();

	#endif // WITH_CPPUNIT
	}
}
