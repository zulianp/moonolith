#include "moonolith_string.hpp"

#include <sstream>
#include <algorithm>

namespace moonolith
{
	std::string trim(const std::string &s)
	{
		auto wsfront=std::find_if_not(s.begin(),s.end(),[](int c){return std::isspace(c);});
		auto wsback=std::find_if_not(s.rbegin(),s.rend(),[](int c){return std::isspace(c);}).base();
		return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
	}

	void split(const std::string &string, const char delim, std::vector<std::string> &components)
	{
		components.clear();

		std::stringstream ss;
		ss.str(string);
		std::string item;
		while (std::getline(ss, item, delim)) {
			components.push_back(item);
		}
	}
}
