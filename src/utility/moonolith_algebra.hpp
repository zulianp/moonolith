#ifndef MOONOLITH_ALGEBRA_HPP
#define MOONOLITH_ALGEBRA_HPP 

#include <vector>

namespace moonolith {
	Real det_2(const Real *mat);
	
	void inverse_2(const Real *in, const Real det, Real *out);
	
	void mat_vec_mul(const Integer rows, const Integer cols,
		const Real *matrix, const Real *vector, Real *result);
	
	void outer_product(const Integer size_left,
		const Integer size_right,
		const Real *left,
		const Real *right,
		Real * result);
	
	void solve_2x2(const Real *mat, const Real *rhs, Real *result);

	void lin_space(const Real from, const Real to, const Integer n, std::vector<Real> &res);
	void mesh_grid(
		const Real from_x, const Real to_x, const Integer n_x,
		const Real from_y, const Real to_y, const Integer n_y,
		std::vector<Real> &res);

	Real binomial_coefficient(const Integer n, const Integer k);








	class TensorIndex
	{
	public:
		TensorIndex();
		TensorIndex(const Integer dim1, const Integer dim2, const Integer dim3);

		void init(const Integer dim1, const Integer dim2, const Integer dim3);
		
		Integer index_for(const Integer i, const Integer j, const Integer k) const;
		Integer size() const;
		Integer operator[](const Integer i) const;

	private:
		Integer dims_[3];
	};
	
}

#endif //MOONOLITH_ALGEBRA_HPP
