#ifndef MOONOLITH_TEST_H
#define MOONOLITH_TEST_H

#include <string>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <assert.h>
#include <sstream>
#include <map>
#include <string>


#ifndef WITH_CPPUNIT
#ifndef WITH_MICROSOFT_TEST
#define WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE
#endif
#endif


#ifdef WITH_CPPUNIT
#include <cppunit/extensions/HelperMacros.h>

//class decl
#define M_TEST_CLASS(TestClassName_) class TestClassName_ : public CppUnit::TestFixture
#define M_TEST_SUITE_BEGIN(TestClassName_) CPPUNIT_TEST_SUITE(TestClassName_)
#define M_TEST_SUITE_END(TestClassName_) CPPUNIT_TEST_SUITE_END(/*TestClassName_*/)

//Function decl
#define M_TEST_CASE_ADD(TestCaseFunctionName_) CPPUNIT_TEST(TestCaseFunctionName_)
#define M_TEST_CASE(TestClassName_, TestCaseFunctionName_) private: void TestCaseFunctionName_()

//teardown and setup
#define M_TEST_SETUP(TestClassName_)  public: void setUp()
#define M_TEST_TEARDOWN(TestClassName_) public: void tearDown()

//Assertions
#define M_TEST_ASSERT_DOUBLES_EQ(Expected_, Actual_, Tol_) CPPUNIT_ASSERT_DOUBLES_EQUAL(Expected_, Actual_, Tol_ )
#define M_TEST_ASSERT(Cond_) CPPUNIT_ASSERT(Cond_)
#define M_TEST_ASSERT_EQ(Var1_, Var2_) CPPUNIT_ASSERT_EQUAL(Var1_, Var2_)

#define M_TEST_SUITE_REGISTRATION(TestClassName_) CPPUNIT_TEST_SUITE_REGISTRATION(TestClassName_)

#define M_TEST_MESSAGE(message_) {\
		std::stringstream ss_; ss_<< "\n\t" << __FILE__<<":"<<__LINE__<<"\n\t" << __FUNCTION__ << ": " <<(message_); \
		std::cout<< ss_.str() <<std::endl;\
}
#else 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WITH_GOOGLE_TEST
//TODO
#else
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WITH_MICROSOFT_TEST
#pragma once
// Headers for CppUnitTest
#include "CppUnitTest.h"

//class decl
#define M_TEST_CLASS(TestClassName_) TEST_CLASS(TestClassName_)
#define M_TEST_SUITE_BEGIN(TestClassName_) //
#define M_TEST_SUITE_END(TestClassName_) //

//Function decl
#define M_TEST_CASE_ADD(TestCaseFunctionName_) //
#define M_TEST_CASE(TestClassName_, TestCaseFunctionName_) TEST_METHOD(TestCaseFunctionName_)

//teardown and setup
#define M_TEST_SETUP(TestClassName_)  TEST_METHOD_INITIALIZE(setUp)
#define M_TEST_TEARDOWN(TestClassName_)  TEST_METHOD_CLEANUP(tearDown)

//Assertions
#define M_TEST_ASSERT_DOUBLES_EQ(Expected_, Actual_, Tol_) Microsoft::VisualStudio::CppUnitTestFramework::Assert::AreEqual(Expected_, Actual_, Tol_ )
#define M_TEST_ASSERT(Cond_) Microsoft::VisualStudio::CppUnitTestFramework::Assert::IsTrue(Cond_)
#define M_TEST_ASSERT_EQ(Var1_, Var2_) Microsoft::VisualStudio::CppUnitTestFramework:: Assert::AreEqual(Var1_, Var2_)

#define M_TEST_SUITE_REGISTRATION(TestClassName_) //

#define M_TEST_MESSAGE(message_) {\
	std::stringstream ss_; ss_<< "\n\t" << __FILE__<<":"<<__LINE__<<"\n\t" << __FUNCTION__ << ": " <<(message_); \
	Microsoft::VisualStudio::CppUnitTestFramework::Logger::WriteMessage(ss_.str().c_str());\
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
#else
//////////////////////////////////////////////////////// BUILT IN HACKED TEST SUITE ///////////////////////////////////////////////////////////////////////////


class TestClassBase {
public:
	virtual ~TestClassBase() {}
	virtual void set_up() {}
	virtual void tear_down() {}
};

//class decl
#define M_TEST_CLASS(TestClassName_) class TestClassName_ : public TestClassBase
#define M_TEST_SUITE_BEGIN(TestClassName_) public: static void Run() { static TestClassName_ testRunner; static const char * className = #TestClassName_;
#define M_TEST_SUITE_END(TestClassName_) return; } 

//Function decl
#define M_TEST_CASE_ADD(TestCaseFunctionName_) testRunner.set_up(); std::cout << className << "::" << #TestCaseFunctionName_ << "..." << std::flush; testRunner.TestCaseFunctionName_();  std::cout << "OK" << std::endl; testRunner.tear_down()
#define M_TEST_CASE(TestClassName_, TestCaseFunctionName_) void TestCaseFunctionName_() 

//teardown and setup
#define M_TEST_SETUP(TestClassName_)  public: void set_up()
#define M_TEST_TEARDOWN(TestClassName_)  public: void tear_down()

//Assertions
#define M_TEST_ASSERT_DOUBLES_EQ(Expected_, Actual_, Tol_) assert(fabs(Expected_ - Actual_) <= Tol_)
#define M_TEST_ASSERT(Cond_) assert(Cond_)
#define M_TEST_ASSERT_EQ(Var1_, Var2_) assert(Var1_ == Var2_)
#define M_TEST_SUITE_REGISTRATION(TestClassName_) static moonolith::AutoRegisterTestUnit<TestClassName_> dummyTestVariable_ ## TestClassName_ ## __LINE__(#TestClassName_)

#define M_TEST_MESSAGE(message_) {\
		std::stringstream ss_; ss_<< "\n\t" << __FILE__<<":"<<__LINE__<<"\n\t" << __FUNCTION__ << ": " <<(message_); \
		std::cout<< ss_.str() <<std::endl;\
}

#endif //WITH_MICROSOFT_TEST
#endif //WITH_GOOGLE_TEST
#endif //WITH_CPPUNIT


namespace moonolith {

	class TestRunner {
	public:
		TestRunner();
		virtual ~TestRunner();
		void run(int argc, char **argv) const;
	};


#ifdef WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE

	class TestRegistry {
	public:
		typedef void (*RunTest)();

		int add_test_unit(const std::string &unit_name, RunTest run_test);

		static TestRegistry &instance();
		void run_all();

	private:
		TestRegistry() {}
		std::map<std::string, RunTest> units_;
	};

	template<class TestUnit>
	class AutoRegisterTestUnit {
	public:
		AutoRegisterTestUnit(const char * name)
		{
			moonolith::TestRegistry::instance().add_test_unit(name, &TestUnit::Run);
		}
	};

#endif //WITH_EXPERIMENTAL_SELF_IMPLEMENTED_TEST_SUITE	
}

#endif // MOONOLITH_TEST_H
