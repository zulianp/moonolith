#ifndef _WIN32
#ifndef __linux__

#include <execinfo.h>   // for backtrace
#include <dlfcn.h>      // for dladdr
#include <cxxabi.h>     // for __cxa_demangle
#include "moonolith_stacktrace.hpp"


#include <sstream>
#include <fstream>




namespace moonolith {

	void Stacktrace::get(const Integer skip)
	{
		const int max_frames = 128;
		void *callstack[max_frames];

		Integer n_frames = backtrace(callstack, max_frames);
		char **symbols = backtrace_symbols(callstack, n_frames);

		for (Integer i = skip; i < n_frames; i++) {
			Dl_info info;
			if (dladdr(callstack[i], &info) && info.dli_sname) 
			{
				const std::string file_name=info.dli_fname;

				char *demangled = NULL;
				int status = -1;
				if (info.dli_sname[0] == '_')
				{
					demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
				}
				
				const std::string fun_name=status == 0 ? demangled : info.dli_sname == 0 ? symbols[i] : info.dli_sname;
				// long offset = (char *)callstack[i] - (char *)info.dli_saddr;

				free(demangled);
				trace_.push_back({
					i, 
					fun_name, 
					file_name, 
					(char *)callstack[i], 
					(char *)symbols[i]
				});
			}
			else {
				// trace_.push_back(TraceElement(i, "", "", (char *) callstack[i], (char *)symbols[i]));
				trace_.push_back({i, "", "", "", "failed"});

			}
		}

		free(symbols);
		if (n_frames == max_frames) {
			std::cerr << "[stack truncated]" << std::endl;
		}
	}

	void Stacktrace::describe(std::ostream &os) const
	{
		for(const_iterator it = begin(); it != end(); ++it) {
			if(it->call.empty()) continue;

			os << it->depth << " " << it->call << "\n";
		}
	}

}


#endif
#endif
