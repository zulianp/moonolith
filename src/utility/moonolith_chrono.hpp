#ifndef MOONOLITH_CHRONO_HPP
#define MOONOLITH_CHRONO_HPP

#include <chrono>
#include <ctime>
#include <ostream>

#ifdef WIN32   // Windows system specific
#include <windows.h>
#elif __APPLE__ // Unix based system specific
#include <mach/mach_time.h> // for mach_absolute_time
#else
#include <sys/time.h>
#endif

namespace moonolith {

	class Chrono {
	public:
		void start();
		void stop();
		void describe(std::ostream &os) const;
		
		inline double get_seconds() const
		{
			return realtime_duration_;
		}

		inline friend std::ostream & operator<<(std::ostream &os, const Chrono &c)
		{
			c.describe(os);
			return os;
		}

	private:
		typedef std::chrono::high_resolution_clock::time_point TimePoint;
		typedef std::chrono::duration<double, std::milli> DurationMillis;

		TimePoint start_, end_;
		DurationMillis duration_;

#ifdef WIN32
		LARGE_INTEGER frequency_;      
		LARGE_INTEGER realtime_start_;     
		LARGE_INTEGER realtime_end_;       
#elif __APPLE__
		uint64_t realtime_start_;           
		uint64_t realtime_end_;  

		double realtime_duration() const;
#else
		timeval realtime_start_;           
		timeval realtime_end_;             
#endif

		double realtime_duration_;
	};
}

#endif //MOONOLITH_CHRONO_HPP
