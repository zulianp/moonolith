#include "moonolith_func_to_color.hpp"


#include <cassert>
#include <cmath>
#include <limits>

namespace moonolith
{

	void get_range(const std::vector<Real> &func, Real &min, Real &max)
	{
		min = std::numeric_limits<Real>::max();
		max = -min;

		for(std::size_t i=0; i<func.size(); ++i)
		{
			max = std::max(max, func[i]);
			min = std::min(min, func[i]);
		}
	}



	void func_to_hsv(const std::vector<Real> &func, std::vector<Real> &color, const bool invert)
	{
		Real min, max;
		get_range(func, min, max);

		func_to_hsv(func,min,max,color,invert);
	}

	void func_to_hsv(const std::vector<Real> &func, const Real min, const Real max, std::vector<Real> &color, const bool invert)
	{
		assert(max>min);

		color.resize(func.size()*3);

		Real r,g,b;

		for(std::size_t i=0; i<func.size(); ++i)
		{
			Real val = (func[i]-min)/(max-min);
			if(val < 0) val = 0;
			if(val > 1) val = 1;

			if(invert) val=1-val;

			val *= 2.0/3.0;


			const Real h = val * 360;
			const Real v = 1;
			const Real s = 1;

			const Real h_prime = h / 60.0;
			const Integer index = floor(h_prime);


			const Real a1 = v * (1.0 - s);
			const Real a2 = v * (1.0 - s * ( h_prime - index));
			const Real a3 = v * (1.0 - s * ( 1.0 - ( h_prime - index)));

			switch(index)
			{
				case 0: r = v; g = a3; b = a1; break;
				case 1: r = a2; g = v; b = a1; break;
				case 2: r = a1; g = v; b = a3; break;
				case 3: r = a1; g = a2; b = v; break;
				case 4: r = a3; g = a1; b = v; break;
				default: r = v; g = a1; b = a2; break;
			}

			color[i*3] = r;
			color[i*3+1] = g;
			color[i*3+2] = b;
		}
	}



	void func_to_rgb(const std::vector<Real> &func,
		const Real fromR, const Real fromG, const Real fromB,
		const Real toR, const Real toG, const Real toB,
		std::vector<Real> &color, const bool invert)
	{
		Real min, max;
		get_range(func, min, max);

		func_to_rgb(func,min,max,
			fromR, fromG, fromB,
			toR, toG, toB,
			color,invert);
	}

	void func_to_rgb(const std::vector<Real> &func, const Real min, const Real max, 
		const Real fromR, const Real fromG, const Real fromB,
		const Real toR, const Real toG, const Real toB,
		std::vector<Real> &color, const bool invert)
	{
		assert(max>min);

		color.resize(func.size()*3);

		for(std::size_t i=0; i<func.size(); ++i)
		{
			Real val = (func[i]-min)/(max-min);
			if(val < 0) val = 0;
			if(val > 1) val = 1;

			if(invert) val=1-val;

			

			color[i*3]   = (1-val)*fromR + val*toR;
			color[i*3+1] = (1-val)*fromG + val*toG;
			color[i*3+2] = (1-val)*fromB + val*toB;
		}
	}

	void func_to_fire(const std::vector<Real> &func, std::vector<Real> &color, const bool invert)
	{
		Real min, max;
		get_range(func, min, max);

		func_to_fire(func,min,max,color,invert);
	}

	void func_to_fire(const std::vector<Real> &func, const Real min, const Real max, std::vector<Real> &color, const bool invert)
	{
		assert(max>min);

		color.resize(func.size()*3);

		Real r,g,b;


		for(std::size_t i=0; i<func.size(); ++i)
		{
			Real val = (func[i]-min)/(max-min);
			if(val < 0) val = 0;
			if(val > 1) val = 1;

			if(invert) val=1-val;

			if(val < 0.2) {
				const Real t = val * 5.0;

				r = (1-t) * 0.46 + t * 0.83;
				g = 0;
				b = 0;
			}
			else if(val < 0.5) {
				const Real t = (val-0.2)/0.3;

				r = (1-t) * 0.83 + t * 1.0;
				g = t * 0.64;
				b = 0;
			}
			else if(val < 0.75) {
				const Real t = (val-0.5)*4.0;
				r = 1;
				g = (1-t) * 0.64 + t;
				b = 0;
			}
			else
			{
				const Real t = (val-0.75)*4.0;
				r = 1;
				g = 1;
				b = t;
			}


			color[i*3] = r;
			color[i*3+1] = g;
			color[i*3+2] = b;
		}
	}
}

