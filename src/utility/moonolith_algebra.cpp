#include "moonolith_algebra.hpp"

#include <cassert>

namespace moonolith {
	Real det_2(const Real *mat)
	{
		return mat[0] * mat[3] - mat[2] * mat[1];
	}
	
	void inverse_2(const Real *in, const Real det, Real *out)
	{
		out[0] =  in[3]/det;
		out[1] = -in[1]/det;
		out[2] = -in[2]/det;
		out[3] =  in[0]/det;
	}
	
	void mat_vec_mul(const Integer rows, const Integer cols,
		const Real *matrix, const Real *vector, Real *result)
	{
		for(Integer i = 0; i < rows; ++i) {
			const Integer offset = i * cols;
			result[i] = matrix[offset] * vector[0];
			
			for(Integer j = 1; j < cols; ++j) {
				result[i] += matrix[offset+j] * vector[j];
			}
		}
	}
	
	void outer_product(const Integer size_left,
		const Integer size_right,
		const Real *left,
		const Real *right,
		Real * result)
	{
		for(Integer i = 0; i < size_left; ++i) {
			const Integer offset_i = i * size_right;
			for(Integer j = 0; j < size_right; ++j) {
				result[offset_i + j] = left[i] * right[j];
			}
		}
	}
	
	void solve_2x2(const Real *mat, const Real *rhs, Real *result)
	{
		Real inv_mat[2 * 2];
		inverse_2(mat, det_2(mat), inv_mat);
		mat_vec_mul(2, 2, inv_mat, rhs, result);
	}

	void lin_space(const Real from, const Real to, const Integer n, std::vector<Real> &res)
	{
		assert(to>from);
		assert(n>1);

		res.resize(n);
		const Real delta = (to-from) / (n-1.0);

		for(Integer i = 0; i < n; ++i)
			res[i] = from + delta*i;
	}

	void mesh_grid(
		const Real from_x, const Real to_x, const Integer n_x,
		const Real from_y, const Real to_y, const Integer n_y,
		std::vector<Real> &res)
	{
		assert(to_x>from_x);
		assert(n_x>1);
		assert(to_y>from_y);
		assert(n_y>1);

		res.resize(n_x*n_y*2);
		const Real delta_x = (to_x-from_x) / (n_x-1.0);
		const Real delta_y = (to_y-from_y) / (n_y-1.0);

		Integer index = 0;
		for(Integer i = 0; i < n_x; ++i)
		{
			const Real x = from_x + delta_x * i;

			for(Integer j = 0; j < n_y; ++j)
			{
				const Real y = from_y + delta_y * j;

				res[index++] = x;
				res[index++] = y;
			}
		}

		assert(index == n_x * n_y * 2);
	}

	Real binomial_coefficient(const Integer n, const Integer kk)
	{
		Real res = 1;
		Integer k = kk;

		if(k > n - k)
			k = n - k;

		for(Integer i = 0; i < k; ++i)
		{
			res *= (n - i);
			res /= (i + 1);
		}

		return res;
	}









	TensorIndex::TensorIndex()
	{ 
		dims_[0]=dims_[1]=dims_[2]=0;
	}


	TensorIndex::TensorIndex(const Integer dim1, const Integer dim2, const Integer dim3)
	{
		init(dim1, dim2, dim3);
	}


	void TensorIndex::init(const Integer dim1, const Integer dim2, const Integer dim3)
	{
		dims_[0]=dim1;
		dims_[1]=dim2;
		dims_[2]=dim3;
	}


	Integer TensorIndex::index_for(const Integer i, const Integer j, const Integer k) const
	{
		assert(dims_[0] > 0);
		assert(dims_[1] > 0);
		assert(dims_[2] > 0);

		assert(i>=0 && i<dims_[0]);
		assert(j>=0 && j<dims_[1]);
		assert(k>=0 && k<dims_[2]);

		return (i*dims_[1]+j)*dims_[2]+k;
	}

	Integer TensorIndex::size() const
	{
		assert(dims_[0] > 0);
		assert(dims_[1] > 0);
		assert(dims_[2] > 0);

		return dims_[0]*dims_[1]*dims_[2];
	}

	Integer TensorIndex::operator[](const Integer i) const
	{
		assert(i>=0 && i < 3);
		assert(dims_[0] > 0);
		assert(dims_[1] > 0);
		assert(dims_[2] > 0);


		return dims_[i];
	}
}
