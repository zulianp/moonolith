#ifndef _WIN32
#ifndef __linux__

#ifndef MOONOLITH_STACKTRACE_HPP
#define MOONOLITH_STACKTRACE_HPP 

#include <vector>
#include <iostream>


namespace moonolith {
	
	typedef struct {
		Integer depth;
		std::string call;
		std::string binary;
		std::string address;
		std::string extra;
	} TraceElement;


	class Stacktrace 
	{
	public:
		typedef std::vector<TraceElement>::const_iterator const_iterator;
		
		void get(const Integer skip = 1);
		void describe(std::ostream &os) const;
		
		inline Integer n_elements() const { return trace_.size(); }
		inline const_iterator begin() const { return trace_.begin(); }
		inline const_iterator end() const { return trace_.end(); }

	private:
		std::vector<TraceElement> trace_;
	};
}

#endif //MOONOLITH_STACKTRACE_HPP
#endif
#endif
