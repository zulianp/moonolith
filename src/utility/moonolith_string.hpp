#ifndef MOONOLITH_STRING_UTILITY_HPP
#define MOONOLITH_STRING_UTILITY_HPP

#include <string>
#include <vector>

namespace moonolith {
	std::string trim(const std::string &s);
	void split(const std::string &string, const char delim, std::vector<std::string> &components);
}

#endif