#include "moonolith_chrono.hpp"


namespace moonolith {

	void Chrono::start()
	{
		start_ = std::chrono::high_resolution_clock::now();

#ifdef WIN32
		QueryPerformanceCounter(&realtime_start_);
#elif __APPLE__
		realtime_start_ = mach_absolute_time();
#else
		gettimeofday(&realtime_start_, NULL);
#endif //WIN32		
	}

#ifdef __APPLE__
	double Chrono::realtime_duration() const
	{
		uint64_t difference = realtime_end_ - realtime_start_;
		static double conversion = 0.0;

		if( conversion == 0.0 )
		{
			mach_timebase_info_data_t info;
			kern_return_t err = mach_timebase_info( &info );

			if( err == 0  )
				conversion = 1e-9 * (double) info.numer / (double) info.denom;
		}

		return conversion * (double) difference;
	}
#endif //__APPLE__

	void Chrono::stop()
	{
		end_ = std::chrono::high_resolution_clock::now();
		duration_ = end_ - start_;

#ifdef WIN32
		double start_time_ms = 0;
		double end_time_ms   = 0;

		QueryPerformanceCounter(&realtime_end_);

		start_time_ms = realtime_start_.QuadPart * (1000000.0 / frequency_.QuadPart);
		end_time_ms   = realtime_end_.QuadPart * (1000000.0 / frequency_.QuadPart);
		realtime_duration_  = end_time_ms - start_time_ms;
#elif __APPLE__
		realtime_end_      = mach_absolute_time();
		realtime_duration_ = realtime_duration();
#else
		double start_time_ms = 0;
		double end_time_ms   = 0;

		gettimeofday(&realtime_end_, NULL);
		start_time_ms = (realtime_start_.tv_sec * 1000000.0) + realtime_start_.tv_usec;
		end_time_ms   = (realtime_end_.tv_sec * 1000000.0) + realtime_end_.tv_usec;
		realtime_duration_ = end_time_ms - start_time_ms;
#endif //WIN32
	}

	void Chrono::describe(std::ostream &os) const
	{

		os << "[Time elapsed] clock: " << std::to_string(duration_.count()) <<  " milliseconds,\t";
		os << " " << realtime_duration_ <<  " seconds.\t";	
	}

}

