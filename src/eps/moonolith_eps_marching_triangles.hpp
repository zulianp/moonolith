#ifndef MOONOLITH_EPS_MARCHING_TRIANGLES_HPP
#define MOONOLITH_EPS_MARCHING_TRIANGLES_HPP


#include "moonolith_eps_canvas.hpp"
#include "moonolith_svg_canvas.hpp"
#include "moonolith_mesh.hpp"

namespace moonolith {
    void draw_countours(const Mesh &mesh, const std::vector<Real> &func, const std::vector<Real> &iso_values, EPSCanvas &canvas);
    void draw_countour(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, EPSCanvas &canvas);

    void draw_countours(const Mesh &mesh, const std::vector<Real> &func, const std::vector<Real> &iso_values, SVGCanvas &canvas);
    void draw_countour(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, SVGCanvas &canvas);
} 

#endif // MOONOLITH_EPS_MARCHING_TRIANGLES_HPP
