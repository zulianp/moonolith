#include "moonolith_eps_marching_triangles.hpp"
#include "moonolith_eps_canvas.hpp"

#include <list>
#include <vector>
#include <cmath>

namespace moonolith {
    typedef std::pair<Integer, Integer> Edge;
    typedef std::vector<Integer> Face;

    const Real TOL = 1e-12;

    bool get_intersecting_edges(const std::vector<int> &marker, const Face &f, Edge &e1, Edge &e2) {
        const int flag1 = marker[f[0]];
        const int flag2 = marker[f[1]];
        const int flag3 = marker[f[2]];
        const int index = flag1 << 2 | flag2 << 1 | flag3;

        if (index == 0 || index == 7) return false;

        if (index == 1 || index == 6) {
            e1.first = 1;
            e1.second = 2;
            e2.first = 2;
            e2.second = 0;
            return true;
        }

        if (index == 2 || index == 5) {
            e1.first = 0;
            e1.second = 1;
            e2.first = 1;
            e2.second = 2;
            return true;
        }

        if (index == 3 || index == 4) {
            e1.first = 0;
            e1.second = 1;
            e2.first = 2;
            e2.second = 0;
            return true;
        }

        return false;
    }

    void push_back_point(const Integer index, const std::vector<Real> &points, std::list<Real> &curve)
    {
        curve.push_back(points[index]);
        curve.push_back(points[index+1]);
    }

    void push_front_point(const Integer index, const std::vector<Real> &points, std::list<Real> &curve)
    {
        curve.push_front(points[index+1]);
        curve.push_front(points[index]);
    }

    Real distance(const std::vector<Real> &points, const Integer i1, const Integer i2)
    {
        const Real x1 = points[i1];
        const Real y1 = points[i1+1];

        const Real x2 = points[i2];
        const Real y2 = points[i2+1];

        const Real dx = x1-x2;
        const Real dy = y1-y2;

        return sqrt(dx*dx+dy*dy);
    }

    template<class Canvas>
    void draw_isolines(const std::vector<Real> &points, Canvas &canvas)
    {
        typedef typename std::list<Edge>::iterator iter;

        std::list<Edge> pool;

        for (std::size_t i = 0; i < points.size(); i += 4) 
            pool.push_back(Edge(i, i + 2));

        // canvas.stroke_line(points[edges[i].first], points[edges[i].first+1],points[edges[i].second], points[edges[i].second+1]);

        while (!pool.empty())
        {
            const Edge edge = pool.front();
            pool.pop_front();

            bool first = true;
            Integer curr = edge.first;

            std::list<Real> curve;

            push_back_point(edge.second,points,curve);
            push_back_point(edge.first,points,curve);

            while (true) {
                bool found = false;

                for (iter it = pool.begin(); it != pool.end(); ++it) {
                    if(distance(points,it->first, curr) < TOL)
                    {
                        curr = it->second;
                        pool.erase(it);
                        found = true;

                        if (first) push_back_point(curr,points,curve);
                        else push_front_point(curr,points,curve);

                        break;
                    } 
                    else if( distance(points, it->second, curr) < TOL )
                    {
                        curr = it->first;
                        pool.erase(it);
                        found = true;

                        if (first) push_back_point(curr,points,curve);
                        else push_front_point(curr,points,curve);

                        break;
                    }
                }

                if (!found && first)
                {
                    curr = edge.second;
                    first = false;
                }
                else if(!found)
                    break;
            }



            std::list<Real>::iterator it = curve.begin();

            std::vector<Real> points;

            for(; it!=curve.end();++it){
                const Real x = *it;
                ++it;
                const Real y = *it;
                if(!std::isnan(x) && !std::isnan(y)) {
                    // canvas.move_to(x,y);
                    points.push_back(x);
                    points.push_back(y);
                    break;
                }
            }
            for(++it; it!=curve.end();++it){
                const Real x = *it;
                ++it;
                const Real y = *it;
                if(!std::isnan(x) && !std::isnan(y)) {
                    // canvas.continue_segment(x,y);
                    points.push_back(x);
                    points.push_back(y);
                }
            }
            canvas.stroke_poly_line(&points[0], points.size()/2);
        }
    }

    void get_intersecting_points(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, const Edge &e, const Face &f, std::vector<Real> &points)
    {
        const Real v1x = mesh.points[f[e.first]*mesh.dim];
        const Real v1y = mesh.points[f[e.first]*mesh.dim+1];

        const Real v2x = mesh.points[f[e.second]*mesh.dim];
        const Real v2y = mesh.points[f[e.second]*mesh.dim+1];

        const Real f1 = func[f[e.first]];
        const Real f2 = func[f[e.second]];

        if(std::isnan(f1) || std::isnan(f2)) {
            points.push_back(NAN);
            points.push_back(NAN);
            return;
        }

        const Real denom = f2 - f1;

        // assert(fabs(denom) > 1e-8);

        if(fabs(denom) <= 1e-8) {
            points.push_back(NAN);
            points.push_back(NAN);
            return;
        }

        Real t = (isoValue - f1) / denom;

        assert(t>=0 && t<=1);

        points.push_back((1.0 - t) * v1x + t * v2x);
        points.push_back((1.0 - t) * v1y + t * v2y);
    }


    template<class Canvas>
    void draw_countour_aux(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, Canvas &canvas) 
    {
        assert(Integer(func.size()) == mesh.n_nodes());
        assert(mesh.dim >= 2);

        if(mesh.dim != 2)
            std::cerr << "[Warning] marching triangles ignoring higher dimentions"<<std::endl;

        std::vector<Real> points;

        std::vector<int> marker(mesh.n_nodes());

        for (std::size_t i = 0; i < marker.size(); ++i)
        {
            if (func[i] < isoValue) marker[i] = 0;
            else marker[i] = 1;
        }

        Edge e1;
        Edge e2;

        const Integer n_faces = mesh.n_elements();
        Face f(3);

        for(Integer i = 0; i < n_faces; ++i)
        {
            assert(mesh.el_ptr[i] - mesh.el_ptr[i+1] && "Works only with triangular faces");
            f[0]=mesh.el_index[mesh.el_ptr[i]];
            f[1]=mesh.el_index[mesh.el_ptr[i]+1];
            f[2]=mesh.el_index[mesh.el_ptr[i]+2];

            const bool found = get_intersecting_edges(marker, f, e1, e2);
            if (found) 
            {
                get_intersecting_points(mesh, func, isoValue, e1, f, points);
                get_intersecting_points(mesh, func, isoValue, e2, f, points);
            }
        }

        draw_isolines(points, canvas);
    }



    void draw_countour(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, EPSCanvas &canvas)
    {
        draw_countour_aux(mesh, func, isoValue, canvas);
    }

    void draw_countours(const Mesh &mesh, const std::vector<Real> &func, const std::vector<Real> &iso_values, EPSCanvas &canvas)
    {
        for (std::size_t i = 0; i < iso_values.size(); ++i)
            draw_countour_aux(mesh, func, iso_values[i], canvas);
    }


    void draw_countours(const Mesh &mesh, const std::vector<Real> &func, const std::vector<Real> &iso_values, SVGCanvas &canvas)
    {
        for (std::size_t i = 0; i < iso_values.size(); ++i)
            draw_countour_aux(mesh, func, iso_values[i], canvas);
    }

    void draw_countour(const Mesh &mesh, const std::vector<Real> &func, const Real isoValue, SVGCanvas &canvas)
    {
        draw_countour_aux(mesh, func, isoValue, canvas);
    }
} 

