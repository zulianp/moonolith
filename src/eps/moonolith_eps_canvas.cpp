#include "moonolith_eps_canvas.hpp"

#include <cassert>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

#include "moonolith_path.hpp"

namespace moonolith {


	EPSCanvas::EPSCanvas()
	: minX_(0), minY_(0), maxX_(0), maxY_(0),
	line_width_(1), clipping_(false), current_scale_(1)
	{
		init();
	}

	void EPSCanvas::clear()
	{
		minX_=0;
		minY_=0;

		maxX_=0;
		maxY_=0;


		clipping_ = false;
		current_scale_ = 1;

		line_width_=1;

		content_.str().clear();
		content_.clear();
		init();
	}

	void EPSCanvas::init() 
	{
		content_ << "/show-ctr {\ndup stringwidth pop\n -2 div 0\n rmoveto show\n} def\n\n 2 setlinejoin\n\n";
	}

	void EPSCanvas::set_line_width(const Real line_width)
	{
		assert(line_width>0);
		line_width_ = line_width;

		content_<<line_width*current_scale_<<" setlinewidth\n\n"; 
	}

	void EPSCanvas::set_dashing(const Real dash)
	{
		content_<<"["<<dash<<"] 0 setdash\n\n"; 
	}


	void EPSCanvas::set_dashing(const Real *dash, const Integer n)
	{
		assert(n>0);

		content_ << "[";
		for(Integer i=0; i < n-1; ++i)
			content_ << dash[i] << " ";

		content_ << dash[n-1] << "] 0 setdash\n\n"; 
	}

	void EPSCanvas::clear_dashing()
	{
		content_<<"[] 0 setdash\n\n"; 
	}

	void EPSCanvas::set_color(const Real red, const Real green, const Real blue)
	{
		assert(red >= 0);
		assert(red <= 1);

		assert(green >= 0);
		assert(green <= 1);

		assert(blue >= 0);
		assert(blue <= 1);

		content_<<red<<" "<<green<<" "<<blue<<" setrgbcolor\n\n\n";
	}

	void EPSCanvas::move_to(const Real x, const Real y)
	{
		update_box(x,y);

		content_<<x<<" "<<y<<" moveto\n"; 
	}

	void EPSCanvas::translate(const Real x, const Real y)
	{
		content_<<x<<" "<<y<<" translate\n\n";
	}

	void EPSCanvas::scale(const Real x, const Real y)
	{
		using std::min;
		current_scale_ = min(1.0/x,1.0/y);
		content_<<x<<" "<<y<<" scale\n\n";
	}

	void EPSCanvas::begin_clip_rect(const Real x, const Real y, const Real w, const Real h)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		clipping_ = true;
		content_<<x<<" "<<y<<" "<<w<<" "<<h<<" rectclip\n\n";
	}

	void EPSCanvas::end_clip_rect()
	{
		clipping_ = false;
		content_<<"cliprestore\n\n";
	}

	void EPSCanvas::stroke_rect(const Real x, const Real y, const Real w, const Real h)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		content_<<x<<" "<<y<<" "<<w<<" "<<h<<" rectstroke\n\n";
	}

	void EPSCanvas::fill_rect(const Real x, const Real y, const Real w, const Real h)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		content_<<x<<" "<<y<<" "<<w<<" "<<h<<" rectfill\n\n";
	}


	void EPSCanvas::draw_circle(const Real x, const Real y, const Real radius)
	{
		update_box(x-radius, y-radius);
		update_box(x+radius, y+radius);

		content_ << x << " " << y  << " moveto\n";
		content_ << x << " " << y << " " << radius << " 0 360 arc\n";
	}

	void EPSCanvas::stroke_circle(const Real x, const Real y, const Real radius)
	{
		draw_circle(x,y,radius);
		stroke();
	}

	void EPSCanvas::fill_circle(const Real x, const Real y, const Real radius)
	{
		draw_circle(x,y,radius);
		fill();
	}

	void EPSCanvas::draw_text(const Real x, const Real y, const Real fontSize, const std::string &fontFamily, const std::string &text, const bool centered)
	{

		content_ << "/" << fontFamily << " " << fontSize << " selectfont\n";
// content_ << "/" << fontFamily << " findfont\n";
// content_ << fontSize << " scalefont\n";
// content_ << "setfont\nnewpath\n";
		content_ << x << " " << y << " moveto ";
		content_ << "(" << text << ")" << " show"<< (centered?"-ctr":"") <<"\n";  

		update_box(x,y);
		update_box(x,y+fontSize);
	}



	void EPSCanvas::continue_segment(const Real x, const Real y)
	{
		update_box(x,y);

		content_<<x<<" "<<y<<" lineto\n";
	}

	void EPSCanvas::stroke()
	{
		content_<<"stroke\n\n";
	}

	void EPSCanvas::fill()
	{
		content_<<"fill\n\n";
	}

	void EPSCanvas::close_path()
	{
		content_<<"closepath\n\n";
	}

	void EPSCanvas::save_state()
	{
		content_<<"\ngsave\n";
	}

	void EPSCanvas::restore_state()
	{
		current_scale_ = 1;
		content_<<"\ngrestore\n";
	}

	void EPSCanvas::draw_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end)
	{
		update_box(x_begin, y_begin);
		update_box(x_end,   y_end);

		content_<<x_begin<<" "<<y_begin<<" moveto\n";
		content_<<x_end<<" "<<y_end<<" lineto\n";
	}

	void EPSCanvas::stroke_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end)
	{
		draw_line(x_begin,y_begin,x_end,y_end);
		stroke();
	}

	void EPSCanvas::draw_poly_line(const Real *x, const Real *y, const Integer n)
	{
		assert(n>1);

		update_box(x[0], y[0]);
		content_<<x[0]<<" "<<y[0]<<" moveto\n";

		for(Integer i=1; i < n; ++i){
			update_box(x[i], y[i]);
			content_<<x[i]<<" "<<y[i]<<" lineto\n";
		}
	}

	void EPSCanvas::stroke_poly_line(const Real *x, const Real *y, const Integer n)
	{
		draw_poly_line(x,y,n);
		stroke();
	}

	void EPSCanvas::stroke_poly_line(const Real *xy, const Integer n)
	{
		assert(n>1);

		update_box(xy[0], xy[1]);
		content_<<xy[0]<<" "<<xy[1]<<" moveto\n";

		for(Integer i=1; i < n; ++i){
			update_box(xy[2*i], xy[2*i+1]);
			content_<<xy[2*i]<<" "<<xy[2*i+1]<<" lineto\n";
		}

		stroke();
	}

	void EPSCanvas::fill_triangle(const Real *x, const Real *y, const Real *colors)
	{
		content_<<"<<\n\t/ShadingType 4\n\t/ColorSpace /DeviceRGB\n\t/DataSource [\n"; 
		for(Integer i=0; i<3; ++i)
		{
			update_box(x[i], y[i]);

			content_<<"\t\t0 "<<x[i]<<" "<<y[i]<<" ";
			content_<<""<<colors[i*3]<<" "<<colors[i*3+1]<<" "<<colors[i*3+2]<<"\n";
		}

		content_<<"\t]\n>> shfill\n\n";
	}


	void EPSCanvas::draw_image(const int *pixels, const Integer w, const Integer h)
	{
		assert(w>0);
		assert(h>0);

		update_box(w, h);

		content_ << "/picstr 768 string def\n";
		content_ << w << " " << h << " scale\n";
		content_ << w << " " << h << " 8 [" << w << " 0 0 " <<  -h << " 0 " << h << "] \n";
		content_ << "{\n\tcurrentfile\t\npicstr readhexstring pop\n}";
		content_ << "false 3\n";
		content_ << "colorimage\n";
		for(Integer i = 0; i < w*h; ++i) {
			content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+0];
			content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+1];
			content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+2];
		}

		content_ << "\n";
	}

	void EPSCanvas::draw_polygon(const Real *x, const Real *y, const Integer n)
	{
		assert(n>2);

		update_box(x[0], y[0]);
		move_to(x[0],y[0]);
		for(Integer i=1; i<n; ++i)
		{
			update_box(x[i], y[i]);
			continue_segment(x[i],y[i]);
		}
		
		close_path();
	}

	void EPSCanvas::draw_polygon(const Real *xy, const Integer n)
	{
		assert(n>2);

		update_box(xy[0], xy[1]);
		move_to(xy[0], xy[1]);
		for(Integer i=1; i<n; ++i)
		{
			update_box(xy[2*i], xy[2*i+1]);
			continue_segment(xy[2*i], xy[2*i+1]);
		}
		
		close_path();
	}

	void EPSCanvas::stroke_polygon(const Real *x, const Real *y, const Integer n)
	{
		draw_polygon(x,y,n);
		stroke();
	}

	void EPSCanvas::fill_polygon(const Real *x, const Real *y, const Integer n)
	{
		draw_polygon(x,y,n);
		fill();
	}

	void EPSCanvas::stroke_polygon(const Real *xy, const Integer n)
	{
		draw_polygon(xy,n);
		stroke();
	}

	void EPSCanvas::fill_polygon(const Real *xy, const Integer n)
	{
		draw_polygon(xy,n);
		fill();
	}

	void EPSCanvas::draw_mesh(const Mesh &mesh)
	{
		if(mesh.dim!=2)
			std::cerr<<"[Warning] ignoring higher dimentions"<<std::endl;

		std::vector<Real> face;

		for(Integer i = 0; i < mesh.n_elements(); ++i)
		{
			face.clear();

			for(Integer j=mesh.el_ptr[i]; j < mesh.el_ptr[i+1]; ++j)
			{
				const Integer index=mesh.el_index[j];

				face.push_back(mesh.points[index*mesh.dim]);
				face.push_back(mesh.points[index*mesh.dim+1]);
			}

			draw_polygon(face);
		}
	}

	void EPSCanvas::stroke_mesh(const Mesh &mesh)
	{
		draw_mesh(mesh);
		stroke();
	}

	void EPSCanvas::fill_mesh(const Mesh &mesh, const std::vector<Real> &color, const bool in_doubt_force_face_color)
	{
		bool use_face_color = in_doubt_force_face_color && mesh.n_nodes() == mesh.n_elements();
		if(std::size_t(mesh.n_nodes()*3) == color.size() && !use_face_color)
		{
			std::vector<Real> x(3), y(3), cols(3*3);

			for(Integer i = 0; i < mesh.n_elements(); ++i)
			{
				if(mesh.el_ptr[i+1]-mesh.el_ptr[i] != 3)
				{
					std::cerr<<"[Error] this method works only for triangular meshes"<<std::endl;
					return;
				}


				for(Integer j=mesh.el_ptr[i], index = 0; j < mesh.el_ptr[i+1]; ++j, ++index)
				{
					const Integer v_index=mesh.el_index[j];

					x[index] = mesh.points[v_index*mesh.dim];
					y[index] = mesh.points[v_index*mesh.dim+1];

					assert(color[v_index*3] >= 0 && color[v_index*3] <= 1);
					assert(color[v_index*3+1] >= 0 && color[v_index*3+1] <= 1);
					assert(color[v_index*3+2] >= 0 && color[v_index*3+2] <= 1);

					cols[index*3] = color[v_index*3];
					cols[index*3+1] = color[v_index*3+1];
					cols[index*3+2] = color[v_index*3+2];
				}

				fill_triangle(&x[0], &y[0], &cols[0]);
			}
		}
		else if(std::size_t(mesh.n_elements()*3) == color.size())
		{
			std::vector<Real> x, y;

			for(Integer i = 0; i < mesh.n_elements(); ++i)
			{
				
				x.resize(mesh.n_nodes(i));
				y.resize(mesh.n_nodes(i));

				for(Integer j=mesh.el_ptr[i], index = 0; j < mesh.el_ptr[i+1]; ++j, ++index)
				{
					const Integer v_index=mesh.el_index[j];

					x[index] = mesh.points[v_index*mesh.dim];
					y[index] = mesh.points[v_index*mesh.dim+1];
				}

				set_color(color[3*i], color[3*i+1], color[3*i+2]);
				fill_polygon(&x[0], &y[0], x.size());
			}
		}
		else
		{
			assert(false);
			std::cerr << "[Error] color and mesh are not compatible" << std::endl;
		}
	}

	void EPSCanvas::write(std::ostream &os)
	{
		os <<"%!PS-Adobe-3.0 EPSF-3.0\n";
		os << "%%BoundingBox: "<< minX_ << " " << minY_ << " " << maxX_ <<" "<< maxY_ <<"\n\n"; 
		os << "%%Pages: 1\n";
		os << "%%Page: 1 1\n";

		os << content_.str();
	}


	bool EPSCanvas::write(const Path &path)
	{
		std::ofstream file;  

		file.open (path.c_str());
		if(!file.good()) {
			file.close();
			std::cerr << "Could not write file " << path << std::endl;
			return false;
		}

		write(file);

		file.close();

		return true;
	}


	void EPSCanvas::begin_shape(const std::string &name)
	{
		content_ << " /"<<name<<" {\n";
	}

	void EPSCanvas::end_shape() 
	{
		content_ <<"} def\n\n";
	}

	void EPSCanvas::draw_shape(const std::string &name)
	{
		content_<<name<<"\n\n\n";
	}

	void EPSCanvas::update_box(const Real x, const Real y)
	{
		using std::min;
		using std::max;

		if (clipping_)
			return;

		minX_ = min(minX_, x-line_width_); 
		minY_ = min(minY_, y-line_width_);

		maxX_ = max(maxX_, x+line_width_);
		maxY_ = max(maxY_, y+line_width_);
	}
}