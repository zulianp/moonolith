#include "moonolith_plotter.hpp"


#include <cassert>

namespace moonolith
{
    template<class Canvas>
    void plot_aux(const axis_data &axis, const legend &legend, const plot_data *datas, const Integer n_data, Canvas &canvas, const plot_options &opts, const bool invert_h)
    {
        assert(axis.axis[0]<axis.axis[1]);
        assert(axis.axis[2]<axis.axis[3]);

        Real axis_min_x = axis.axis[0];
        Real axis_max_x = axis.axis[1];

        Real axis_min_y = axis.axis[2];
        Real axis_max_y = axis.axis[3];

        Real scaling_x = opts.scaling_x;
        Real scaling_y = opts.scaling_y;

        std::vector<Real> ticks_x = axis.ticks_x;
        std::vector<Real> ticks_y = axis.ticks_y;

        if(axis.log_x)
        {
            axis_min_x = log10(axis_min_x);
            axis_max_x = log10(axis_max_x);

            scaling_x *= (axis.axis[1]-axis.axis[0])/(axis_max_x-axis_min_x);

            for(std::size_t i=0; i<ticks_x.size(); ++i)
                ticks_x[i]=log10(axis.ticks_x[i]);
        }


        if(axis.log_y)
        {
            axis_min_y = log10(axis_min_y);
            axis_max_y = log10(axis_max_y);

            scaling_y *= (axis.axis[3]-axis.axis[2])/(axis_max_y-axis_min_y);

            for(std::size_t i=0; i<ticks_y.size(); ++i)
                ticks_y[i]=log10(axis.ticks_y[i]);
        }

        axis_min_x *= scaling_x;
        axis_max_x *= scaling_x;

        axis_min_y *= scaling_y;
        axis_max_y *= scaling_y;

        for(std::size_t i=0; i<ticks_x.size(); ++i)
            ticks_x[i]*=scaling_x;

        for(std::size_t i=0; i<ticks_y.size(); ++i)
            ticks_y[i]*=scaling_y;

        const Real width  = axis_max_x - axis_min_x;
        const Real height = axis_max_y - axis_min_y;

        if(opts.bottom_line && !opts.show_box)
        {
            canvas.set_color(0,0,0);
            canvas.set_line_width(opts.border_thickness);

            canvas.stroke_line(opts.label_offset-opts.line_width_offset, opts.line_width_offset + height, opts.label_offset-opts.line_width_offset+width, opts.line_width_offset + height);
        }

        for(Integer i=0; i<n_data; ++i)
        {
            const plot_data &data = datas[i];

            if(data.n_data<=0) continue;

            std::vector<Real> x(data.x, data.x+data.n_data);
            std::vector<Real> y(data.y, data.y+data.n_data);

            assert(x.size()==y.size());

            for(std::size_t i=0; i<x.size(); ++i)
            {
                if(axis.log_x)
                    x[i] = log10(x[i]);

                if(axis.log_y)
                    y[i] = log10(y[i]);

                x[i] *= scaling_x;
                y[i] *= scaling_y;

                x[i] = x[i] - axis_min_x + opts.label_offset - opts.line_width_offset;
                y[i] = y[i] - axis_min_y + opts.line_width_offset;

                if(invert_h)
                    y[i] = height - y[i];
            }


            canvas.begin_shape(data.name);

            if(data.show_line){
                canvas.set_color(data.line_color[0], data.line_color[1], data.line_color[2]);
                canvas.set_line_width(data.line_width);

                if(data.dashing<0) canvas.clear_dashing();
                else canvas.set_dashing(data.dashing);

                canvas.stroke_poly_line(&x[0], &y[0], x.size());
            }

            if(data.show_marker)
            {
                canvas.clear_dashing();
                canvas.set_color(data.marker_color[0], data.marker_color[1], data.marker_color[2]);

                for(std::size_t i=0; i<x.size(); ++i)
                    canvas.fill_circle(x[i], y[i], data.marker_size);
            }

            canvas.end_shape();
            canvas.draw_shape(data.name);
        }


        if(opts.show_x_ticks_markers)
        {
            canvas.begin_shape("ticks_x");
            canvas.set_color(0, 0, 0);
            canvas.set_line_width(opts.tick_marker_thickness);

            const Real tys = invert_h?height:0 + opts.line_width_offset;
            const Real tye = tys + (invert_h?1:-1)*opts.ticks_marker_size;

            for(std::size_t i=0; i<ticks_x.size(); ++i)
            {
                const Real tx = ticks_x[i]+opts.label_offset - axis_min_x - opts.line_width_offset;
                canvas.stroke_line(tx, tys, tx, tye);
            }

            canvas.end_shape();
            canvas.draw_shape("ticks_x");
        }

        if(opts.show_ticks_x_labels && !axis.label_ticks_x.empty())
        {
            assert(axis.label_ticks_x.size() == ticks_x.size());

            canvas.begin_shape("ticks_x_label");
            canvas.set_color(0, 0, 0);

            for(std::size_t i=0; i<ticks_x.size(); ++i)
            {
                const Real tx = ticks_x[i]+opts.label_offset - axis_min_x - opts.line_width_offset;

            //text-anchor="middle" dy=".76em"
                canvas.draw_text(tx, invert_h?(height + opts.text_offset + opts.line_width_offset) : (-opts.text_offset - opts.line_width_offset), opts.label_ticks_font_size, opts.label_ticks_font_family, axis.label_ticks_x[i]);
            }

            canvas.end_shape();
            canvas.draw_shape("ticks_x_label");
        }



        if(opts.show_y_ticks_markers)
        {
            canvas.begin_shape("ticks_y");
            canvas.set_color(0, 0, 0);
            canvas.set_line_width(opts.tick_marker_thickness);

            const Real txs = opts.label_offset - opts.line_width_offset;
            const Real txe = txs - opts.ticks_marker_size;

            for(std::size_t i=0; i<ticks_y.size(); ++i)
            {
                Real ty = ticks_y[i] - axis_min_y + opts.line_width_offset;
                if(invert_h)
                    ty = height -ty;

                canvas.stroke_line(txs, ty, txe, ty);
            }

            canvas.end_shape();
            canvas.draw_shape("ticks_y");
        }



        if(opts.show_ticks_y_labels && !axis.label_ticks_y.empty())
        {
            assert(axis.label_ticks_y.size() == ticks_y.size());

            canvas.begin_shape("ticks_y_label");
            canvas.set_color(0, 0, 0);

            for(std::size_t i=0; i<ticks_y.size(); ++i)
            {
                Real ty = ticks_y[i] - axis_min_y + opts.line_width_offset;
                if(invert_h)
                    ty = height -ty;

            //text-anchor="end" dy="0.28em"
                canvas.draw_text(opts.label_offset - opts.text_offset - opts.line_width_offset, ty, opts.label_ticks_font_size, opts.label_ticks_font_family, axis.label_ticks_y[i]);
            }

            canvas.end_shape();
            canvas.draw_shape("ticks_y_label");
        }

        if(opts.show_box)
        {
            canvas.set_color(0, 0, 0);
            canvas.set_line_width(opts.border_thickness);

            canvas.stroke_rect(opts.label_offset-opts.line_width_offset, opts.line_width_offset, width, height);
        }


        if(legend.show)
        {
            //TODO use positinings
            Real legend_x = 0;
            Real legend_y = 0;

            const Real x = -legend.offset;
            const Real y = -legend.vertical_spacing*legend.h_offset-legend.offset;
            const Real w = legend.text_width+legend.line_length+legend.text_offset+2*legend.offset;
            const Real h = (n_data-1+legend.h_offset*2)*legend.vertical_spacing + 2*legend.offset;

            switch(legend.position)
            {
                case legend::SOUTH:
                {
                    legend_x = (width-w)/2.0+opts.label_offset-opts.line_width_offset-x;
                    legend_y = opts.line_width_offset + (invert_h ? (height - (n_data - legend.h_offset)*legend.vertical_spacing - legend.pos_y):((n_data - legend.h_offset)*legend.vertical_spacing - legend.pos_y+opts.ticks_marker_size));
                    break;
                }
                case legend::SOUTH_WEST:
                {
                    legend_x = legend.pos_x+opts.label_offset - opts.line_width_offset;
                    legend_y = opts.line_width_offset + (invert_h ? (height - (n_data - legend.h_offset)*legend.vertical_spacing - legend.pos_y):((n_data - legend.h_offset)*legend.vertical_spacing - legend.pos_y+opts.ticks_marker_size));
                    break;
                }
                case legend::NORTH_WEST:
                {
                    legend_x = legend.pos_x+opts.label_offset - opts.line_width_offset;
                    legend_y = opts.line_width_offset + (invert_h ? (legend.pos_y + opts.line_width_offset):(height+legend.offset+opts.line_width_offset-h));
                    break;
                }
                case legend::NORTH:
                {
                    legend_x = (width-w)/2.0+opts.label_offset-opts.line_width_offset-x;
                    legend_y = opts.line_width_offset + (invert_h ? (legend.pos_y + opts.line_width_offset):(height+legend.offset+opts.line_width_offset-h));
                    break;
                }
            }

            canvas.begin_shape("legend");
            canvas.translate(legend_x, legend_y);

            canvas.set_color(0, 0, 0);
            
            canvas.set_line_width(legend.box_line_width);
            canvas.stroke_rect(x, y, w, h);

            for(Integer i=0; i<n_data; ++i)
            {
                const plot_data &data = datas[i];
                const Real pos = i*legend.vertical_spacing;

                canvas.set_color(data.line_color[0], data.line_color[1], data.line_color[2]);
                canvas.set_line_width(data.line_width);

                if(data.dashing<0) canvas.clear_dashing();
                else canvas.set_dashing(data.dashing);


                canvas.stroke_line(0, pos, legend.line_length, pos);

                canvas.set_color(0,0,0);
                //text-anchor="start" dy="0.2em"
                canvas.draw_text(legend.line_length+legend.text_offset, pos+ (invert_h?1:-1)*legend.font_size/4, legend.font_size, legend.font_family, data.name, false);

                //TODO maybe add dots
            }


            canvas.end_shape();
            canvas.draw_shape("legend");
        }

    }


    void plot(const axis_data &axis, const legend &legend, const plot_data *datas, const Integer n_data, EPSCanvas &canvas, const plot_options &opts)
    {
        plot_aux(axis, legend, datas, n_data, canvas, opts, false);
    }

    void plot(const axis_data &axis, const legend &legend, const plot_data *datas, const Integer n_data, SVGCanvas &canvas, const plot_options &opts)
    {
        plot_aux(axis, legend, datas, n_data, canvas, opts, true);
    }

}
