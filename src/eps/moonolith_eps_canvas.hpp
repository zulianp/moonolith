#ifndef MOONOLITH_EPS_CANVAS_HPP
#define MOONOLITH_EPS_CANVAS_HPP

#include "moonolith_mesh.hpp"
#include "moonolith_path.hpp"

#include <string>
#include <sstream>
#include <vector>


namespace moonolith {
	class Path;

	class EPSCanvas {
	public:
		EPSCanvas();

		void clear();

		void init();

		void set_line_width(const Real line_width);

		void set_dashing(const Real dash);
		void set_dashing(const Real *dash, const Integer n);
		void clear_dashing();

		void set_color(const Real red, const Real green, const Real blue);

		void translate(const Real x, const Real y);
		void scale(const Real x, const Real y);

		void begin_clip_rect(const Real x, const Real y, const Real w, const Real h);
		void end_clip_rect();


		void stroke_rect(const Real x, const Real y, const Real w, const Real h);
		void fill_rect(const Real x, const Real y, const Real w, const Real h);

		void draw_circle(const Real x, const Real y, const Real radius);
		void stroke_circle(const Real x, const Real y, const Real radius);
		void fill_circle(const Real x, const Real y, const Real radius);

		void draw_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end);
		void stroke_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end);

		void draw_poly_line(const Real *x, const Real *y, const Integer n);
		void stroke_poly_line(const Real *x, const Real *y, const Integer n);
		void stroke_poly_line(const Real *xy, const Integer n);
		inline void stroke_poly_line(const std::vector<Real> &xy) { stroke_poly_line(&xy[0],xy.size()/2); }

		void draw_polygon(const Real *x, const Real *y, const Integer n);
		void draw_polygon(const Real *xy, const Integer n);
		inline void draw_polygon(const std::vector<Real> &xy) { draw_polygon(&xy[0],xy.size()/2); }

		void stroke_polygon(const Real *x, const Real *y, const Integer n);
		void fill_polygon(const Real *x, const Real *y, const Integer n);

		void stroke_polygon(const Real *xy, const Integer n);
		void fill_polygon(const Real *xy, const Integer n);

		inline void stroke_polygon(const std::vector<Real> &xy) { stroke_polygon(&xy[0],xy.size()/2); }
		inline void fill_polygon(const std::vector<Real> &xy) { fill_polygon(&xy[0],xy.size()/2); }
		

		void fill_triangle(const Real *x, const Real *y, const Real *colors);
		void draw_text(const Real x, const Real y, const Real fontSize, const std::string &fontFamily, const std::string &text, const bool centered = true);
		void draw_image(const int *pixels, const Integer w, const Integer h);

		void draw_mesh(const Mesh &mesh);
		void stroke_mesh(const Mesh &mesh);
		void fill_mesh(const Mesh &mesh, const std::vector<Real> &color, const bool in_doubt_force_face_color = true);

		void move_to(const Real x, const Real y);
		void continue_segment(const Real x, const Real y);

		

		void begin_shape(const std::string &name);
		void end_shape();
		void draw_shape(const std::string &name);

		void stroke();
		void fill();
		void close_path();

		void save_state();
		void restore_state();


		void write(std::ostream &os);
		bool write(const Path &path);
		// bool write(const std::string &path);


		void update_box(const Real x, const Real y);
	private:
		Real minX_, minY_;
		Real maxX_, maxY_;
		std::stringstream content_;
		Real line_width_;
		bool clipping_;

		Real current_scale_;
	};
}

#endif