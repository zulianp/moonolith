#include "moonolith_svg_canvas.hpp"

#include <cassert>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

#include "moonolith_path.hpp"

namespace moonolith {

	std::string random_string( size_t length )
	{
		auto randchar = []() -> char
		{
			const char charset[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";
			const size_t max_index = (sizeof(charset) - 1);
			return charset[ rand() % max_index ];
		};
		std::string str(length,0);
		std::generate_n( str.begin(), length, randchar );
		return str;
	}


	SVGCanvas::SVGCanvas()
	: minX_(0), minY_(0), maxX_(0), maxY_(0),
	clipping_(false), current_scale_(1), 
	line_width_(1), dashing_(""), color_("black")
	{ }

	void SVGCanvas::clear()
	{
		minX_=0;
		minY_=0;

		maxX_=0;
		maxY_=0;

		clipping_ = false;
		current_scale_ = 1;

		content_.str().clear();
		content_.clear();

		line_width_=1;
		dashing_="";
		color_="black";
		
		transform_.str().clear();
		transform_.clear();
	}



	void SVGCanvas::set_line_width(const Real line_width)
	{
		assert(line_width>0);
		line_width_ = line_width*current_scale_;
	}

	void SVGCanvas::set_dashing(const Real dash)
	{
		dashing_ = std::to_string(dash) + ", " + std::to_string(dash);
	}


	void SVGCanvas::set_dashing(const Real *dash, const Integer n)
	{
		assert(n>0);
		std::stringstream ss;

		for(Integer i=0; i < n; ++i)
			ss << dash[i] << ", ";

		dashing_ = ss.str();
	}

	void SVGCanvas::clear_dashing()
	{
		dashing_ = "";
	}

	void SVGCanvas::set_color(const Real red, const Real green, const Real blue)
	{
		assert(red >= 0);
		assert(red <= 1);

		assert(green >= 0);
		assert(green <= 1);

		assert(blue >= 0);
		assert(blue <= 1);

		std::stringstream ss;
		ss<<"rgb(" << red * 255 << ", "<< green * 255<< ", " << blue * 255 << ")";

		color_ = ss.str();
	}

	void SVGCanvas::translate(const Real x, const Real y)
	{
		transform_ << " translate("<<x<<", "<<y<<")";
	}

	void SVGCanvas::scale(const Real x, const Real y)
	{
		using std::min;
		current_scale_ = min(1.0/x,1.0/y);
		transform_ <<" scale("<<x<<", "<<y<<" )";
	}

	void SVGCanvas::begin_clip_rect(const Real x, const Real y, const Real w, const Real h)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		clipping_ = true;
		clip_id_ = "c_"+random_string(10);

		content_<< "<defs>\n";
		content_<< "\t<clipPath id=\""<<clip_id_<<"\">\n";
		content_<< "\t<rect x=\""<<x<<"\" y=\""<<y<<"\" width=\""<<w<<"\" height=\""<<h<<"\" />\n";
		content_<< "\t</clipPath>\n";
		content_<< "</defs>\n\n";		
	}

	void SVGCanvas::end_clip_rect()
	{
		clipping_ = false;
		clip_id_ = "";
	}

	void SVGCanvas::set_options(const std::string &style)
	{
		content_ << " stroke-width=\""<<line_width_<<"\" stroke-dasharray=\""<<dashing_<<"\" style=\""<<style<<"\" transform=\""<<transform_.str()<<"\" ";

		if(clipping_)
		{
			assert(!clip_id_.empty());

			content_ << " clip-path=\"url(#"<<clip_id_<<")\" ";
		}
	}

	void SVGCanvas::stroke_rect(const Real x, const Real y, const Real w, const Real h, const std::string &style)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		content_ << "<rect x=\""<<x<<"\" y=\""<<y<<"\" ";
		content_ << "width=\""<<w<<"\" height=\""<<h<<"\" ";
		content_ << "stroke=\""<<color_<<"\" fill=\"none\"";
		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::fill_rect(const Real x, const Real y, const Real w, const Real h, const std::string &style)
	{
		update_box(x, y);
		update_box(x+w, y+h);

		content_ << "<rect x=\""<<x<<"\" y=\""<<y<<"\" ";
		content_ << "width=\""<<w<<"\" height=\""<<h<<"\" ";
		content_ << "fill=\""<<color_<<"\" stroke=\"none\"";
		set_options(style);
		content_ << "/>\n";
	}


	void SVGCanvas::stroke_circle(const Real x, const Real y, const Real radius, const std::string &style)
	{
		update_box(x-radius, y-radius);
		update_box(x+radius, y+radius);

		content_ << "<circle cx=\""<<x<<"\" cy=\""<<y<<"\" ";
		content_ << "r=\""<< radius <<"\" ";
		content_ << "stroke=\""<<color_<<"\" fill=\"none\"";
		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::fill_circle(const Real x, const Real y, const Real radius, const std::string &style)
	{
		update_box(x-radius, y-radius);
		update_box(x+radius, y+radius);

		content_ << "<circle cx=\""<<x<<"\" cy=\""<<y<<"\" ";
		content_ << "r=\""<< radius <<"\" ";
		content_ << "fill=\""<<color_<<"\" stroke=\"none\"";
		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::draw_text(const Real x, const Real y, const Real fontSize, const std::string &fontFamily, const std::string &text, const bool centered, const std::string &style)
	{
		content_ << "<text x=\""<<x<<"\" y=\""<<y<<"\"";
		content_ << " font-family=\""<<fontFamily<<"\" font-size=\""<<fontSize<<"\" ";
		content_ << "fill=\""<<color_<<"\" stroke=\"none\"";
		if(centered)
			content_ << " text-aligh = \"center\" ";
		set_options(style);
		content_ << ">"<<text<<"</text>\n";

		update_box(x,y);
		update_box(x,y+fontSize);
	}

	void SVGCanvas::stroke_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end, const std::string &style)
	{
		update_box(x_begin, y_begin);
		update_box(x_end,   y_end);

		content_ << "<line x1=\""<<x_begin<<"\" y1=\""<<y_begin<<"\" ";
		content_ << "x2=\""<<x_end<<"\" y2=\""<<y_end<<"\" ";
		content_ << "stroke=\""<<color_<<"\" fill=\"none\" ";
		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::stroke_poly_line(const Real *x, const Real *y, const Integer n, const std::string &style)
	{
		assert(n>1);

		content_ << "<polyline fill=\"none\" stroke=\""<<color_<<"\" points=\"";
		
		for(Integer i=0; i < n; ++i){
			update_box(x[i], y[i]);
			content_<<x[i]<<", "<<y[i]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::stroke_poly_line(const Real *xy, const Integer n, const std::string &style)
	{
		assert(n>1);

		content_ << "<polyline fill=\"none\" stroke=\""<<color_<<"\" points=\"";

		for(Integer i=0; i < n; ++i){
			update_box(xy[2*i], xy[2*i+1]);
			content_<<xy[2*i]<<", "<<xy[2*i+1]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::fill_triangle(const Real *x, const Real *y, const Real *colors, const std::string style)
	{
		

		const Real x1 = x[0];
		const Real x2 = x[1];
		const Real x3 = x[2];

		const Real y1 = y[0];
		const Real y2 = y[1];
		const Real y3 = y[2];

		update_box(x1,y1);
		update_box(x2,y2);
		update_box(x3,y3);

		const Real avgx1 = (x2+x3)/2;
		const Real avgy1 = (y2+y3)/2;

		const Real avgx2 = (x1+x3)/2;
		const Real avgy2 = (y1+y3)/2;

		const Real avgx3 = (x1+x2)/2;
		const Real avgy3 = (y1+y2)/2;

		std::stringstream c1, c2, c3;
		c1 <<"rgb(" << colors[0] * 255 << ", "<< colors[1] * 255<< ", " << colors[2] * 255 << ")";
		c2 <<"rgb(" << colors[3] * 255 << ", "<< colors[4] * 255<< ", " << colors[5] * 255 << ")";
		c3 <<"rgb(" << colors[6] * 255 << ", "<< colors[7] * 255<< ", " << colors[8] * 255 << ")";

		const std::string id1=random_string(10);
		const std::string id2=random_string(10);
		const std::string id3=random_string(10);
		const std::string id4=random_string(10);
		const std::string id5=random_string(10);
		const std::string id6=random_string(10);


		content_ << "<g>\n";
		content_ << "\t<defs>\n";
		content_ << "\t\t<linearGradient id=\""<<id1<<"\" gradientUnits=\"userSpaceOnUse\" x1=\""<<x1<<"\" y1=\""<<y1<<"\" x2=\""<<avgx1<<"\" y2=\""<<avgy1<<"\">\n";
		content_ << "\t\t\t<stop offset=\"0%\" stop-color=\""<<c1.str()<<"\"/>\n";
		content_ << "\t\t\t<stop offset=\"100%\" stop-color=\"#000000\" />\n";
		content_ << "\t\t</linearGradient>\n";

		content_ << "\t\t<linearGradient id=\""<<id2<<"\" gradientUnits=\"userSpaceOnUse\" x1=\""<<x2<<"\" y1=\""<<y2<<"\" x2=\""<<avgx2<<"\" y2=\""<<avgy2<<"\">\n";
		content_ << "\t\t\t<stop offset=\"0%\" stop-color=\""<<c2.str()<<"\"/>\n";
		content_ << "\t\t\t<stop offset=\"100%\" stop-color=\"#000000\" />\n";
		content_ << "\t\t</linearGradient>\n";

		content_ << "\t\t<linearGradient id=\""<<id3<<"\" gradientUnits=\"userSpaceOnUse\" x1=\""<<x3<<"\" y1=\""<<y3<<"\" x2=\""<<avgx3<<"\" y2=\""<<avgy3<<"\">\n";
		content_ << "\t\t\t<stop offset=\"0%\" stop-color=\""<<c3.str()<<"\"/>\n";
		content_ << "\t\t\t<stop offset=\"100%\" stop-color=\"#000000\" />\n";
		content_ << "\t\t</linearGradient>\n\n";

		content_ << "\t\t<path id=\""<<id4<<"\" d=\"M "<<x1<<","<<y1<<" L "<<x2<<","<<y2<<" "<<x3<<","<<y3<<" Z\" fill=\"url(#"<<id1<<")\"/>\n";
		content_ << "\t\t<path id=\""<<id5<<"\" d=\"M "<<x1<<","<<y1<<" L "<<x2<<","<<y2<<" "<<x3<<","<<y3<<" Z\" fill=\"url(#"<<id2<<")\"/>\n\n";
		
		content_ << "\t\t<filter id=\""<<id6<<"\">\n";
		content_ << "\t\t\t<feImage xlink:href=\"#"<<id4<<"\" result=\"layerA\" x=\"0\" y=\"0\" />\n";
		content_ << "\t\t\t<feImage xlink:href=\"#"<<id5<<"\" result=\"layerB\" x=\"0\" y=\"0\" />\n";
		content_ << "\t\t\t<feComposite in=\"layerA\" in2=\"layerB\" operator=\"arithmetic\" k1=\"0\" k2=\"1.0\" k3=\"1.0\" k4=\"0\" result=\"temp\"/>\n";
		content_ << "\t\t\t<feComposite in=\"temp\" in2=\"SourceGraphic\" operator=\"arithmetic\" k1=\"0\" k2=\"1.0\" k3=\"1.0\" k4=\"0\"/>\n";
		content_ << "\t\t</filter>\n";
		content_ << "\t</defs>\n\n";
		content_ << "\t<path d=\"M "<<x1<<","<<y1<<" L "<<x2<<","<<y2<<" "<<x3<<","<<y3<<" Z\" fill=\"url(#"<<id3<<")\" filter=\"url(#"<<id6<<")\" stroke=\"none\" shape-rendering=\"crispEdges\"";
		set_options(style);
		content_ << "/>\n";
		content_ << "</g>\n";

	}


	void SVGCanvas::draw_image(const int *pixels, const Integer w, const Integer h)
	{

		(void) pixels;
		(void) w;
		(void) h;

		std::cerr<<"Not implemented"<<std::endl;

		//TODO
		// assert(w>0);
		// assert(h>0);

		// update_box(w, h);

		// content_ << "/picstr 768 string def\n";
		// content_ << w << " " << h << " scale\n";
		// content_ << w << " " << h << " 8 [" << w << " 0 0 " <<  -h << " 0 " << h << "] \n";
		// content_ << "{\n\tcurrentfile\t\npicstr readhexstring pop\n}";
		// content_ << "false 3\n";
		// content_ << "colorimage\n";
		// for(Integer i = 0; i < w*h; ++i) {
		// 	content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+0];
		// 	content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+1];
		// 	content_ << std::setfill('0') << std::setw(2) << std::hex << pixels[i*3+2];
		// }

		// content_ << "\n";
	}


	void SVGCanvas::stroke_polygon(const Real *x, const Real *y, const Integer n, const std::string &style)
	{
		assert(n>2);

		content_ << "<polygon fill=\"none\" stroke=\""<<color_<<"\" points=\"";
		
		for(Integer i=0; i < n; ++i){
			update_box(x[i], y[i]);
			content_<<x[i]<<", "<<y[i]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::fill_polygon(const Real *x, const Real *y, const Integer n, const std::string &style)
	{
		assert(n>2);

		content_ << "<polygon stroke=\"none\" fill=\""<<color_<<"\" points=\"";
		
		for(Integer i=0; i < n; ++i){
			update_box(x[i], y[i]);
			content_<<x[i]<<", "<<y[i]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "\"/>\n";
	}

	void SVGCanvas::stroke_polygon(const Real *xy, const Integer n, const std::string &style)
	{
		assert(n>2);

		content_ << "<polygon fill=\"none\" stroke=\""<<color_<<"\" points=\"";

		for(Integer i=0; i < n; ++i){
			update_box(xy[2*i], xy[2*i+1]);
			content_<<xy[2*i]<<", "<<xy[2*i+1]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::fill_polygon(const Real *xy, const Integer n, const std::string &style)
	{
		assert(n>2);

		content_ << "<polygon stroke=\"none\" fill=\""<<color_<<"\" points=\"";

		for(Integer i=0; i < n; ++i){
			update_box(xy[2*i], xy[2*i+1]);
			content_<<xy[2*i]<<", "<<xy[2*i+1]<<" ";
		}
		content_ << "\"";

		set_options(style);
		content_ << "/>\n";
	}

	void SVGCanvas::stroke_mesh(const Mesh &mesh, const std::string &style)
	{
		if(mesh.dim!=2)
			std::cerr<<"[Warning] ignoring higher dimentions"<<std::endl;

		std::vector<Real> face;

		for(Integer i = 0; i < mesh.n_elements(); ++i)
		{
			face.clear();

			for(Integer j=mesh.el_ptr[i]; j < mesh.el_ptr[i+1]; ++j)
			{
				const Integer index=mesh.el_index[j];

				face.push_back(mesh.points[index*mesh.dim]);
				face.push_back(mesh.points[index*mesh.dim+1]);
			}

			stroke_polygon(face, style);
		}
	}

	void SVGCanvas::fill_mesh(const Mesh &mesh, const std::vector<Real> &color, const std::string &style)
	{
		if(std::size_t(mesh.n_nodes()*3) == color.size())
		{
			std::vector<Real> x(3), y(3), cols(3*3);

			for(Integer i = 0; i < mesh.n_elements(); ++i)
			{
				if(mesh.el_ptr[i+1]-mesh.el_ptr[i] != 3)
				{
					std::cerr<<"[Error] this method works only for triangular meshes"<<std::endl;
					return;
				}


				for(Integer j=mesh.el_ptr[i], index = 0; j < mesh.el_ptr[i+1]; ++j, ++index)
				{
					const Integer v_index=mesh.el_index[j];

					x[index] = mesh.points[v_index*mesh.dim];
					y[index] = mesh.points[v_index*mesh.dim+1];

					assert(color[v_index*3] >= 0 && color[v_index*3] <= 1);
					assert(color[v_index*3+1] >= 0 && color[v_index*3+1] <= 1);
					assert(color[v_index*3+2] >= 0 && color[v_index*3+2] <= 1);

					cols[index*3] = color[v_index*3];
					cols[index*3+1] = color[v_index*3+1];
					cols[index*3+2] = color[v_index*3+2];
				}
				fill_triangle(&x[0], &y[0], &cols[0], style);
			}
		}
		else if(std::size_t(mesh.n_elements()*3) == color.size())
		{
			std::vector<Real> x, y;

			for(Integer i = 0; i < mesh.n_elements(); ++i)
			{

				x.resize(mesh.n_nodes(i));
				y.resize(mesh.n_nodes(i));

				for(Integer j=mesh.el_ptr[i], index = 0; j < mesh.el_ptr[i+1]; ++j, ++index)
				{
					const Integer v_index=mesh.el_index[j];

					x[index] = mesh.points[v_index*mesh.dim];
					y[index] = mesh.points[v_index*mesh.dim+1];
				}

				set_color(color[3*i], color[3*i+1], color[3*i+2]);
				fill_polygon(&x[0], &y[0], x.size(), style);
			}
		}
		else
		{
			assert(false);
			std::cerr << "[Error] color and mesh are not compatible" << std::endl;
		}
	}

	void SVGCanvas::write(std::ostream &os)
	{
		os <<"<?xml version=\"1.0\" standalone=\"no\"?>\n";
		os <<"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
		os <<"<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" ";
		os << "width=\""<< (maxX_ - minX_) <<"\" height=\""<< (maxY_ - minY_) <<"\" ";
		os << "x=\""<< minX_ <<"\" y=\""<< minY_ <<"\">\n";

		os << content_.str();

		os << "</svg>\n";
	}


	bool SVGCanvas::write(const Path &path)
	{
		std::ofstream file;  

		file.open (path.c_str());
		if(!file.good()) {
			file.close();
			std::cerr << "Could not write file " << path << std::endl;
			return false;
		}

		write(file);

		file.close();

		return true;
	}


	void SVGCanvas::begin_shape(const std::string &name)
	{
		content_ << "<g id=\""<<name<<"\" >\n";
	}

	void SVGCanvas::end_shape() 
	{
		content_ <<"</g>\n";
	}


	void SVGCanvas::update_box(const Real x, const Real y)
	{
		using std::min;
		using std::max;

		if (clipping_)
			return;

		minX_ = min(minX_, x-line_width_); 
		minY_ = min(minY_, y-line_width_);

		maxX_ = max(maxX_, x+line_width_);
		maxY_ = max(maxY_, y+line_width_);
	}
}