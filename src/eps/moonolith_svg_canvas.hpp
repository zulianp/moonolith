#ifndef MOONOLITH_SVG_CANVAS_HPP
#define MOONOLITH_SVG_CANVAS_HPP

#include "moonolith_mesh.hpp"
#include "moonolith_path.hpp"

#include <string>
#include <sstream>
#include <vector>


namespace moonolith {
	class Path;

	class SVGCanvas {
	public:
		SVGCanvas();

		void clear();

		void set_line_width(const Real line_width);

		void set_dashing(const Real dash);
		void set_dashing(const Real *dash, const Integer n);
		void clear_dashing();

		void set_color(const Real red, const Real green, const Real blue);

		void translate(const Real x, const Real y);
		void scale(const Real x, const Real y);

		void begin_clip_rect(const Real x, const Real y, const Real w, const Real h);
		void end_clip_rect();



		void stroke_rect(const Real x, const Real y, const Real w, const Real h, const std::string &style = "");
		void fill_rect(const Real x, const Real y, const Real w, const Real h, const std::string &style = "");

		void stroke_circle(const Real x, const Real y, const Real radius, const std::string &style = "");
		void fill_circle(const Real x, const Real y, const Real radius, const std::string &style = "");

		void draw_text(const Real x, const Real y, const Real fontSize, const std::string &fontFamily, const std::string &text, const bool centered = true, const std::string &style = "");

		void stroke_line(const Real x_begin, const Real y_begin, const Real x_end, const Real y_end, const std::string &style = "");
		void stroke_poly_line(const Real *x, const Real *y, const Integer n, const std::string &style = "");
		void stroke_poly_line(const Real *xy, const Integer n, const std::string &style = "");

		void fill_triangle(const Real *x, const Real *y, const Real *colors, const std::string style = "");


		void draw_image(const int *pixels, const Integer w, const Integer h);

		void stroke_polygon(const Real *x, const Real *y, const Integer n, const std::string &style = "");
		void fill_polygon(const Real *x, const Real *y, const Integer n, const std::string &style = "");
		void stroke_polygon(const Real *xy, const Integer n, const std::string &style = "");
		void fill_polygon(const Real *xy, const Integer n, const std::string &style = "");
		inline void stroke_polygon(const std::vector<Real> &xy, const std::string &style = "") { stroke_polygon(&xy[0],xy.size()/2, style); }
		inline void fill_polygon(const std::vector<Real> &xy, const std::string &style = "") { fill_polygon(&xy[0],xy.size()/2, style); }

		void stroke_mesh(const Mesh &mesh, const std::string &style = "");
		void fill_mesh(const Mesh &mesh, const std::vector<Real> &color, const std::string &style = "");

		void write(std::ostream &os);
		bool write(const Path &path);

		void begin_shape(const std::string &name);
		void end_shape();
		void draw_shape(const std::string &name) { (void) name; }

	private:
		void update_box(const Real x, const Real y);
		void set_options(const std::string &style = "");

		Real minX_, minY_;
		Real maxX_, maxY_;
		std::stringstream content_;
		bool clipping_;

		Real current_scale_;

		Real line_width_;
		std::string dashing_;
		std::string color_;
		std::stringstream transform_;
		std::string clip_id_;
	};
}

#endif