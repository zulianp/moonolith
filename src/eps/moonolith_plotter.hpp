#ifndef MOONOLITH_PLOTTER_HPP
#define MOONOLITH_PLOTTER_HPP

#include "moonolith_algebra.hpp"

#include "moonolith_eps_canvas.hpp"
#include "moonolith_svg_canvas.hpp"

#include <vector>
#include <string>
#include <cmath>


namespace moonolith
{
    struct axis_data
    {
        Real axis[4]; //min_x, max_x, min_y, max_y
        std::vector<Real> ticks_x, ticks_y;
        std::vector<std::string> label_ticks_x, label_ticks_y;
        bool log_x, log_y;
        bool use_integral_values_x;

        axis_data()
        {
            log_x = false;
            log_y = false;
            use_integral_values_x = false;
        }


        void init_ticks(const Integer n_ticks_x, const Integer n_ticks_y, const Integer n_decimals = 1)
        {
            lin_space(axis[0], axis[1], n_ticks_x, ticks_x);
            lin_space(axis[2], axis[3], n_ticks_y, ticks_y);

            label_ticks_x.resize(ticks_x.size());
            label_ticks_y.resize(ticks_y.size());

            Real tmp = pow(10, n_decimals); 

            for(std::size_t i=0; i<ticks_x.size(); ++i){
                std::string str;
                if(use_integral_values_x) {
                    str = std::to_string(int(ticks_x[i]));
                } else {
                    str = std::to_string(round(ticks_x[i]*tmp)/tmp);
                    str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
                }

                label_ticks_x[i]=str;
            }

            for(std::size_t i=0; i<ticks_y.size(); ++i){
                std::string str = std::to_string(round(ticks_y[i]*tmp)/tmp);
                str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
                label_ticks_y[i]=str;
            }
        }
    };

    struct plot_options
    {
        Real tick_marker_thickness;
        bool show_x_ticks_markers, show_y_ticks_markers;
        bool show_ticks_x_labels, show_ticks_y_labels;
        Real border_thickness;
        Real ticks_marker_size;
        bool bottom_line, show_box;
        Real label_ticks_font_size;
        std::string label_ticks_font_family;


        Real label_offset;
        Real line_width_offset;
        Real text_offset;

        Real scaling_x, scaling_y;



        plot_options()
        {
            tick_marker_thickness = 0.5;

            show_x_ticks_markers = true;
            show_y_ticks_markers = true;

            show_ticks_x_labels = true;
            show_ticks_y_labels = true;

            ticks_marker_size = 3;

            border_thickness = 0.5;

            bottom_line = true;
            show_box = true;

            label_ticks_font_size = 5;
            label_ticks_font_family = "Utopia";


            label_offset = 40;

            line_width_offset = 0;
            text_offset = 10;

            scaling_x = 150;
            scaling_y = 150;

        }

        void fit_to_size(const axis_data &axis, const Real w, const Real h)
        {
            scaling_x = w/(axis.axis[1]-axis.axis[0]);
            scaling_y = h/(axis.axis[3]-axis.axis[2]);
        }
    };

    

    struct legend
    {

        enum positioning
        {
            SOUTH,
            SOUTH_WEST,
            NORTH_WEST,
            NORTH
        };

        positioning position;
        Real pos_x, pos_y;
        Real text_width;
        Real h_offset;
        
        bool show;


        Real offset;
        Real vertical_spacing;
        Real line_length;

        Real box_line_width;
        Real text_offset;

        Real font_size;
        std::string font_family;

        legend()
        {
            position = SOUTH_WEST;
            show = true;
            text_width = 40; //TODO should be computed automatically

            pos_x = 10;
            pos_y = 10;

            h_offset = 0.3;


            text_offset = 4;
            offset = 5;
            vertical_spacing = 12;
            line_length = 13;

            box_line_width = 0.5;

            font_size = 10;
            font_family = "Utopia";
        }
    };


    struct plot_data
    {
        Real *x;
        Real *y;
        Integer n_data;
        std::string name;

        bool show_line;
        Real line_color[3];
        Real line_width;
        Real dashing;

        bool show_marker;
        Real marker_color[3];
        Real marker_size;

        plot_data()
        {
            n_data = 0;
            name = "data";

            show_line = true;
            line_width = 1;
            dashing = -1;
            line_color[0]=line_color[1]=line_color[1] = 0;

            show_marker = false;
            marker_color[0]=marker_color[1]=marker_color[1] = 0;
            marker_size = 3;
        }
    };



    void plot(const axis_data &axis, const legend &legend, const plot_data *datas, const Integer n_data, EPSCanvas &canvas, const plot_options &opts = plot_options());
    void plot(const axis_data &axis, const legend &legend, const plot_data *datas, const Integer n_data, SVGCanvas &canvas, const plot_options &opts = plot_options());
}


#endif //MOONOLITH_PLOTTER_HPP
