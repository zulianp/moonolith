#ifndef MOONOLITH_TP_B_SPLINE_HPP
#define MOONOLITH_TP_B_SPLINE_HPP

#include "moonolith_algebra.hpp"

#include <vector>
#include <cassert>

namespace moonolith {


	class TensorProductBSpline {
	private:
		Integer degree_u_, degree_v_;
		TensorIndex tensor_index_;

		std::vector<Real> knots_u_, knots_v_;
		std::vector<Real> control_points_;

	public:
		inline Integer degree_u() const { return degree_u_; }
		inline Integer degree_v() const { return degree_v_; }
		inline Integer dim() const { return tensor_index_[2]; }

		inline const std::vector<Real> &controlPoints() const { return control_points_; }

		void init(const std::vector<Real> &knots_u, const std::vector<Real> &knots_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim = 2);
		void init(const Integer degree_u, const Integer degree_v, const std::vector<Real> &knots_u, const std::vector<Real> &knots_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim = 2);

		void interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const;
		void interpolate(const Real u, const Real v, std::vector<Real> &result) const;

		void derivative(TensorProductBSpline &dx, TensorProductBSpline &dy) const;

	private:
		inline Real ctrl_pts_at(const Integer i, const Integer j, const Integer k) const
		{
			return control_points_[tensor_index_.index_for(i,j,k)];
		}
	};
}
#endif //MOONOLITH_TP_B_SPLINE_HPP
