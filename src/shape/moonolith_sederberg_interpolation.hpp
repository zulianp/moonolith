#ifndef MOONOLITH_SEDERBERG_INTERPOLATION_HPP
#define MOONOLITH_SEDERBERG_INTERPOLATION_HPP


//
//  SederbergInterpolation taken from http://dl.acm.org/citation.cfm?id=166117.166118


#include <vector>


namespace moonolith {

	void polygon_orient(std::vector<Real> &polygon, const bool cw);


	std::vector<Real> polygon_barycenter(const std::vector<Real> &polygon);
	void polygon_set_barycenter(const Real bx, const Real by, std::vector<Real> &polygon);

	Real polygon_area(const std::vector<Real> &polygon);
	void polygon_set_area(const Real new_area, std::vector<Real> &polygon);

	Real polygon_diameter(const std::vector<Real> &polygon);
	void polygon_fit_to_size(const Real size, std::vector<Real> &polygon);


	void polygon_compute_intrinsic_definition(const std::vector<Real> &polygon, std::vector<Real> &edges, std::vector<Real> &angles, const bool compute_first_angle);
	void polygon_build_from_intrinsic_definition(const std::vector<Real> &edges, const std::vector<Real> &angles, const Real p_x, const Real p_y, std::vector<Real> &polygon);

	void serderberg_interpolate(const std::vector<Real> &poly_0, const std::vector<Real> &poly_1, const Real t, std::vector<Real> &poly);
}

#endif
