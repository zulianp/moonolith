#include "moonolith_maximum_entropy.hpp"

#include "moonolith_algebra.hpp"


#include <cmath>
#include <cassert>

namespace moonolith {
    Real Zi(const Real x, const Real y, const Real mi, const std::vector<Real> &lambda)
    {
        const Real dot_product = lambda[0]*x+lambda[1]*y;
        return mi * exp(-dot_product);
    }


    bool check_boundary(const std::vector<Real> &polygon, const Integer j, const Real px, const Real py, const Real tol, std::vector<Real> &b, MaxiumumEntropyMemoryValues &mem, const bool force)
    {
        const Integer n_boundary = polygon.size()/2;

        std::vector<Real> &radii = mem.radii;
        std::vector<Real> &segments = mem.segments;
        std::vector<Real> &mat = mem.mat;

        for(Integer i = 0; i < n_boundary; ++i) {
            segments[2*i] = polygon[2*i] - px;
            segments[2*i+1] = polygon[2*i+1] - py;

            radii[i] = sqrt(segments[2*i]*segments[2*i]+segments[2*i+1]*segments[2*i+1]);

            if(radii[i] < tol) {
                b[j*n_boundary+i] = 1;
                return true;
            }
        }

        Real min_area=std::numeric_limits<Real>::max();
        Integer min_index=-1;

        for(Integer i = 0; i < n_boundary; ++i) 
        {
            const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

            mat[0] = segments[i*2];
            mat[1] = segments[i*2+1];

            mat[2] = segments[ip1*2];
            mat[3] = segments[ip1*2+1];

            const Real area = det_2(&mat[0]);
            const Real product = segments[i*2]*segments[ip1*2]+segments[i*2+1]*segments[ip1*2+1];

            if(product < 0)
            {
                if(fabs(area) < tol)
                {
                    const Real denominator = 1.0/(radii[i] + radii[ip1]);

                    b[j*n_boundary+i] = radii[ip1] * denominator;
                    b[j*n_boundary+ip1] = radii[i] * denominator;

                    return true;
                }

                if(area<min_area)
                {
                    min_index=i;
                    min_area=area;
                }
            }
        }

        if(!force)
            return false;


        assert(min_index>=0);

        if(min_index < 0) min_index=0; //TODO

        const Integer i = min_index;
        const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

        const Real denominator = 1.0/(radii[i] + radii[ip1]);

        b[j*n_boundary+i] = radii[ip1] * denominator;
        b[j*n_boundary+ip1] = radii[i] * denominator;


        return true;
    }

    void prior_functions(const Integer n_boundary, MaxiumumEntropyMemoryValues &mem)
    {
        std::vector<Real> &rho = mem.rho;
        std::vector<Real> &radii = mem.radii;
        std::vector<Real> &edges = mem.edges;

        for(Integer i = 0; i < n_boundary; ++i) 
        {
            const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);
            rho[i] = radii[i] + radii[ip1] - edges[i];
        }

        Real pi_tilde = 0.0;

        for(Integer i = 0; i < n_boundary; ++i) {
            const Integer im1 = i == 0? (n_boundary-1) : (i-1);

            const Real denom = rho[im1] * rho[i];

            mem.m[i] = 1.0 / denom;
            pi_tilde += mem.m[i];
        }

        for(Integer i = 0; i < n_boundary; ++i)
            mem.m[i] /= pi_tilde;
    }

    bool solve_optimization_problem(const std::vector<Real> &polygon, const Real px, const Real py, MaxiumumEntropyMemoryValues &mem, const MaxiumumEntropyOptions &opts)
    {
        const Integer n_boundary = polygon.size()/2;

        std::vector<Real> &lambda = mem.lambda;
        std::vector<Real> &delta_lambda = mem.delta_lambda;
        std::vector<Real> &gradient = mem.gradient;
        std::vector<Real> &hessian = mem.hessian;

        std::vector<Real> &Zis = mem.Zis;

        lambda[0]=lambda[1]=0;

        Real alpha = 1;

        for(Integer k = 0; k < opts.newton_solver_max_iter; ++k) {
////////////////Gradient
            gradient[0]=gradient[1]=0;

            for(Integer i = 0; i < n_boundary; ++i) {
                const Real x = polygon[2*i]-px;
                const Real y = polygon[2*i+1]-py;

                Zis[i] = Zi(x, y, mem.m[i], lambda);
                gradient[0] -= Zis[i] * x;
                gradient[1] -= Zis[i] * y;
            }
//////////////

            const Real grad_norm = sqrt(gradient[0]*gradient[0]+gradient[1]*gradient[1]);
            if(grad_norm < opts.newton_solver_eps) break; //Newton converged


//////////////////////hessian
            for(Integer i = 0; i < n_boundary; ++i) {
                const Real x = polygon[2*i]-px;
                const Real y = polygon[2*i+1]-py;

                const Real Z_i = Zis[i];

                hessian[0]              += Z_i * x * x;
                hessian[1] = hessian[2] += Z_i * x * y;
                hessian[3]              += Z_i * y * y;
            }
/////////////////////


/////////////////////solve linear system
            const Real hessian_determinant = det_2(&hessian[0]);

            if(fabs(hessian_determinant) > opts.hessian_esp)
            {
                if(fabs(hessian_determinant) < opts.eps)
                {
                    gradient[0] *= -1;
                    gradient[1] *= -1;

                    solve_2x2(&hessian[0], &gradient[0], &delta_lambda[0]);
                }
                else
                {
                    delta_lambda[1] = (hessian[2] * gradient[0] - gradient[1] * hessian[0]) / hessian_determinant;
                    delta_lambda[0] = (-gradient[0] - hessian[1] * delta_lambda[1]) / hessian[0];
                }

            }
            else
            {
                return false;
            }
/////////////////////

            // if(grad_norm > 1.0e-4) alpha /= 2;
            // else alpha = 1.0;

            lambda[0] = lambda[0] + alpha * delta_lambda[0];
            lambda[1] = lambda[1] + alpha * delta_lambda[1];


            if(std::isnan(lambda[0]) || std::isnan(lambda[1]))
                return false;
        }

        for(Integer i = 0; i < n_boundary; ++i) {
            const Real x = polygon[2*i]-px;
            const Real y = polygon[2*i+1]-py;

            Zis[i] = Zi(x, y, mem.m[i], lambda);
        }


        return true;
    }

    void maximum_entropy(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const MaxiumumEntropyOptions &opts)
    {
        MaxiumumEntropyMemoryValues mem;        
        maximum_entropy(polygon,points,b,mem,opts);
    }

    void maximum_entropy(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, MaxiumumEntropyMemoryValues &mem, const MaxiumumEntropyOptions &opts)
    {
        const Integer n_boundary = polygon.size()/2;
        const Integer n_points = points.size()/2;

        b.resize(n_boundary * n_points);
        std::fill(b.begin(), b.end(),0);

        mem.initialize(n_boundary);

        for(Integer i = 0; i < n_boundary; ++i) {
            const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

            const Real dx = polygon[i*2]-polygon[ip1*2];
            const Real dy = polygon[i*2+1]-polygon[ip1*2+1];

            mem.edges[i] = sqrt(dx*dx + dy*dy);
        }

        for(Integer j = 0; j < n_points; ++j)
        {
            const Real pjx =  points[j*2];
            const Real pjy =  points[j*2+1];

            if(check_boundary(polygon, j, pjx, pjy, opts.eps, b, mem, false))
                continue;

            prior_functions(n_boundary, mem);

            const bool ok=solve_optimization_problem(polygon, pjx, pjy, mem, opts);

            if(!ok)
            {
                if(check_boundary(polygon, j, pjx, pjy, opts.eps, b, mem, false))
                    continue;
            }


            Real Z = 0.0;

            for(Integer i = 0; i < n_boundary; ++i) {
                b[j*n_boundary+i] = mem.Zis[i];
                Z += b[j*n_boundary+i];
            }

            for(Integer k=0; k < n_boundary; ++k)
                b[j*n_boundary+k] /= Z;
        }
    }

}
