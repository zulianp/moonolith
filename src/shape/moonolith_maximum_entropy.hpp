#ifndef MOONOLITH_MAXIMUM_ENTROPY_HPP
#define MOONOLITH_MAXIMUM_ENTROPY_HPP

#include <vector>
#include <limits>

namespace moonolith {
    struct MaxiumumEntropyMemoryValues {
        std::vector<Real> gradient;
        std::vector<Real> lambda;
        std::vector<Real> delta_lambda;
        std::vector<Real> hessian;

        std::vector<Real> Zis;


        std::vector<Real> edges;
        std::vector<Real> rho;
        std::vector<Real> m;
        

        std::vector<Real> radii;
        std::vector<Real> segments;
        std::vector<Real> mat;

        void initialize(const Integer n)
        {
            gradient.resize(2);
            lambda.resize(2);
            delta_lambda.resize(2);
            hessian.resize(4);

            Zis.resize(n);


            edges.resize(n);
            rho.resize(n);
            m.resize(n);


            radii.resize(n);
            segments.resize(2*n);
            mat.resize(4);
        }

        bool isInitialized(const Integer n)
        {
            return radii.size() == std::size_t(n);
        }
    };


    struct MaxiumumEntropyOptions {
        Real eps;
        Real hessian_esp;

        Real newton_solver_max_iter;
        Real newton_solver_eps;

        MaxiumumEntropyOptions()
        {
            eps=1e-8;
            hessian_esp=1e-15;
            newton_solver_eps = 1e-12;

            newton_solver_max_iter = 100;
        }
    };

    void maximum_entropy(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const MaxiumumEntropyOptions &opts = MaxiumumEntropyOptions());
    void maximum_entropy(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, MaxiumumEntropyMemoryValues &mem, const MaxiumumEntropyOptions &opts = MaxiumumEntropyOptions());
}
    


#endif //MOONOLITH_MAXIMUM_ENTROPY_HPP

