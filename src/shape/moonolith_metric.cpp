#include "moonolith_metric.hpp"

#include "moonolith_algebra.hpp"

#include <cmath>
#include <cassert>

namespace moonolith
{

	void metric(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol)
	{
		MetricMemoryValues mem;
		metric(polygon, points, mem, b, tol);
	}

	void metric(const std::vector<Real> &polygon, const std::vector<Real> &points, MetricMemoryValues &mem, std::vector<Real> &b, const Real tol)
	{
		const Integer n_boundary = polygon.size()/2;
		const Integer n_points = points.size()/2;

		if(!mem.isInitialized(n_boundary)) {
			mem.initialize(n_boundary);
		}


		b.resize(n_boundary*n_points);


		std::vector<Real> &segments = mem.segments;
		std::vector<Real> &radii = mem.radii;

		std::vector<Real> &edges = mem.edges;

		std::vector<Real> &A = mem.A;
		std::vector<Real> &D = mem.D;

		std::vector<Real> &q = mem.q;
		std::vector<Real> &C = mem.C;
		std::vector<Real> &B = mem.B;

		std::vector<Real> &mat = mem.mat;

		for(Integer i = 0; i < n_boundary; ++i)
		{
			const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

			edges[2 * i]     = polygon[2 * ip1]     - polygon[2 * i];
			edges[2 * i + 1] = polygon[2 * ip1 + 1] - polygon[2 * i + 1];
		}

		for(Integer j=0; j < n_points; ++j)
		{
			const Real pjx =  points[j*2];
			const Real pjy =  points[j*2+1];

			bool value_setted=false;

			for(Integer i = 0; i < n_boundary; ++i) 
			{
				segments[2*i] = polygon[2*i] - pjx;
				segments[2*i+1] = polygon[2*i+1] - pjy;

				radii[i] = sqrt(segments[2*i]*segments[2*i]+segments[2*i+1]*segments[2*i+1]);

				if(radii[i] < tol) { //we are on the vertex
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					b[j*n_boundary+i] = 1;

					value_setted = true;
					break;
				}
			}

			if(value_setted)
				continue;



			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				mat[0] = segments[i*2];
				mat[1] = segments[i*2+1];

				mat[2] = segments[ip1*2];
				mat[3] = segments[ip1*2+1];

				A[i] = 0.5 * det_2(&mat[0]);
				D[i] = segments[i*2]*segments[ip1*2]+segments[i*2+1]*segments[ip1*2+1];

				if(fabs(A[i]) < tol && D[i] < 0) { //we are on the edge
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					const Real denominator = 1.0/(radii[i] + radii[ip1]);

					b[j*n_boundary+i] = radii[ip1] * denominator;
					b[j*n_boundary+ip1] = radii[i] * denominator;

					value_setted = true;
					break;
				}

				const Integer im1 = i == 0? (n_boundary-1) : (i-1);

				const Real eix = edges[2*i];
				const Real eiy = edges[2*i+1];

				const Real eimx = edges[2*im1];
				const Real eimy = edges[2*im1+1];

				q[i] = radii[i] + radii[ip1] - sqrt(eix*eix+eiy*eiy);
				assert(q[i] > 0);

				mat[0] = eix;
				mat[1] = eiy;

				mat[2] = eimx;
				mat[3] = eimy;

				C[i] = 0.5 * det_2(&mat[0]);


				mat[0] = segments[im1*2];
				mat[1] = segments[im1*2+1];

				mat[2] = segments[ip1*2];
				mat[3] = segments[ip1*2+1];
				
				B[i] = 0.5 * det_2(&mat[0]);
			}

			if(value_setted)
				continue;

			Real W = 0.0;

			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer im2 = (i + n_boundary - 2) % n_boundary;
				const Integer im1 = i == 0? (n_boundary-1) : (i-1);
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);


				const Real w = (A[im2] / (q[im2]*q[im1]*C[im1])) - (B[i] / (q[im1]*q[i]*C[i])) + (A[ip1] / (q[i]*q[ip1]*C[ip1]));

				b[j*n_boundary+i] = w;
				W += w;
			}

			for(Integer k=0; k<n_boundary; ++k)
				b[j*n_boundary+k] /= W;
		}
	}
}
