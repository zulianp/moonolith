#include "moonolith_meanvalue.hpp"

#include "moonolith_algebra.hpp"

#include <math.h>


namespace moonolith {

	void meanvalue(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol)
	{
		MeanvalueMemoryValues mem;
		meanvalue(polygon, points, mem, b, tol);
	}

	void meanvalue(const std::vector<Real> &polygon, const std::vector<Real> &points, MeanvalueMemoryValues &mem, std::vector<Real> &b, const Real tol)
	{			
		const Integer n_boundary = polygon.size()/2;
		const Integer n_points = points.size()/2;

		if(!mem.isInitialized(n_boundary)) {
			mem.initialize(n_boundary);
		}

		std::vector<Real> &segments = mem.segments;
		std::vector<Real> &radii = mem.radii;
		std::vector<Real> &areas = mem.areas;
		std::vector<Real> &products = mem.products;
		std::vector<Real> &tangents = mem.tangents;
		std::vector<Real> &mat = mem.mat;

		b.resize(n_boundary*n_points);
		std::fill(b.begin(), b.end(), 0);


		for(Integer j = 0; j < n_points; ++j)
		{
			const Real pjx =  points[j*2];
			const Real pjy =  points[j*2+1];

			bool value_setted = false;

			for(Integer i = 0; i < n_boundary; ++i) 
			{
				segments[2*i] = polygon[2*i] - pjx;
				segments[2*i+1] = polygon[2*i+1] - pjy;

				radii[i] = sqrt(segments[2*i]*segments[2*i]+segments[2*i+1]*segments[2*i+1]);

				if(radii[i] < tol) { //we are on the vertex
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					b[j*n_boundary+i] = 1;

					value_setted = true;
					break;
				}
			}

			if(value_setted) continue;

			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				mat[0] = segments[i*2];
				mat[1] = segments[i*2+1];

				mat[2] = segments[ip1*2];
				mat[3] = segments[ip1*2+1];

				areas[i] = det_2(&mat[0]);
				products[i] = segments[i*2]*segments[ip1*2]+segments[i*2+1]*segments[ip1*2+1];

				if(fabs(areas[i]) < tol && products[i] < 0) { //we are on the edge
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					const Real denominator = 1.0/(radii[i] + radii[ip1]);

					b[j*n_boundary+i] = radii[ip1] * denominator;
					b[j*n_boundary+ip1] = radii[i] * denominator;

					value_setted = true;
					break;
				}
			}


			if(value_setted) continue;


			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				tangents[i] = areas[i]/(radii[i]*radii[ip1] + products[i]);
			}


			Real W = 0;
			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer im1 = i == 0? (n_boundary-1) : (i-1);

				b[j*n_boundary+i] = (tangents[im1] + tangents[i])/radii[i];
				W += b[j*n_boundary+i];
			}

			for(Integer k=0; k<n_boundary; ++k)
				b[j*n_boundary+k] /= W;
		}
	}

	bool meanvalue_derivative(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, std::vector<Real> &derivatives, const Real tol)
	{
		MeanvalueMemoryGradients mem;
		return meanvalue_derivative(polygon, points, mem, b, derivatives, tol);
	}

	bool meanvalue_derivative(const std::vector<Real> &polygon, const std::vector<Real> &points, MeanvalueMemoryGradients &mem, std::vector<Real> &b, std::vector<Real> &derivatives, const Real tol)
	{
		const Integer n_boundary = polygon.size()/2;
		const Integer n_points = points.size()/2;


		if(!mem.isInitialized(n_boundary)) {
			mem.initialize(n_boundary);
		}


		b.resize(n_boundary*n_points);
		std::fill(b.begin(), b.end(), 0);

		derivatives.resize(n_boundary*n_points*2);
		std::fill(derivatives.begin(), derivatives.end(), 0);

		std::vector<Real> &segments = mem.segments;
		std::vector<Real> &radii = mem.radii;
		std::vector<Real> &products = mem.products;
		std::vector<Real> &areas = mem.areas;
		std::vector<Real> &tangents = mem.tangents;
		std::vector<Real> &mat = mem.mat;


		std::vector<Real> &areas_prime = mem.areas_prime;
		std::vector<Real> &products_prime = mem.products_prime;
		std::vector<Real> &radii_prime = mem.radii_prime;
		std::vector<Real> &tangents_prime = mem.tangents_prime;
		std::vector<Real> &w_prime = mem.w_prime;

		for(Integer j = 0; j < n_points; ++j)
		{
			const Real pjx =  points[j*2];
			const Real pjy =  points[j*2+1];

			bool value_setted = false;

			for(Integer i = 0; i < n_boundary; ++i) 
			{
				segments[2*i] = polygon[2*i] - pjx;
				segments[2*i+1] = polygon[2*i+1] - pjy;

				radii[i] = sqrt(segments[2*i]*segments[2*i]+segments[2*i+1]*segments[2*i+1]);

				if(radii[i] < tol) { //we are on the vertex
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					b[j*n_boundary+i] = 1;

					value_setted = true;
					break;
				}
			}

			if(value_setted) continue;

			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				mat[0] = segments[i*2];
				mat[1] = segments[i*2+1];

				mat[2] = segments[ip1*2];
				mat[3] = segments[ip1*2+1];



				products[i] = segments[i*2]*segments[ip1*2]+segments[i*2+1]*segments[ip1*2+1];
				areas[i] = det_2(&mat[0]);

				if(fabs(areas[i]) < tol && products[i] < 0) { //we are on the edge
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					const Real denominator = 1.0/(radii[i] + radii[ip1]);

					b[j*n_boundary+i] = radii[ip1] * denominator;
					b[j*n_boundary+ip1] = radii[i] * denominator;

					value_setted = true;
					//TODO add derivative
					break;
				}

				const Real vix = polygon[2*i];
				const Real viy = polygon[2*i+1];

				const Real vip1x = polygon[2*ip1];
				const Real vip1y = polygon[2*ip1+1];

				areas_prime[2*i]   = viy-vip1y;
				areas_prime[2*i+1] = vip1x-vix;

				products_prime[2*i]   = 2 * pjx - vix - vip1x;
				products_prime[2*i+1] = 2 * pjy - viy - vip1y;

				radii_prime[2*i]   = (pjx-vix)/radii[i];
				radii_prime[2*i+1] = (pjy-viy)/radii[i];
			}

			if(value_setted) continue;

			for(Integer i = 0; i < n_boundary; ++i)
			{
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				const Real denominator = radii[i] * radii[ip1] + products[i];

				const Real denominator_primex = radii_prime[2*i]*radii[ip1]+radii[i]*radii_prime[2*ip1] + products_prime[2*i];
				const Real denominator_primey = radii_prime[2*i+1]*radii[ip1]+radii[i]*radii_prime[2*ip1+1] + products_prime[2*i+1];

				tangents_prime[2*i]  =(areas_prime[2*i]*denominator  -areas[i]*denominator_primex)/(denominator*denominator);
				tangents_prime[2*i+1]=(areas_prime[2*i+1]*denominator-areas[i]*denominator_primey)/(denominator*denominator);

				tangents[i]=areas[i]/denominator;
			}

			Real W=0;
			Real W_primex = 0;
			Real W_primey = 0;


			for(Integer i = 0; i < n_boundary; ++i)
			{
				const Integer im1 = (i > 0) ? (i-1) : (n_boundary-1);

				w_prime[2*i] = ((tangents_prime[2*im1] + tangents_prime[2*i])*radii[i]-(tangents[im1]+tangents[i])*radii_prime[2*i])/(radii[i]*radii[i]);
				w_prime[2*i+1] = ((tangents_prime[2*im1+1] + tangents_prime[2*i+1])*radii[i]-(tangents[im1]+tangents[i])*radii_prime[2*i+1])/(radii[i]*radii[i]);

				W_primex += w_prime[2*i];
				W_primey += w_prime[2*i+1];

				b[j*n_boundary+i]=(tangents[im1]+tangents[i])/radii[i];
				W+=b[j*n_boundary+i];
			}

			for(Integer k=0; k<n_boundary; ++k){
				derivatives[j*n_boundary+2*k]=(w_prime[2*k]*W-b[j*n_boundary+k]*W_primex)/(W*W);
				derivatives[j*n_boundary+2*k+1]=(w_prime[2*k+1]*W-b[j*n_boundary+k]*W_primey)/(W*W);

				b[j*n_boundary+k] /= W;
			}

		}

		return true;
	}
}

