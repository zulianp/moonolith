#include "moonolith_sederberg_interpolation.hpp"

#include "moonolith_algebra.hpp"


#include <math.h>
#include <cassert>

namespace moonolith {

	void polygon_orient(std::vector<Real> &polygon, const bool cw)
	{
		std::vector<Real> edges;
		std::vector<Real> angles;
		polygon_compute_intrinsic_definition(polygon, edges, angles, true);

		Real sum=0;

		for(std::size_t i=0; i<angles.size(); ++i)
			sum += angles[i];

#ifndef NDEBUG
		const Real test = sum / M_PI;
		const Integer test_int = round(test);

		//The sum of angles must be a multiple of pi
		assert(fabs(test-test_int) < 1e-10);
#endif

		if(cw && sum<0) return;
		if(!cw && sum>0) return;

		std::vector<Real> tmp(polygon.size());

		const Integer n_vertices = polygon.size()/2;
		for(Integer i=0; i<n_vertices; ++i){
			tmp[2*(n_vertices-1-i)]=polygon[2*i];
			tmp[2*(n_vertices-1-i)+1]=polygon[2*i+1];
		}

		std::swap(polygon, tmp);
	}

	std::vector<Real> polygon_barycenter(const std::vector<Real> &polygon)
	{
		std::vector<Real> res(2);
		res[0]=0;
		res[1]=0;

		const Integer n_vertices = polygon.size()/2;
		assert(n_vertices>2);

		for(Integer i=0; i<n_vertices; ++i){
			res[0]+=polygon[2*i];
			res[1]+=polygon[2*i+1];
		}

		res[0] /= n_vertices;
		res[1] /= n_vertices;

		return res;
	}

	void polygon_set_barycenter(const Real bx, const Real by, std::vector<Real> &polygon)
	{
		std::vector<Real> delta=polygon_barycenter(polygon);

		delta[0] = bx-delta[0];
		delta[1] = by-delta[1];


		const Integer n_vertices = polygon.size()/2;
		assert(n_vertices>2);

		for(Integer i=0; i<n_vertices; ++i){
			polygon[2*i] += delta[0];
			polygon[2*i+1] += delta[1];
		}


	}

	Real polygon_area(const std::vector<Real> &polygon)
	{

		const std::vector<Real> barycenter = polygon_barycenter(polygon);
		const Integer n_vertices = polygon.size()/2;

		Real area=0;

		std::vector<Real> mat(4);

		for(Integer i=0; i<n_vertices; ++i)
		{
			const Integer ip1 = (i + 1) == n_vertices? 0 : (i+1);

			mat[0] = polygon[2*i] - barycenter[0];
			mat[1] = polygon[2*i+1] - barycenter[1];

			mat[2] = polygon[ip1*2] - barycenter[0];
			mat[3] = polygon[ip1*2+1] - barycenter[1];

			area += det_2(&mat[0]);
		}

		return area;
	}

	void polygon_set_area(const Real new_area, std::vector<Real> &polygon)
	{
		const Real current_area=polygon_area(polygon);
		const Real scaling=sqrt(fabs(new_area/current_area));

		for(std::size_t i=0; i<polygon.size(); ++i)
			polygon[i]*=scaling;
	}


	Real polygon_diameter(const std::vector<Real> &polygon)
	{
		Real result = 0;

		const Integer n_vertices = polygon.size()/2;
		assert(n_vertices>2);

		for(Integer i=0; i<n_vertices; ++i)
		{
			for(Integer j=0; j<n_vertices; ++j)
			{
				const Real dx = polygon[2*i] - polygon[2*j];
				const Real dy = polygon[2*i+1] - polygon[2*j+1];
				result = std::max(result, sqrt(dx*dx+dy*dy));
			}
		}

		return result;
	}

	void polygon_fit_to_size(const Real size, std::vector<Real> &polygon)
	{
		const Real diameter = polygon_diameter(polygon);
		const Real scaling = size/2.0/diameter;

		for(std::size_t i=0; i<polygon.size(); ++i)
			polygon[i]*=scaling;
	}


	void polygon_compute_intrinsic_definition(const std::vector<Real> &polygon, std::vector<Real> &edges, std::vector<Real> &angles, const bool compute_first_angle)
	{
		assert(polygon.size() > 5);

		const Integer n_edges = polygon.size()/2;
		edges.resize(n_edges);
		angles.resize(n_edges);


		std::vector<Real> prev(2), current(2), next(2);
		std::vector<Real> tmp(2*2);


		for(Integer i=0;i<n_edges;++i)
		{
			const Integer ip1 = (i + 1) == n_edges? 0 : (i+1);
			const Integer im1 = i == 0? (n_edges-1) : (i-1);

			if(!compute_first_angle)
			{
				//angle with x-axis for the first one, this to avoid that first = (1,0) and division by 0
				prev[0]=i==0?(-1+polygon[0]):polygon[2*(i-1)];
				prev[1]=i==0?polygon[1]     :polygon[2*(i-1)+1];
			}
			else
			{
				prev[0]=polygon[2*im1];
				prev[1]=polygon[2*im1+1];
			}

			current[0]=polygon[2*i];
			current[1]=polygon[2*i+1];

			next[0]=polygon[2*ip1]-current[0];
			next[1]=polygon[2*ip1+1]-current[1];

			prev[0]=current[0]-prev[0];
			prev[1]=current[1]-prev[1];

			edges[i]=sqrt(next[0]*next[0]+next[1]*next[1]);


			tmp[0] = prev[0];
			tmp[1] = next[0];

			tmp[2] = prev[1];
			tmp[3] = next[1];

			const Real dot = next[0]*prev[0]+next[1]*prev[1];

			angles[i]=atan2(det_2(&tmp[0]),dot);
		}
	}

	void polygon_build_from_intrinsic_definition(const std::vector<Real> &edges, const std::vector<Real> &angles, const Real p_x, const Real p_y, std::vector<Real> &polygon)
	{
		assert(edges.size()==angles.size());

		const Integer n_edges=edges.size();

		polygon.resize(2 * n_edges);
		polygon[0]=p_x;
		polygon[1]=p_y;

		for(Integer i=1;i<n_edges;++i)
		{
			polygon[2*i]   = polygon[2*(i-1)]   + edges[i-1] * cos(angles[i-1]);
			polygon[2*i+1] = polygon[2*(i-1)+1] + edges[i-1] * sin(angles[i-1]);
		}
	}



	void serderberg_interpolate(const std::vector<Real> &poly_0, const std::vector<Real> &poly_1, const Real t, std::vector<Real> &poly)
	{
		assert(t>=0);
		assert(t<=1);
		assert(poly_0.size() == poly_1.size());

		poly.resize(poly_0.size());

		if(t==0)
		{
			std::copy(poly_0.begin(), poly_0.end(), poly.begin());
			return;
		}
		else if(t==1)
		{
			std::copy(poly_1.begin(), poly_1.end(), poly.begin());
			return;
		}

		std::vector<Real> edges_0, edges_1;
		std::vector<Real> angles_0, angles_1;

		polygon_compute_intrinsic_definition(poly_0, edges_0, angles_0, false);
		polygon_compute_intrinsic_definition(poly_1, edges_1, angles_1, false);

		std::vector<Real> delta_edges(edges_0.size());
		std::vector<Real> delta_edges_squared(edges_0.size());


		Real max_delta = 0;
		Real max_edge = 0;

		for(std::size_t i=0; i<delta_edges.size(); ++i){
			delta_edges[i] = fabs(edges_0[i] - edges_1[i]);

			max_delta = std::max(delta_edges[i], max_delta);
			max_edge = std::max(max_edge, std::max(edges_0[i], edges_1[i]));
		}

		const Real edge_tol=max_delta*1e-4; 

		for(std::size_t i=0; i<delta_edges.size(); ++i)
			delta_edges[i]=std::max(edge_tol,delta_edges[i]); 

		for(std::size_t i=0; i<delta_edges.size(); ++i)
			delta_edges_squared[i]=delta_edges[i]*delta_edges[i];


		const std::vector<Real> poly0_bary = polygon_barycenter(poly_0);
		const std::vector<Real> poly1_bary = polygon_barycenter(poly_1);

		const Real new_bary_x = (1-t)*poly0_bary[0]+t*poly1_bary[0];
		const Real new_bary_y = (1-t)*poly0_bary[1]+t*poly1_bary[1];

		const Real new_area=(1-t)*polygon_area(poly_0) + t*polygon_area(poly_1);     

		const Real p0_x=poly_0[0];
		const Real p0_y=poly_0[1]; 

		 //interpolate the angles  
		std::vector<Real> angles(angles_0.size());
		for(std::size_t i=0; i<angles.size(); ++i)
			angles[i] = (1-t)*angles_0[i]+t*angles_1[i];

		 //interpolate edges
		std::vector<Real> edges(edges_0.size());
		for(std::size_t i=0; i<edges.size(); ++i)
			edges[i]=(1-t)*edges_0[i]+t*edges_1[i];


		const Integer size=angles.size();

		std::vector<Real> coss(size), sins(size);
		std::vector<Real> alphas(size);

		Real alpha=0;

		Real E=0;
		Real F=0;
		Real G=0;

		Real U=0;
		Real V=0;
		
		for(Integer i=0; i<size; ++i)
		{   
			alpha+=angles[i]; 
			alphas[i]=alpha; 
			coss[i]=cos(alpha);
			sins[i]=sin(alpha);

			const Real coss_squared = coss[i]*coss[i];
			const Real sins_coss = coss[i]*sins[i];
			const Real sins_squared = sins[i]*sins[i];

			E+=delta_edges_squared[i]*coss_squared;
			F+=delta_edges_squared[i]*sins_coss;
			G+=delta_edges_squared[i]*sins_squared;

			U+=2*edges[i]*coss[i];
			V+=2*edges[i]*sins[i];
		}

		const Real denominator=E*G-F*F;  

		if(fabs(denominator)>1e-8)
		{
			const Real lambda1=(U*G-F*V)/denominator;
			const Real lambda2=(E*V-U*F)/denominator;

			for(Integer i=0; i<size; ++i)
			{
				edges[i]-= 0.5 * delta_edges_squared[i]*(lambda1*coss[i]+lambda2*sins[i]);
			}
		}

		polygon_build_from_intrinsic_definition(edges,alphas,p0_x, p0_y, poly);
		polygon_set_area(new_area, poly);
		polygon_set_barycenter(new_bary_x, new_bary_y, poly);
	}
}
