#ifndef MOONOLITH_EXPONENTIAL_3_POINT_HPP
#define MOONOLITH_EXPONENTIAL_3_POINT_HPP

#include <vector>

namespace moonolith
{

		struct Exponential3PointMemoryValues {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> radiip;
		std::vector<Real> areas;
		std::vector<Real> Bs;

		std::vector<Real> mat;

		void initialize(const Integer n)
		{
			segments.resize(n*2);
			Bs.resize(n*2);
			radii.resize(n);
			radiip.resize(n);
			areas.resize(n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}
	};

	void exponential_3_point(const std::vector<Real> &polygon, const std::vector<Real> &points, const Real p, std::vector<Real> &b, const Real tol = 1e-8);
	void exponential_3_point(const std::vector<Real> &polygon, const std::vector<Real> &points, const Real p, Exponential3PointMemoryValues &mem, std::vector<Real> &b, const Real tol=1e-8);

}


#endif //MOONOLITH_EXPONENTIAL_3_POINT_HPP

