#ifndef MOONOLITH_DISCRETE_HARMONIC_HPP
#define MOONOLITH_DISCRETE_HARMONIC_HPP

#include <vector>

namespace moonolith
{

		struct DiscreteHarmonicMemoryValues {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> areas;
		std::vector<Real> Bs;

		std::vector<Real> mat;

		void initialize(const Integer n)
		{
			segments.resize(n*2);
			Bs.resize(n*2);
			radii.resize(n);
			areas.resize(n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}
	};

	void discrete_harmonic(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol = 1e-8);
	void discrete_harmonic(const std::vector<Real> &polygon, const std::vector<Real> &points, DiscreteHarmonicMemoryValues &mem, std::vector<Real> &b, const Real tol=1e-8);

}


#endif //MOONOLITH_DISCRETE_HARMONIC_HPP

