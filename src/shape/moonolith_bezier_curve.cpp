#include "moonolith_bezier_curve.hpp"

#include "moonolith_algebra.hpp"

#include <cmath>
#include <cassert>


namespace moonolith {

	void BezierCurve::init(const std::vector<Real> &control_points, Integer dim)
	{
		init(control_points.size()/dim-1, control_points, dim);
	}

	void BezierCurve::init(const Integer degree, const std::vector<Real> &control_points, Integer dim)
	{
		degree_ = degree;
		dim_ = dim;

		control_points_ = control_points;

		assert(degree_>0);
		assert(Integer(control_points_.size())/dim_ == degree_+1);
	}

	void BezierCurve::interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const
	{
		result.resize(dim_* ts.size());
		std::vector<Real> temp;

		for(std::size_t i = 0; i < ts.size(); ++i) {
			interpolate(ts[i], temp);

			for(Integer j=0; j < dim_; ++j)
				result[i*dim_ + j] = temp[j];
		}
	}

	void BezierCurve::interpolate(const Real t, std::vector<Real> &result) const
	{
		result.resize(dim_);
		std::fill(result.begin(), result.end(), 0);

		for(std::size_t i=0; i < control_points_.size()/dim_; ++i)
		{
			const Real bi = binomial_coefficient(degree_,i)*std::pow(t,i)*std::pow(1-t,degree_-i);

			for(Integer j=0; j < dim_; ++j)
				result[j] += bi * control_points_[i*dim_+j];
		}
	}


	void BezierCurve::derivative(BezierCurve &dx) const
	{
		const Integer new_n_control=control_points_.size()/dim_ - 1;
		std::vector<Real> new_control(dim_*new_n_control, 0);

		for(Integer i=0; i< new_n_control; ++i)
		{
			for(Integer j=0; j < dim_; ++j)
				new_control[i*dim_+j] = degree_ * (control_points_[(i+1)*dim_+j]-control_points_[i*dim_+j]);
		}


		dx.init(degree_-1, new_control, dim_);
	}

}
