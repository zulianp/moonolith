#ifndef MOONOLITH_METRIC_HPP
#define MOONOLITH_METRIC_HPP

#include <vector>

namespace moonolith
{
	struct MetricMemoryValues {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> edges;
		std::vector<Real> A;
		std::vector<Real> D;
		std::vector<Real> q;
		std::vector<Real> C;
		std::vector<Real> B;

		std::vector<Real> mat;

		void initialize(const Integer n)
		{
			segments.resize(2 * n);
			radii.resize(n);

			edges.resize(2 * n);

			A.resize(n);
			D.resize(n);
			q.resize(n);
			C.resize(n);
			B.resize(n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}
	};

	void metric(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol=1e-8);
	void metric(const std::vector<Real> &polygon, const std::vector<Real> &points, MetricMemoryValues &mem, std::vector<Real> &b, const Real tol=1e-8);

}
#endif //MOONOLITH_METRIC_HPP
