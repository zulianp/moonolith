#ifndef MOONOLITH_BEZIER_SURFACE_HPP
#define MOONOLITH_BEZIER_SURFACE_HPP

#include "moonolith_algebra.hpp"

#include <vector>

namespace moonolith {

	class BezierSurface {
	private:
		Integer degree_u_, degree_v_;
		TensorIndex tensor_index_;

		std::vector<Real> control_points_;

	public:
		inline Integer degree_u() const { return degree_u_; }
		inline Integer degree_v() const { return degree_v_; }
		inline Integer dim() const { return tensor_index_[2]; }

		inline const std::vector<Real> &controlPoints() const { return control_points_; }

		void init(const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim = 2);
		void init(const Integer degree_u, const Integer degree_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim = 2);

		void interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const;
		void interpolate(const Real u, const Real v, std::vector<Real> &result) const;

		void derivative(BezierSurface &dx, BezierSurface &dy) const;
	};
}
#endif //MOONOLITH_BEZIER_SURFACE_HPP
