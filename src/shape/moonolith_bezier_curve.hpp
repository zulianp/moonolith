#ifndef MOONOLITH_BEZIER_CURVE_HPP
#define MOONOLITH_BEZIER_CURVE_HPP

#include <vector>

namespace moonolith {

	class BezierCurve {
	private:
		Integer degree_;
		Integer dim_;

		std::vector<Real> control_points_;

	public:
		inline Integer degree() const { return degree_; }
		inline Integer dim() const { return dim_; }

		inline const std::vector<Real> &control_points() const { return control_points_; }


		void init(const std::vector<Real> &control_points, Integer dim = 2);
		void init(const Integer degree, const std::vector<Real> &control_points, Integer dim = 2);

		void interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const;
		void interpolate(const Real t, std::vector<Real> &result) const;

		void derivative(BezierCurve &dx) const;
	};
}

#endif //MOONOLITH_BEZIER_CURVE_HPP
