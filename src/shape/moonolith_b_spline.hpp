#ifndef MOONOLITH_B_SPLINE_HPP
#define MOONOLITH_B_SPLINE_HPP

#include <cassert>
#include <vector>

namespace moonolith {	
	class BSpline
	{
	private:
		Integer degree_;
		Integer dim_;
		std::vector<Real> knots_;
		std::vector<Real> control_points_;

	public:
		inline Integer degree() const { return degree_; }
		inline Integer dim() const { return dim_; }

		inline const std::vector<Real> &control_points() const { return control_points_; }

		void init(const std::vector<Real> &knots, const std::vector<Real> &control_points, Integer dim = 2);
		void init(const Integer degree, const std::vector<Real> &knots, const std::vector<Real> &control_points, Integer dim = 2);

		void interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const;
		void interpolate(const Real t, std::vector<Real> &result) const;

		void derivative(BSpline &result) const;

	private:
		Integer find_interval(const Real t) const;
		Integer first_interval(const Integer interval, const Integer degree) const;

		void find_edges(const Integer first_interval, const Integer degree, std::vector<Real> &edges) const;
		void create_new_edges(const std::vector<Real> &weigths, const std::vector<Real> &edges, std::vector<Real> &new_edges) const;

		void compute_weigth(const Real t, const Integer first_interval, const Integer degree, std::vector<Real> &weigths) const;
	};
}
#endif //MOONOLITH_B_SPLINE_HPP
