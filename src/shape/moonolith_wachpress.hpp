#ifndef MOONOLITH_WACHSPRESS_HPP
#define MOONOLITH_WACHSPRESS_HPP

#include "moonolith_wachpress.hpp"

#include <vector>

namespace moonolith
{
	struct WachspressMemoryValues {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> areas;
		std::vector<Real> Cs;

		std::vector<Real> mat;

		void initialize(const Integer n)
		{
			segments.resize(n*2);
			Cs.resize(n*2);
			radii.resize(n);
			areas.resize(n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}
	};

	void wachspress(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol = 1e-8);
	void wachspress(const std::vector<Real> &polygon, const std::vector<Real> &points, WachspressMemoryValues &mem, std::vector<Real> &b, const Real tol=1e-8);


}

#endif //MOONOLITH_WACHSPRESS_HPP
