#include "moonolith_bezier_surface.hpp"

#include <cassert>
#include <cmath>

namespace moonolith {

	
	void BezierSurface::init(const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim)
	{
		init(n_control_u-1, n_control_v-1, control_points, n_control_u, n_control_v, dim);
	}

	void BezierSurface::init(const Integer degree_u, const Integer degree_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim)
	{
		degree_u_ = degree_u;
		degree_v_ = degree_v;

		tensor_index_.init(n_control_u, n_control_v, dim);

		control_points_ = control_points;

		assert(degree_u_ > 0);
		assert(degree_v_ > 0);
		assert(n_control_u == degree_u_+1);
		assert(n_control_v == degree_v_+1);
	}

	void BezierSurface::interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const
	{
		const Integer n_t = ts.size()/2;
		assert(std::size_t(n_t*2) == ts.size());

		result.resize(dim() * n_t);
		std::vector<Real> temp;


		for(Integer i = 0; i < n_t; ++i) 
		{
			interpolate(ts[2*i], ts[2*i+1], temp);

			for(Integer j=0; j < dim(); ++j)
				result[i*dim() + j] = temp[j];
		}
	}

	void BezierSurface::interpolate(const Real u, const Real v, std::vector<Real> &result) const
	{
		result.resize(dim());
		std::fill(result.begin(), result.end(),0);

		for(Integer i=0; i < tensor_index_[0]; ++i)
		{
			const Real bi = binomial_coefficient(degree_u_,i) * std::pow(u,i) * std::pow(1-u, degree_u_-i);

			for(Integer j=0;j<tensor_index_[1];++j)
			{
				const Real bj=binomial_coefficient(degree_v_,j) * std::pow(v,j) * std::pow(1-v,degree_v_ - j);

				for(Integer k=0; k < dim(); ++k)
					result[k] += bi * bj * control_points_[tensor_index_.index_for(i,j,k)];
			}
		}
	}

	void BezierSurface::derivative(BezierSurface &dx, BezierSurface &dy) const
	{

		const TensorIndex ti_dx(tensor_index_[0] - 1, tensor_index_[1], dim());
		std::vector<Real> ctrl_dx(ti_dx.size());

		for(Integer j=0; j < ti_dx[1]; ++j)
		{
			for(Integer i=0; i < ti_dx[0]; ++i)
			{
				for(Integer k=0; k < dim(); ++k)
					ctrl_dx[ti_dx.index_for(i,j,k)] = degree_u_ * (control_points_[tensor_index_.index_for(i+1,j,k)] - control_points_[tensor_index_.index_for(i,j,k)]);
			}
		}
		dx.init(degree_u_ - 1, degree_v_, ctrl_dx, ti_dx[0], ti_dx[1], ti_dx[2]);



		const TensorIndex ti_dy(tensor_index_[0], tensor_index_[1] - 1, dim());
		std::vector<Real> ctrl_dy(ti_dy.size());

		for(Integer i=0; i < ti_dy[0]; ++i)
		{
			for(Integer j=0; j < ti_dy[1]; ++j)
			{
				for(Integer k=0; k < dim(); ++k)
					ctrl_dy[ti_dy.index_for(i,j,k)] = degree_v_ * (control_points_[tensor_index_.index_for(i,j+1,k)] - control_points_[tensor_index_.index_for(i,j,k)]);
			}
		}
		dy.init(degree_u_, degree_v_-1, ctrl_dy, ti_dy[0], ti_dy[1], ti_dy[2]);
	}
}
