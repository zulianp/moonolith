#include "moonolith_tp_b_spline.hpp"

#include "moonolith_b_spline.hpp"



namespace moonolith {

	void TensorProductBSpline::init(const std::vector<Real> &knots_u, const std::vector<Real> &knots_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim)
	{
		init(knots_u.size()-n_control_u-1, knots_v.size()-n_control_v-1, knots_u, knots_v, control_points, n_control_u, n_control_v, dim);
	}

	void TensorProductBSpline::init(const Integer degree_u, const Integer degree_v, const std::vector<Real> &knots_u, const std::vector<Real> &knots_v, const std::vector<Real> &control_points, const Integer n_control_u, const Integer n_control_v, const Integer dim)
	{
		degree_u_ = degree_u;
		degree_v_ = degree_v;

		tensor_index_.init(n_control_u, n_control_v, dim);

		knots_u_ = knots_u;
		knots_v_ = knots_v;

		control_points_ = control_points;

		assert(degree_u_>0);
		assert(degree_v_>0);
		assert(degree_u_ == Integer(knots_u_.size()) - tensor_index_[0] - 1);
		assert(degree_v_ == Integer(knots_v_.size()) - tensor_index_[1] - 1);
	}

	void TensorProductBSpline::interpolate(const std::vector<Real> &ts, std::vector<Real> &result) const
	{
		const Integer n_t = ts.size()/2;
		assert(std::size_t(n_t*2) == ts.size());

		result.resize(dim() * n_t);

		std::vector<Real> temp;

		for(Integer i = 0; i < n_t; ++i) 
		{
			interpolate(ts[2*i], ts[2*i+1], temp);

			for(Integer j=0; j < dim(); ++j)
				result[i*dim() + j] = temp[j];
		}
	}

	void TensorProductBSpline::interpolate(const Real u, const Real v, std::vector<Real> &result) const
	{
		BSpline bspline;

		std::vector<Real> tmp;
		std::vector<Real> tmp_control_u(dim() * tensor_index_[0]);
		std::vector<Real> tmp_control_v(dim() * tensor_index_[1]);


		for(Integer i = 0; i < tensor_index_[0]; ++i)
		{
			for(Integer j = 0; j < tensor_index_[1]; ++j){
				for(Integer k=0; k < dim(); ++k)
					tmp_control_v[j*dim()+k] = ctrl_pts_at(i, j, k);
			}

			bspline.init(degree_v_, knots_v_, tmp_control_v);
			bspline.interpolate(v, tmp);

			for(Integer k=0; k < dim(); ++k)
				tmp_control_u[i * dim() + k] = tmp[k];
		}

		bspline.init(degree_u_, knots_u_, tmp_control_u);
		bspline.interpolate(u, result);
	}


	void TensorProductBSpline::derivative(TensorProductBSpline &dx, TensorProductBSpline &dy) const
	{
		std::vector<Real> knot_dx(knots_u_.size()-2);
		for(std::size_t i = 1; i < knots_u_.size()-1; ++i)
			knot_dx[i-1] = knots_u_[i];

		const TensorIndex ti_dx(tensor_index_[0]-1, tensor_index_[1], dim());

		std::vector<Real> ctrl_dx( ti_dx.size() );

		for(Integer j = 0; j < ti_dx[1]; ++j)
		{
			for(Integer i = 0; i < ti_dx[0]; ++i)
			{
				for(Integer k=0; k < dim(); ++k)
					ctrl_dx[ti_dx.index_for(i,j,k)]=degree_u_/(knots_u_[i+degree_u_+1]-knots_u_[i+1]) * (ctrl_pts_at(i+1, j, k) - ctrl_pts_at(i, j, k));
			}
		}
		dx.init(degree_u_ - 1, degree_v_, knot_dx, knots_v_, ctrl_dx, ti_dx[0], ti_dx[1], ti_dx[2]);




		std::vector<Real> knot_dy(knots_v_.size()-2);
		for(std::size_t i = 1; i < knots_v_.size()-1; ++i)
			knot_dy[i-1] = knots_v_[i];

		const TensorIndex ti_dy(tensor_index_[0], tensor_index_[1] - 1, dim());

		std::vector<Real> ctrl_dy(ti_dy.size());
		for(Integer i = 0; i < ti_dy[0]; ++i)
		{
			for(Integer j = 0; j < ti_dy[1]; ++j)
			{
				for(Integer k = 0; k < dim(); ++k)
					ctrl_dy[ti_dy.index_for(i,j,k)]=degree_v_/(knots_v_[j+degree_v_+1]-knots_v_[j+1]) * (ctrl_pts_at(i, j+1, k) - ctrl_pts_at(i, j, k));
			}
		}
		dy.init(degree_u_, degree_v_ - 1, knots_u_, knot_dy, ctrl_dy, ti_dy[0], ti_dy[1], ti_dy[2]);
	}
}
