/**
 	@brief
		- explain what (data-layout)
		- example usage (file ref)


	bibtex-entry: 

	@Article{label,
   	author           = ...
   	title            = ...
   	journal          = ...
   	volume           = ...
   	number           = ...
   	year             = ...
   	pages            = ...
   	publisher        = ...
}

 */


#ifndef MOONOLITH_QUAD_HPP
#define MOONOLITH_QUAD_HPP

#include <vector>
#include <assert.h>

namespace moonolith {

	/*
	 * bi-linear functions in the reference quadrilateral
		(0,1)					(1, 1)
		________________________
	 	|						|
	 	|						|
	 	|						|
	 	|						|
	 	|						|
	 	|						|
	 	|						|
	 	|						|
	 	|						|
		|_______________________|
		(0, 0)					(1, 0)

		@fixme Needs fixing for shell elements
	*/

	int quad_n_functions();
	Real quad_fun_0(const Real *x);
	Real quad_fun_1(const Real *x);
	Real quad_fun_2(const Real *x);
	Real quad_fun_3(const Real *x);
	void quad_fun(const Real *x, Real *values);

	void quad_grad_0(const Real *x, Real * grad);
	void quad_grad_1(const Real *x, Real * grad);
	void quad_grad_2(const Real *x, Real * grad);
	void quad_grad_3(const Real *x, Real * grad);
	void quad_grad(const Real *x, Real *values);


	void quad_transform(const Real *points, const Real *in, Real *out, const int dim = 2);
	void quad_jacobian(const Real *points, const Real *in, Real *out);
	void quad_inverse_transform(const Real *points, const Real *in, Real *out, const Real tol = 1e-8);

	class RefQuad {
	public:
		RefQuad() {}
		inline static int n_functions() { return quad_n_functions(); }
		static Real fun(const int i, const Real *x);
		static void fun(const Real *x, Real *values);
		static void grad(const int i, const Real *x, Real *grad);

		//computes all reference gradients values in \mathbb{R}^{4 \times 2}
		static void grad(const Real *x, Real *values);
	};

	class Quad {
	public:
		Quad(const Real *points);

		inline static const RefQuad &reference() 
		{	
			static const RefQuad ref_;
			return ref_;
		}

		///grad in global coordinates evaluated on the reference element
		void grad(const int i, const Real *x, Real *grad);
		void grad(const Real *x, Real *grad);
		
		inline void transform(const Real *in, Real *out) const
		{
			quad_transform(points_, in, out);
		}

		inline void jacobian(const Real *in, Real *out) const
		{
			quad_jacobian(points_, in, out);
		}

		inline void inverse_transform(const Real *in, Real *out, const Real tol = 1e-8) const
		{
			quad_inverse_transform(points_, in, out, tol);
		}

		inline Real* points() { return points_; }
		inline const Real * points() const { return points_; }

	private:
		Real points_[4 * 2];

	};
}

#endif //MOONOLITH_QUAD_HPP
