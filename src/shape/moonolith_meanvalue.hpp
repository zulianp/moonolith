#ifndef MOONOLITH_MEAN_VALUE_HPP
#define MOONOLITH_MEAN_VALUE_HPP

#include <vector>

namespace moonolith {
	struct MeanvalueMemoryValues {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> areas;
		std::vector<Real> products;
		std::vector<Real> tangents;

		std::vector<Real> mat;

		void initialize(const Integer n)
		{
			segments.resize(n*2);
			radii.resize(n);
			areas.resize(n);
			products.resize(n);
			tangents.resize(n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}
	};

	struct MeanvalueMemoryGradients {
		std::vector<Real> segments;
		std::vector<Real> radii;
		std::vector<Real> areas;
		std::vector<Real> products;
		std::vector<Real> tangents;

		std::vector<Real> mat;

		

		std::vector<Real> areas_prime;
		std::vector<Real> products_prime;
		std::vector<Real> radii_prime;

		std::vector<Real> tangents_prime;
		std::vector<Real> w_prime;

		void initialize(const Integer n)
		{
			segments.resize(n*2);
			radii.resize(n);
			products.resize(n);
			areas.resize(n);

			tangents.resize(n);

			areas_prime.resize(2*n);
			products_prime.resize(2*n);
			radii_prime.resize(2*n);

			tangents_prime.resize(2*n);
			w_prime.resize(2*n);

			segments.resize(2*n);

			mat.resize(4);
		}

		bool isInitialized(const Integer n)
		{
			return radii.size() == std::size_t(n);
		}

	};

	void meanvalue(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol=1e-8);
	void meanvalue(const std::vector<Real> &polygon, const std::vector<Real> &points, MeanvalueMemoryValues &mem, std::vector<Real> &b, const Real tol=1e-8);

	bool meanvalue_derivative(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, std::vector<Real> &derivatives, const Real tol = 1e-8);
	bool meanvalue_derivative(const std::vector<Real> &polygon, const std::vector<Real> &points, MeanvalueMemoryGradients &mem, std::vector<Real> &b, std::vector<Real> &derivatives, const Real tol = 1e-8);

}


#endif //MOONOLITH_MEAN_VALUE_HPP

