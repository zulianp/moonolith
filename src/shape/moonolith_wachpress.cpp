#include "moonolith_wachpress.hpp"

#include "moonolith_algebra.hpp"
#include <cmath>

namespace moonolith
{
	void wachspress(const std::vector<Real> &polygon, const std::vector<Real> &points, std::vector<Real> &b, const Real tol)
	{
		WachspressMemoryValues mem;
		wachspress(polygon, points, mem, b, tol);
	}

	void wachspress(const std::vector<Real> &polygon, const std::vector<Real> &points, WachspressMemoryValues &mem, std::vector<Real> &b, const Real tol)
	{
		const Integer n_boundary = polygon.size()/2;
		const Integer n_points = points.size()/2;

		if(!mem.isInitialized(n_boundary)) {
			mem.initialize(n_boundary);
		}


		std::vector<Real> &radii = mem.radii;
		std::vector<Real> &areas = mem.areas;
		std::vector<Real> &Cs = mem.Cs;
		std::vector<Real> &segments = mem.segments;
		std::vector<Real> &mat = mem.mat;

		b.resize(n_boundary*n_points);
		std::fill(b.begin(), b.end(), 0);


		for(Integer i = 0; i < n_boundary; ++i) 
		{

			const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);
			const Integer im1 = i == 0? (n_boundary-1) : (i-1);

			mat[0] = polygon[2*im1]-polygon[2*i];
			mat[1] = polygon[2*ip1]-polygon[2*i];

			mat[2] = polygon[2*im1+1]-polygon[2*i+1];
			mat[3] = polygon[2*ip1+1]-polygon[2*i+1];

			Cs[i] = det_2(&mat[0]);
		}


		for(Integer j = 0; j < n_points; ++j) {
			const Real pjx =  points[j*2];
			const Real pjy =  points[j*2+1];

			bool value_setted = false;

			for(Integer i = 0; i < n_boundary; ++i) 
			{
				segments[2*i] = polygon[2*i] - pjx;
				segments[2*i+1] = polygon[2*i+1] - pjy;

				radii[i] = sqrt(segments[2*i]*segments[2*i]+segments[2*i+1]*segments[2*i+1]);

				if(radii[i] < tol) { //we are on the vertex
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					b[j*n_boundary+i] = 1;

					value_setted = true;
					break;
				}
			}

			if(value_setted) continue;

			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer ip1 = (i + 1) == n_boundary? 0 : (i+1);

				mat[0] = segments[i*2];
				mat[1] = segments[i*2+1];

				mat[2] = segments[ip1*2];
				mat[3] = segments[ip1*2+1];

				areas[i] = det_2(&mat[0]);

				if(fabs(areas[i]) < tol) { //we are on the edge
					for(Integer k=0; k<n_boundary; ++k)
						b[j*n_boundary+k] = 0;

					const Real denominator = 1.0/(radii[i] + radii[ip1]);

					b[j*n_boundary+i] = radii[ip1] * denominator;
					b[j*n_boundary+ip1] = radii[i] * denominator;

					value_setted = true;
					break;
				}
			}

			if(value_setted) continue;

			Real W = 0;
			for(Integer i = 0; i < n_boundary; ++i) {
				const Integer im1 = i == 0? (n_boundary-1) : (i-1);


				b[j*n_boundary+i] = Cs[i]/(areas[im1]*areas[i]);
				W += b[j*n_boundary+i];
			}

			for(Integer k=0; k<n_boundary; ++k)
				b[j*n_boundary+k] /= W;
		}
	}
}

