#include "moonolith_quad.hpp"

#include <assert.h>
#include <cmath>

#include "moonolith_algebra.hpp"

#define F_MINUS(x) (1.0-x)
#define F_PLUS(x)  (x)
#define D_MINUS(x) (-1.0)
#define D_PLUS(x) (1.0)

namespace moonolith {
	
	int quad_n_functions()
	{
		return 4;
	}
	
	Real quad_fun_0(const Real *x)
	{
		return F_MINUS(x[0]) * F_MINUS(x[1]);
	}
	
	Real quad_fun_1(const Real *x)
	{
		return F_PLUS(x[0]) * F_MINUS(x[1]);
	}
	
	Real quad_fun_2(const Real *x)
	{
		return F_PLUS(x[0]) * F_PLUS(x[1]);
	}
	
	Real quad_fun_3(const Real *x)
	{
		return F_MINUS(x[0]) * F_PLUS(x[1]);
	}
	
	void quad_fun(const Real *x, Real *values)
	{
		values[0] = quad_fun_0(x);
		values[1] = quad_fun_1(x);
		values[2] = quad_fun_2(x);
		values[3] = quad_fun_3(x);
	}
	
	void quad_grad_0(const Real *x, Real * grad)
	{
		grad[0] = D_MINUS(x[0]) * F_MINUS(x[1]);
		grad[1] = F_MINUS(x[0]) * D_MINUS(x[1]);
	}
	
	void quad_grad_1(const Real *x, Real * grad)
	{
		grad[0] = D_PLUS(x[0]) * F_MINUS(x[1]);
		grad[1] = F_PLUS(x[0]) * D_MINUS(x[1]);
	}
	
	void quad_grad_2(const Real *x, Real * grad)
	{
		grad[0] = D_PLUS(x[0]) * F_PLUS(x[1]);
		grad[1] = F_PLUS(x[0]) * D_PLUS(x[1]);
	}
	
	void quad_grad_3(const Real *x, Real * grad)
	{
		grad[0] = D_MINUS(x[0]) * F_PLUS(x[1]);
		grad[1] = F_MINUS(x[0]) * D_PLUS(x[1]);
	}

	void quad_grad(const Real *x, Real *values)
	{
		quad_grad_0(x, &values[0]);
		quad_grad_1(x, &values[2]);
		quad_grad_2(x, &values[4]);
		quad_grad_3(x, &values[6]);
	}
	
	void quad_transform(const Real *points, const Real *in, Real *out, const int dim)
	{
		std::fill(out, out + dim, Real(0));
		
		Real f_values[4];
		quad_fun(in, f_values);
		
		for(Integer i = 0; i < 4; ++i) {
			const Integer i_x_dim = i*dim;
			
			for (Integer d = 0; d < 2; ++d) {
				out[d] += points[i_x_dim + d] * f_values[i];
			}
		}
	}
	
	void quad_jacobian(const Real *points, const Real *in, Real *out)
	{
		
		Real outer_product_buff[2 * 2];
		Real grad 			   [2];
		
		quad_grad_0(in, grad);
		outer_product(2, 2, &points[0], grad, out);
			
		quad_grad_1(in, grad);
		outer_product(2, 2, &points[2], grad, outer_product_buff);
		
		for (Integer i = 0; i < 4; ++i) {
			out[i] += outer_product_buff[i];
		}
		
		quad_grad_2(in, grad);
		outer_product(2, 2, &points[4], grad, outer_product_buff);
		
		for (Integer i = 0; i < 4; ++i) {
			out[i] += outer_product_buff[i];
		}
		
		quad_grad_3(in, grad);
		outer_product(2, 2, &points[6], grad, outer_product_buff);
		
		for (Integer i = 0; i < 4; ++i) {
			out[i] += outer_product_buff[i];
		}
	}
	
	void quad_inverse_transform(const Real *points, const Real *in, Real *out, const Real tol)
	{
		const Integer max_newton_steps = 10;
		
		Real increment[2];
		Real gradient [2];
		Real hessian  [2 * 2];
		
		out[0] = 0;
		out[1] = 0;
		
		for(Integer i = 0; i < max_newton_steps; ++i) {
			quad_transform(points, out, gradient);
			
			gradient[0] -= in[0];
			gradient[1] -= in[1];
			
			quad_jacobian(points, out, hessian);
			solve_2x2(hessian, gradient, increment);
			
			out[0] -= increment[0];
			out[1] -= increment[1];
			
			if(std::sqrt(increment[0] * increment[0] +
					     increment[1] * increment[1]) < tol) {
				break;
			}
		}
	}
	
	Real RefQuad::fun(const int i, const Real *x)
	{
		typedef Real (*Fun)(const Real *);
		static const Fun fun_[4] =
		{
			&quad_fun_0,
			&quad_fun_1,
			&quad_fun_2,
			&quad_fun_3
		};
		
		return (*fun_[i])(x);
	}
	
	void RefQuad::fun(const Real *x, Real *values)
	{
		quad_fun(x, values);
	}
	
	void RefQuad::grad(const int i, const Real *x, Real *grad)
	{
		typedef void (*Grad)(const Real *, Real *);
		static const Grad grad_[4] =
		{
			&quad_grad_0,
			&quad_grad_1,
			&quad_grad_2,
			&quad_grad_3
		};
		
		(*grad_[i])(x, grad);
	}

 	void RefQuad::grad(const Real *x, Real *values)
 	{
 		quad_grad(x, values);
 	}
	
	Quad::Quad(const Real *points)
	{
		std::copy(points, points + 8, points_);
	}

	void Quad::grad(const int i, const Real *x, Real *grad)
	{
		Real ref_grad[2], jac[2 * 2], inv_jac[2 * 2];

		reference().grad(i, x, ref_grad);
		jacobian(x, jac);

		const Real det_jac = det_2(jac);
		inverse_2(jac, det_jac, inv_jac);

		grad[0] = ref_grad[0] * inv_jac[0] + ref_grad[1] * inv_jac[2];
		grad[1] = ref_grad[0] * inv_jac[1] + ref_grad[1] * inv_jac[3];
	}

	void Quad::grad(const Real *x, Real *grad)
	{
		Real ref_grad[4 * 2], jac[2 * 2], inv_jac[2 * 2];

		reference().grad(x, ref_grad);
		jacobian(x, jac);

		const Real det_jac = det_2(jac);
		assert(det_jac > 0);

		inverse_2(jac, det_jac, inv_jac);

		for(Integer i = 0; i < 4; ++i) {
			const int i2   = i * 2;
			const int i2p1 = i2 + 1;

			grad[i2]   = ref_grad[i2] * inv_jac[0] + ref_grad[i2p1] * inv_jac[2];
			grad[i2p1] = ref_grad[i2] * inv_jac[1] + ref_grad[i2p1] * inv_jac[3];
		}
	}
}

//clean macros
#undef F_MINUS
#undef F_PLUS
#undef D_MINUS
#undef D_PLUS


