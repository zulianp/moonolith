
#include "moonolith.hpp"

using namespace moonolith;

#include <stdlib.h>
#include <iostream>

#include <cmath>
#include "moonolith_mesh.hpp"
#include "moonolith_eps_canvas.hpp"
#include "moonolith_triangulator.hpp"
#include "moonolith_eps_marching_triangles.hpp"

#include "moonolith_test.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_triangulator.hpp"
#include "moonolith_tetrahedralizer.hpp"


#include "moonolith_poly_io.hpp"
#include "moonolith_obj_io.hpp"
#include "moonolith_mesh_io.hpp"

#include "moonolith_path.hpp"



// Real func_p2(const Real x, const Real y)
// {
// 	using std::pow;
// 	return -2 * (57 * x + 5 * y + 10 * y * y + 10 * x * x - 67) * (14 - 18 * y + 5 * x) * (4 + 2 * y + 5 * x) * (19 + 22 * y + 5 * x) * (-7 - 4 * y + 5 * x) / (6039047 * x - 4766300 * y + 3585514 * y * y + 5534505 * x * x + 5855565 * y * x + 596890 * y * y * x * x - 31680 * (int) pow((double) y, (double) 5) + 697500 * (int) pow((double) x, (double) 5) + 2788768 * (int) pow((double) y, (double) 3) - 6602675 * (int) pow((double) x, (double) 3) + 119125 * (int) pow((double) x, (double) 4) + 121280 * (int) pow((double) y, (double) 4) + 1654140 * y * x * x - 3186325 * y * (int) pow((double) x, (double) 3) - 4419224 * y * y * x - 3322488 * (int) pow((double) y, (double) 3) * x + 416700 * y * y * (int) pow((double) x, (double) 3) + 493320 * (int) pow((double) y, (double) 3) * x * x - 280800 * (int) pow((double) y, (double) 4) * x + 525000 * y * (int) pow((double) x, (double) 4) - 5787502);
// }

// Real func_1(const Real x, const Real y, const Real p)
// {
// 	using std::pow;
// 	return -0.1e1 / (0.35880e5 * (double) y * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.14300e5 * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.49000e5 * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.115500e6 * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.44800e5 * (double) y * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.66500e5 * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.101700e6 * (double) y * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.163000e6 * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.112670e6 * (double) y * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.21280e5 * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.50540e5 * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.3664e4 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.18520e5 * x * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.10165e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.7448e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) - 0.7028e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y - 0.8610e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x + 0.17680e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.64350e5 * x * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.4680e4 * (double) (int) pow((double) y, (double) 3) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.26000e5 * pow(x, 0.3e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.16250e5 * pow(x, 0.4e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.29000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.71500e5 * x * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.7200e4 * (double) (int) pow((double) y, (double) 3) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.7500e4 * pow(x, 0.3e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.12500e5 * pow(x, 0.4e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.8800e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.5000e4 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.12500e5 * pow(x, 0.4e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.16000e5 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.2500e4 * pow(x, 0.4e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.16720e5 * (double) (int) pow((double) y, (double) 3) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.11875e5 * pow(x, 0.4e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.3168e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 4) + 0.625e3 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.4e1) - 0.3168e4 * (double) (int) pow((double) y, (double) 4) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.21000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.114000e6 * x * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.78375e5 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.9980e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) + 0.3475e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x * x + 0.34624e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.21300e5 * x * x * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.77140e5 * (double) (y * y) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.26125e5 * pow(x, 0.3e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.12152e5 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) + 0.3750e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.3e1) + 0.9232e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.38200e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.16720e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) + 0.24700e5 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (y * y) - 0.10300e5 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) * x * x + 0.49020e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y - 0.7660e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y * x + 0.11080e5 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x + 0.52440e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (y * y) + 0.111150e6 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.4360e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) * x - 0.3550e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y * x * x - 0.51480e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x - 0.83850e5 * x * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.4680e4 * (double) (int) pow((double) y, (double) 3) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x + 0.48750e5 * pow(x, 0.3e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.33800e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x * x + 0.7200e4 * (double) (int) pow((double) y, (double) 3) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x - 0.52500e5 * pow(x, 0.3e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * (double) y + 0.25000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x * x - 0.780e3 * (double) y * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x - 0.138300e6 * (double) y * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x - 0.54000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x + 0.146000e6 * x * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * (double) y + 0.255700e6 * (double) y * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x + 0.56000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x - 0.201500e6 * x * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.8800e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x + 0.47500e5 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.35000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.6720e4 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x + 0.5500e4 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.3500e4 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.47500e5 * pow(x, 0.3e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.3800e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) * x + 0.250e3 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.3e1) * (double) y - 0.23600e5 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x - 0.36400e5 * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1)) * (-0.5e1 * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.5e1 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.14e2 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) - 0.2e1 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y - 0.15e2 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x - 0.10e2 * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.2e1 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.10e2 * x * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1)) * (0.14e2 - (double) (18 * y) + 0.5e1 * x) * (0.4e1 + (double) (2 * y) + 0.5e1 * x) * (0.19e2 + (double) (22 * y) + 0.5e1 * x) * (-0.7e1 - (double) (4 * y) + 0.5e1 * x);
// }

// Real func_2(const Real x, const Real y, const Real p)
// {
// 	using std::pow;
// 	return 0.5e1 / (0.35880e5 * (double) y * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.14300e5 * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.49000e5 * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.115500e6 * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.44800e5 * (double) y * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.66500e5 * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.101700e6 * (double) y * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.163000e6 * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.112670e6 * (double) y * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.21280e5 * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.50540e5 * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.3664e4 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.18520e5 * x * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.10165e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.7448e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) - 0.7028e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y - 0.8610e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x + 0.17680e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.64350e5 * x * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.4680e4 * (double) (int) pow((double) y, (double) 3) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.26000e5 * pow(x, 0.3e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.16250e5 * pow(x, 0.4e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.29000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.71500e5 * x * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.7200e4 * (double) (int) pow((double) y, (double) 3) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) - 0.7500e4 * pow(x, 0.3e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.12500e5 * pow(x, 0.4e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) + 0.8800e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.5000e4 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.12500e5 * pow(x, 0.4e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.16000e5 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.2500e4 * pow(x, 0.4e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.16720e5 * (double) (int) pow((double) y, (double) 3) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.11875e5 * pow(x, 0.4e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.3168e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 4) + 0.625e3 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.4e1) - 0.3168e4 * (double) (int) pow((double) y, (double) 4) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.21000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) - 0.114000e6 * x * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.78375e5 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.9980e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) + 0.3475e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x * x + 0.34624e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) + 0.21300e5 * x * x * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.77140e5 * (double) (y * y) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.26125e5 * pow(x, 0.3e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) + 0.12152e5 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) + 0.3750e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.3e1) + 0.9232e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) - 0.38200e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.16720e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) + 0.24700e5 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (y * y) - 0.10300e5 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) * x * x + 0.49020e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y - 0.7660e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y * x + 0.11080e5 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x + 0.52440e5 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) (y * y) + 0.111150e6 * x * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.4360e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (y * y) * x - 0.3550e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y * x * x - 0.51480e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x - 0.83850e5 * x * x * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.4680e4 * (double) (int) pow((double) y, (double) 3) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x + 0.48750e5 * pow(x, 0.3e1) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.33800e5 * (double) (y * y) * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x * x + 0.7200e4 * (double) (int) pow((double) y, (double) 3) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x - 0.52500e5 * pow(x, 0.3e1) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * (double) y + 0.25000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x * x - 0.780e3 * (double) y * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1) * x - 0.138300e6 * (double) y * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x - 0.54000e5 * (double) (y * y) * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * x + 0.146000e6 * x * x * pow(0.13e2 / 0.16e2 + x + x * x + 0.3e1 / 0.2e1 * (double) y + (double) (y * y), p / 0.2e1) * (double) y + 0.255700e6 * (double) y * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x + 0.56000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x - 0.201500e6 * x * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.8800e4 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x + 0.47500e5 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.35000e5 * (double) (y * y) * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.6720e4 * (double) (y * y) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x + 0.5500e4 * (double) y * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x * x + 0.3500e4 * pow(x, 0.3e1) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * (double) y - 0.47500e5 * pow(x, 0.3e1) * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) * (double) y + 0.3800e4 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) (int) pow((double) y, (double) 3) * x + 0.250e3 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * pow(x, 0.3e1) * (double) y - 0.23600e5 * (double) (int) pow((double) y, (double) 3) * pow(0.5e1 / 0.4e1 - 0.2e1 * x + x * x + (double) y + (double) (y * y), p / 0.2e1) * x - 0.36400e5 * pow(0.34e2 / 0.25e2 - 0.6e1 / 0.5e1 * x + x * x + (double) (2 * y) + (double) (y * y), p / 0.2e1)) * (-0.10e2 * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.2e1 * (double) y * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.10e2 * x * pow(0.5e1 / 0.4e1 + 0.2e1 * x + x * x - (double) y + (double) (y * y), p / 0.2e1) + 0.5e1 * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.20e2 * (double) y * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.5e1 * x * pow(0.41e2 / 0.25e2 - 0.8e1 / 0.5e1 * x + x * x - (double) (2 * y) + (double) (y * y), p / 0.2e1) - 0.14e2 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) + 0.18e2 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * (double) y - 0.5e1 * pow(0.1e1 - 0.2e1 * x + x * x + (double) (y * y), p / 0.2e1) * x) * (0.4e1 + (double) (2 * y) + 0.5e1 * x) * (0.19e2 + (double) (22 * y) + 0.5e1 * x) * (-0.7e1 - (double) (4 * y) + 0.5e1 * x) * (-0.1e1 + x);;
// }

// Real func_l(const std::vector<Real> &v, const Real x, const Real y, const Real p, const Real coord)
// {
// 	const Integer n_v = v.size()/2;
// 	std::vector<Real> r(n_v), A(n_v), B(n_v), C(n_v);

// 	for(Integer i=0; i<n_v; ++i)
// 	{
// 		const Real dx = v[2*i]-x;
// 		const Real dy = v[2*i+1]-y; 
// 		r[i]=sqrt(dx*dx+dy*dy);
// 	}



// 	for(Integer i=0; i<n_v; ++i)
// 	{
// 		const Integer ip=(i+1) % n_v;
// 		const Integer im = i == 0? (n_v-1) : (i-1);

// 		const Real vx = v[2*i];
// 		const Real vy = v[2*i+1];

// 		const Real vpx = v[2*ip];
// 		const Real vpy = v[2*ip+1];

// 		const Real vmx = v[2*im];
// 		const Real vmy = v[2*im+1];

// 		A[i]=((vx-x)*(vpy-y)-(vy-y)*(vpx-x))/2;
// 		B[i]=((vmx-x)*(vpy-y)-(vmy-y)*(vpx-x))/2;
// 	}

// 	Real res;
// 	Real wsum=0;
// 	for(Integer i = 0; i < n_v; ++i)
// 	{
// 		const Integer ip=(i+1) % n_v;
// 		const Integer im = i == 0? (n_v-1) : (i-1);
// 		Real Ai=1, Aiim=1, Aim=1; 

// 		for(Integer j = 0; j < n_v; ++j){
// 			if(j!=i)
// 				Ai*=A[j];

// 			if(j!=i && j!=im)
// 				Aiim *=A[j];

// 			if(j!=im)
// 				Aim *=A[j];
// 		}

// 		const Real w = pow(r[ip],p)*Ai - pow(r[i],p)*B[i]*Aiim + pow(r[im],p)*Aim;

// 		wsum+=w;

// 		if(i==coord)
// 			res=w;
// 	}

// 	res/=wsum;

// 	return res;

// }

void tetrahedralize_cmd(const int argc, const char **argv)
{
    assert(argc >= 4);

    Path path = argv[2];
 
    Mesh mesh;
    if(!read_mesh(path, mesh) ) {
        std::cerr << "unable to read mesh at " << path << std::endl;
        return;
    }

    Mesh out;
    TetrahedralizerOptions opts;

    if(argc >= 5) {
        opts.maximum_volume = atof(argv[4]);
    }
    
    // opts.verbose = true;
    opts.convex_hull = false;
    opts.mark_bounday = true;
    opts.allow_surface_refinement = true;
    opts.allowed_steiner_points = true;
    tetrahedralize(mesh, out, opts);
    write_mesh(argv[3], out);
}

// g++ -DReal=double
int main(const int argc, const char **argv) {
	// std::vector<Real> v(12);
	// const Real scaling = 100;
	// const Integer coord = 1;

	// v[0]=1; 		v[1]=0;
	// v[2]=4./5.; 	v[3]=1;
	// v[4]=-1; 		v[5]=1./2.;
	// v[6]=-1./2.; 	v[7]=-3./4.;
	// v[8]=3./5.; 	v[9]=-1;
	// v[10]=1; 		v[11]=-1./2.;

	// for(std::size_t i=0; i<v.size(); ++i)
	// 	v[i]*=scaling;

	// Mesh mesh;
	// TriangulatorOptions opts;
	// opts.maximum_area = 0.1;
	// triangulate(v, mesh, opts);
	// Integer index;

	// const std::vector<Real>  ps={-2, -1.5, -1, -0.5, 0, 0.5, 1, 2, 4};
	// std::vector<Real> isolinesN(50);
	// index = 0;
	// for(Integer i=-50; i<0; ++i)
	// 	isolinesN[index++]=i/10.0;

	// std::vector<Real> isolinesP(51);
	// index = 0;
	// for(Integer i=0; i<=50; ++i)
	// 	isolinesP[index++]=i/10.0;

	// std::vector<Real> func(mesh.n_nodes());
	// for(auto p : ps){
	// 	std::cout<<"Processing p="<<p<<"..."<<std::flush;
	// 	for(std::size_t i=0; i < func.size(); ++i)
	// 	{
	// 		const Real x = mesh.points[i*mesh.dim];
	// 		const Real y = mesh.points[i*mesh.dim+1];
	// 		func[i]=func_l(v, x, y, p, coord);
	// 	}

	// 	EPSCanvas canvas;
	// 	canvas.set_line_width(0.5);

	// 	canvas.set_color(0.5, 0.5, 0.5);
	// 	draw_countours(mesh, func, isolinesN, canvas);

	// 	canvas.set_color(0, 0, 0);
	// 	draw_countours(mesh, func, isolinesP, canvas);

	// 	canvas.set_line_width(1);
	// 	canvas.stroke_polygon(v);

	// 	canvas.write("p_"+std::to_string(p)+".eps");

	// 	std::cout<<"Done"<<std::endl;
	// }

    if(argc > 1) {
        static const std::string tetra = "tetra";

        if(argv[1] == tetra) {
            tetrahedralize_cmd(argc, argv);
        }
    }


	return EXIT_SUCCESS;
}

