#include "moonolith_quadrature_triangle_gauss.hpp"

#include <assert.h>

namespace moonolith {
	inline const Quadrature &quadrature_triangle_gauss(const int order)
	{
		static const int n_rules = 1;
		static const Quadrature rules[n_rules]
		{
			//order 0
			{
				{ 0.333333333333333333333333333333333, 0.333333333333333333333333333333333 },
				{ 1.0 },
				0.5
			}

			//order 1 ...
		};
		
		assert(order < n_rules);
		return rules[order];
	}
}
