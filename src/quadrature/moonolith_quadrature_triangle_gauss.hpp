#ifndef MOONOLITH_GAUSS_TRI_HPP
#define MOONOLITH_GAUSS_TRI_HPP

#include "moonolith_quadrature_rule.hpp"

namespace moonolith {
	inline const Quadrature &quadrature_triangle_gauss(const int order);
}

#endif //MOONOLITH_GAUSS_TRI_HPP
