#ifndef MOONOLITH_QUADRATURE_RULE_HPP
#define MOONOLITH_QUADRATURE_RULE_HPP 

#include <vector>

namespace moonolith {
	typedef struct {
		std::vector<Real> points;
		std::vector<Real> weights;
		Real ref_volume;
	} Quadrature;
}

#endif //MOONOLITH_QUADRATURE_RULE_HPP
