import ctypes
from ctypes import cdll, c_int32, c_double, c_bool, c_void_p, c_char_p, cast, Structure, POINTER,byref
import optparse, os

# todo generate with cmake when compiling moonolith
integer = c_int32
real = c_double

moonolith_lib = cdll.LoadLibrary('../../bin/src/python/libc_extern_bindings.dylib')

#triangulate
class TriangulatorOptions(Structure):
	_fields_ = [
	("allow_boundary_refinement", c_bool),
	("allow_edge_refinement", c_bool),
	("constrain_angle", c_bool),
	("convex_hull", c_bool),
	("add_third_dimension", c_bool),
	("maximum_area", real),
	("allowed_steiner_points", integer),
	("verbose", c_bool)]

	def __init__(self):
		self.add_third_dimension = c_bool(0);

		self.allow_boundary_refinement = c_bool(1);
		self.allow_edge_refinement = c_bool(1);

		self.constrain_angle = c_bool(1);

		self.maximum_area = -1;
		self.allowed_steiner_points = -1;

		self.convex_hull = c_bool(1);

		self.verbose = c_bool(0);

	def set_opts(self, options):
		self.add_third_dimension = options.get("add_third_dimension", self.add_third_dimension)
		self.allow_boundary_refinement = options.get("allow_boundary_refinement", self.allow_boundary_refinement)
		self.allow_edge_refinement = options.get("allow_edge_refinement", self.allow_edge_refinement)
		self.constrain_angle = options.get("constrain_angle", self.constrain_angle)
		self.maximum_area = options.get("maximum_area", self.maximum_area)
		self.allowed_steiner_points = options.get("allowed_steiner_points", self.allowed_steiner_points)
		self.convex_hull = options.get("convex_hull", self.convex_hull)
		self.verbose = options.get("verbose", self.verbose)

moonolith_lib.triangulate_polygon.argtypes = (integer, POINTER(real), c_void_p, POINTER(TriangulatorOptions))
moonolith_lib.triangulate_polygon.restype  = c_bool

######################################################
######################################################

# mesh
# class initializer
def init_mesh_implementation():
	global moonolith_lib

	print "Initializing python-c byindings for Mesh"

	# new/delete
	moonolith_lib.mesh_new.restype = c_void_p
	moonolith_lib.mesh_delete.argtypes = [c_void_p]

	# n_nodes
	moonolith_lib.mesh_n_nodes.argtypes = [c_void_p]
	moonolith_lib.mesh_n_nodes.restype = integer

	# points
	moonolith_lib.mesh_points.argtypes = [c_void_p]
	moonolith_lib.mesh_points.restype = POINTER(real)

	# read mesh
	moonolith_lib.mesh_read.argtypes = [c_void_p, c_char_p]
	moonolith_lib.mesh_read.restype = c_bool

	# write mesh
	moonolith_lib.mesh_write.argtypes = [c_void_p, c_char_p]
	moonolith_lib.mesh_write.restype = c_bool

	# print
	moonolith_lib.mesh_describe.argtypes = [c_void_p]

	# dim
	moonolith_lib.mesh_dim.argtypes = [c_void_p]
	moonolith_lib.mesh_dim.restype = integer

	return moonolith_lib

#mesh class
class Mesh(object):
	_fields_ = [("p_impl_", c_void_p)]
	cpp_lib_ = init_mesh_implementation()
	
	def __init__(self):
		self.p_impl_ = self.cpp_lib_.mesh_new()
		
	def __del__(self):
		self.cpp_lib_.mesh_delete(self.p_impl_);

	def n_nodes(self):
		return self.cpp_lib_.mesh_n_nodes(self.p_impl_)

	def points(self):
		array_type_p = POINTER(real * (self.n_nodes() * 3))
		return cast(self.cpp_lib_.mesh_points(self.p_impl_), array_type_p).contents

	def read(self, path):
		return self.cpp_lib_.mesh_read(self.p_impl_, c_char_p(path))

	def write(self, path):
		return self.cpp_lib_.mesh_write(self.p_impl_, c_char_p(path))

	def describe(self):
		self.cpp_lib_.mesh_describe(self.p_impl_)

	def dim(self):
		return self.cpp_lib_.mesh_dim(self.p_impl_)

######################################################
######################################################		
 
def triangulate_polygon(points, **options):
	global moonolith_lib;
	m = Mesh()

	points_array = (real * len(points))()
	points_array[:] = points;
	
	opts = TriangulatorOptions()
	opts.set_opts(options)

	moonolith_lib.triangulate_polygon(len(points)/2, points_array, m.p_impl_, opts)
	return m


######################################################
######################################################

def init_hash_grid_implementation():
	global moonolith_lib
	print "Initializing python-c byindings for HashGrid"

	# new/delete
	moonolith_lib.hash_grid_new.restype = c_void_p
	moonolith_lib.hash_grid_delete.argtypes = [c_void_p]

	# reset
	moonolith_lib.hash_grid_reset.argtypes = [c_void_p, integer, POINTER(real), POINTER(integer)]

	#describe
	moonolith_lib.hash_grid_describe.argtypes = [c_void_p]

	#hash
	moonolith_lib.hash_grid_hash.argtypes = [c_void_p, POINTER(real)]	
	return moonolith_lib


class HashGrid(object):
	_fields_ = [("p_impl_", c_void_p)]
	cpp_lib_ = init_hash_grid_implementation()

	def __init__(self):
		self.p_impl_ = self.cpp_lib_.hash_grid_new()

	def __del__(self):
		self.cpp_lib_.hash_grid_delete(self.p_impl_)

	def reset(self, aabb_min, aabb_max, dims):

		dim = len(aabb_min);
		
		box_array = (real * (dim * 2))()
		box_array[:dim] = aabb_min;
		box_array[dim:] = aabb_max

		dims_array = (integer * dim)()
		dims_array[:] = dims

		print dims
	
		self.cpp_lib_.hash_grid_reset(self.p_impl_, integer(dim),  box_array, dims_array)
	
	def describe(self):
		self.cpp_lib_.hash_grid_describe(self.p_impl_)

	def hash(self, p):
		p_array = (real * len(p))()
		p_array[:] = p
		return self.cpp_lib_.hash_grid_hash(self.p_impl_, p_array)



######################################################
######################################################


