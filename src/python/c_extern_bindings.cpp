
#include <iostream>
#include "moonolith_mesh.hpp"
#include "moonolith_mesh_io.hpp"
#include "moonolith_path.hpp"
#include "moonolith_triangulator.hpp"
#include "moonolith_hash_grid.hpp"
#include "moonolith_box.hpp"

extern "C" {

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	void * mesh_new()
	{
		return new moonolith::Mesh();
	}

	void mesh_delete(void *ptr)
	{
		if(!ptr) return;

		moonolith::Mesh * mesh = reinterpret_cast<moonolith::Mesh *>(ptr);
		delete mesh;
	}

	Integer mesh_n_nodes(const void *ptr)
	{
		if(!ptr) return 0;

		const moonolith::Mesh * mesh = reinterpret_cast<const moonolith::Mesh *>(ptr);
		return mesh->n_nodes();
	}

	Real *mesh_points(void *ptr)
	{
		if(!ptr) return nullptr;

		moonolith::Mesh * mesh = reinterpret_cast<moonolith::Mesh *>(ptr);
		if(mesh->points.empty()) return nullptr;

		return &mesh->points[0];
	}

	bool mesh_read(void *ptr, const char *filename)
	{
		if(!ptr) return false;
		moonolith::Mesh * mesh = reinterpret_cast<moonolith::Mesh *>(ptr);	

		moonolith::Path path = filename;
		return moonolith::read_mesh(path, *mesh);
	}

	bool mesh_write(const void *ptr, const char *filename)
	{
		if(!ptr) return false;
		const moonolith::Mesh * mesh = reinterpret_cast<const moonolith::Mesh *>(ptr);	

		moonolith::Path path = filename;
		return moonolith::write_mesh(path, *mesh);
	}

	void mesh_describe(const void *ptr)
	{
		if(!ptr) return;
		const moonolith::Mesh * mesh = reinterpret_cast<const moonolith::Mesh *>(ptr);	
		mesh->describe(std::cout);
	}

	Integer mesh_dim(const void *ptr)
	{
		if(!ptr) return 0;
		const moonolith::Mesh * mesh = reinterpret_cast<const moonolith::Mesh *>(ptr);	
		return mesh->dim;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool triangulate_polygon(const Integer n_points, const Real *points, void *void_mesh, const moonolith::TriangulatorOptions *opts)
	{
		if(!void_mesh) return false;
		moonolith::Mesh * mesh = reinterpret_cast<moonolith::Mesh *>(void_mesh);	

		std::vector<Real> polygon;
		polygon.insert(polygon.end(), points, points + n_points * 2);
		triangulate_points(polygon, *mesh, *opts);
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	void * hash_grid_new()
	{
		return new moonolith::HashGrid();
	}

	void hash_grid_reset(void *ptr, const Integer dim, const Real *aabb, const Integer *dims)
	{
		if(!ptr) return;

		moonolith::Box box(dim);
		std::copy(aabb, aabb+dim, box.get_min().begin());
		std::copy(aabb + dim, aabb + 2 * dim, box.get_max().begin());

		std::vector<Integer> v_dims(dim);
		std::copy(dims, dims+dim, v_dims.begin());

		moonolith::HashGrid * hash_grid = reinterpret_cast<moonolith::HashGrid *>(ptr);
		hash_grid->reset(box, v_dims);
	}

	void hash_grid_delete(void *ptr)
	{
		if(!ptr) return;

		moonolith::HashGrid * hash_grid = reinterpret_cast<moonolith::HashGrid *>(ptr);
		delete hash_grid;
	}

	void hash_grid_describe(const void *ptr)
	{
		if(!ptr) return;

		const moonolith::HashGrid * hash_grid = reinterpret_cast<const moonolith::HashGrid *>(ptr);
		hash_grid->describe(std::cout);
	}

	Integer hash_grid_hash(const void *ptr, const Real *p)
	{
		if(!ptr) return moonolith::invalid_index<Integer>::value();

		const moonolith::HashGrid * hash_grid = reinterpret_cast<const moonolith::HashGrid *>(ptr);

		std::vector<Real> v_p(hash_grid->n_dims());

		std::copy(p, p+v_p.size(), v_p.begin());

		return hash_grid->hash(v_p);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
}

