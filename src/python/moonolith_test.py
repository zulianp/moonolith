import moonolith
from moonolith import *

# read from disk	 
m = Mesh()
if m.read('../../data/tetrahedron.ele'):
	for p in m.points():
		print p


# triangulate a polygon
m2 = triangulate_polygon([0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0], 
						 maximum_area = 0.3, 
						 allow_edge_refinement = 1)

# write to disk
m2.write('prova.obj')


#hash grid
hg = HashGrid()
hg.reset([0.0, 0.0], [1.0, 1.0], [10, 10])
hg.describe()

print hg.hash([0.5, 0.5])