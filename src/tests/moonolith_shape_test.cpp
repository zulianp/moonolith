#include <sstream>

#include "moonolith_matrix_io.hpp"

#include "moonolith_test.hpp"
#include "moonolith_quad.hpp"
#include "moonolith_meanvalue.hpp"
#include "moonolith_maximum_entropy.hpp"
#include "moonolith_metric.hpp"
#include "moonolith_triangulator.hpp"
#include "moonolith_func_to_color.hpp"

#include "moonolith_eps_canvas.hpp"
#include "moonolith_eps_marching_triangles.hpp"
#include "moonolith_poly_io.hpp"

#include "moonolith_b_spline.hpp"
#include "moonolith_tp_b_spline.hpp"
#include "moonolith_bezier_curve.hpp"
#include "moonolith_bezier_surface.hpp"

#include "moonolith_sederberg_interpolation.hpp"

#include "moonolith_algebra.hpp"
#include "moonolith_path.hpp"

using namespace moonolith;

M_TEST_CLASS(ShapeTest) {
	M_TEST_SUITE_BEGIN( ShapeTest );		
	M_TEST_CASE_ADD( quad );
	M_TEST_CASE_ADD ( b_spline_1 );
	M_TEST_CASE_ADD ( b_spline_2 );
	M_TEST_CASE_ADD ( tp_b_spline );
	M_TEST_CASE_ADD ( bezier_curve );
	M_TEST_CASE_ADD ( bezier_surface );
	M_TEST_CASE_ADD( meanvalue_coords );
	M_TEST_CASE_ADD( sederberg );
	M_TEST_CASE_ADD( maximum_entropy_coords );
	M_TEST_CASE_ADD( metric_coords );
	M_TEST_SUITE_END( ShapeTest );

	M_TEST_CASE(ShapeTest, quad)
	{
		//! [quad example]
		std::vector<Real> element{ 0.2, 0.1,
			0.6, 0.0,
			0.7, 0.5,
			0.1, 0.5 };

			Quad q(&element[0]);
			std::vector<Real> x{ 0.5, 0.5 };
			std::vector<Real> grads(q.reference().n_functions() * 2);

		//reference gradients
			q.reference().grad(&x[0], &grads[0]);

		//actual gradients
			q.grad(&x[0], &grads[0]);

		// for(auto v : grads) {
		// 	std::cout << v << std::endl;
		// }

	//! [quad example]
		}

		M_TEST_CASE(ShapeTest, b_spline_1)
		{
		//! [b_spline example]
			std::vector<Real> knots(11);
			lin_space(0,1,11, knots);

			std::vector<Real> control(2*7);

			control[0]=0;
			control[1]=0;

			control[2]=1;
			control[3]=-1;

			control[4]=2;
			control[5]=0;

			control[6]=2;
			control[7]=1;

			control[8]=4;
			control[9]=3;

			control[10]=1;
			control[11]=2;

			control[12]=0;
			control[13]=3;

			BSpline spline;
			spline.init(3, knots, control);
			std::vector<Real> t;
			std::vector<Real> res;


			lin_space(0.31, 0.69, 10, t);
			spline.interpolate(t, res);


			std::vector<Real> expected;
			Integer rows, cols;

			Path path = "../data/spline_values_1.txt";
			M_TEST_ASSERT( read_matrix(path, expected, rows, cols) );


			M_TEST_ASSERT ( cols == 10 );
			M_TEST_ASSERT ( rows == 2 );
			M_TEST_ASSERT ( expected.size() == res.size() );

			for(std::size_t i = 0; i < 10; ++i) {
				for(Integer j=0; j < 2; ++j){
					const Real expected_entry = expected[j*10 + i];
					const Real value = res[i * 2 + j];

					M_TEST_ASSERT_DOUBLES_EQ(expected_entry, value, 1e-14);
				}
			}


			BSpline deriv;
			spline.derivative(deriv);

			lin_space(0.41, 0.59, 10, t);
			deriv.interpolate(t, res);

			path = "../data/spline_derivative.txt";
			M_TEST_ASSERT( read_matrix(path, expected, rows, cols) );

			M_TEST_ASSERT ( cols == 10 );
			M_TEST_ASSERT ( rows == 2 );
			M_TEST_ASSERT ( expected.size() == res.size() );

			for(std::size_t i = 0; i < 10; ++i) {
				for(Integer j=0; j < 2; ++j){
					const Real expected_entry = expected[j*10 + i];
					const Real value = res[i * 2 + j];

					M_TEST_ASSERT_DOUBLES_EQ(expected_entry, value, 1e-13);
				}
			}




			lin_space(0.31, 0.69, 100, t);
			spline.interpolate(t, res);

			EPSCanvas canvas;
			canvas.set_line_width(0.1);
			canvas.set_color(1, 0.5, 0);
			canvas.stroke_poly_line(res);
			canvas.set_color(0, 0, 0);
			canvas.stroke_poly_line(control);

			canvas.write("test_spline_1.eps");

		//! [b_spline example]
		}

		M_TEST_CASE(ShapeTest, b_spline_2)
		{
		//! [b_spline example]
			std::vector<Real> knots(11);
			knots[0]=knots[1]=knots[2]=knots[3]=0;

			knots[4]=0.25;
			knots[5]=0.5;
			knots[6]=0.75;

			knots[7]=knots[8]=knots[9]=knots[10]=1;

			std::vector<Real> control(2*7);

			control[0]=0;
			control[1]=0;

			control[2]=1;
			control[3]=0;

			control[4]=1;
			control[5]=1;

			control[6]=2;
			control[7]=4;

			control[8]=3;
			control[9]=4;

			control[10]=3;
			control[11]=5;

			control[12]=2;
			control[13]=5;


			BSpline spline;
			spline.init(3, knots, control);

			std::vector<Real> t, res;
			lin_space(0.0, 1.0 , 20, t);
			spline.interpolate(t, res);

			std::vector<Real> expected;
			Integer rows, cols;

			Path path = "../data/spline_values_2.txt";
			M_TEST_ASSERT( read_matrix(path, expected, rows, cols) );


			M_TEST_ASSERT ( cols == 20 );
			M_TEST_ASSERT ( rows == 2 );
			M_TEST_ASSERT ( expected.size() == res.size() );

			for(std::size_t i = 0; i < 20; ++i) {
				for(Integer j=0; j < 2; ++j){
					const Real expected_entry = expected[j*20 + i];
					const Real value = res[i * 2 + j];

					M_TEST_ASSERT_DOUBLES_EQ(expected_entry, value, 1e-14);
				}
			}


		//! [b_spline example]
		}


		M_TEST_CASE(ShapeTest, tp_b_spline)
		{
		//! [tp_b_spline example]
			TensorProductBSpline spline;
			const Integer n_control_u = 4;
			const Integer n_control_v = 5;

			TensorIndex ti(n_control_u, n_control_v, 2);

			std::vector<Real> control(ti.size());

			control[ti.index_for(0, 0, 0)]=0.; control[ti.index_for(0, 0, 1)]=0.;
			control[ti.index_for(1, 0, 0)]=1.; control[ti.index_for(1, 0, 1)]=-1.;
			control[ti.index_for(2, 0, 0)]=4.; control[ti.index_for(2, 0, 1)]=-1.5;
			control[ti.index_for(3, 0, 0)]=6.5; control[ti.index_for(3, 0, 1)]=1.;

			control[ti.index_for(0, 1, 0)]=-1.; control[ti.index_for(0, 1, 1)]=0.5;
			control[ti.index_for(1, 1, 0)]=1.; control[ti.index_for(1, 1, 1)]=0.5;
			control[ti.index_for(2, 1, 0)]=3.; control[ti.index_for(2, 1, 1)]=1.;
			control[ti.index_for(3, 1, 0)]=5.; control[ti.index_for(3, 1, 1)]=1.6;

			control[ti.index_for(0, 2, 0)]=-2.; control[ti.index_for(0, 2, 1)]=0.6;
			control[ti.index_for(1, 2, 0)]=0.; control[ti.index_for(1, 2, 1)]=1.;
			control[ti.index_for(2, 2, 0)]=2.; control[ti.index_for(2, 2, 1)]=2.;
			control[ti.index_for(3, 2, 0)]=4.; control[ti.index_for(3, 2, 1)]=2.;

			control[ti.index_for(0, 3, 0)]=-4.; control[ti.index_for(0, 3, 1)]=2.2;
			control[ti.index_for(1, 3, 0)]=-2.; control[ti.index_for(1, 3, 1)]=2.;
			control[ti.index_for(2, 3, 0)]=0.; control[ti.index_for(2, 3, 1)]=3.;
			control[ti.index_for(3, 3, 0)]=1.; control[ti.index_for(3, 3, 1)]=4.;

			control[ti.index_for(0, 4, 0)]=-2.; control[ti.index_for(0, 4, 1)]=3.;
			control[ti.index_for(1, 4, 0)]=-1.; control[ti.index_for(1, 4, 1)]=4.;
			control[ti.index_for(2, 4, 0)]=0.5; control[ti.index_for(2, 4, 1)]=5.;
			control[ti.index_for(3, 4, 0)]=1.; control[ti.index_for(3, 4, 1)]=5.;

			std::vector<Real> knots_u; lin_space(0, 1, n_control_u+4, knots_u);
			std::vector<Real> knots_v; lin_space(0, 1, n_control_v+3, knots_v);
			
			spline.init(knots_u, knots_v, control, n_control_u, n_control_v);

			std::vector<Real> t_transposed, t;
			Integer rows, cols, t_cols;
			Path path = "../data/tp_evaluation.txt";
			M_TEST_ASSERT( read_matrix(path, t_transposed, rows, t_cols) );
			assert(rows == 2);
			
			t.resize(t_transposed.size());
			for (Integer i = 0; i < t_cols; ++i)
			{
				t[2*i] = t_transposed[i];
				t[2*i + 1] = t_transposed[t_cols+i];
			}

			std::vector<Real> res;
			spline.interpolate(t, res);




			std::vector<Real> expected;
			path = "../data/tp_values.txt";
			M_TEST_ASSERT( read_matrix(path, expected, rows, cols) );

			M_TEST_ASSERT ( cols == t_cols );
			M_TEST_ASSERT ( rows == 2 );
			M_TEST_ASSERT ( expected.size() == res.size() );

			for(Integer i = 0; i < t_cols; ++i) {
				for(Integer j=0; j < 2; ++j){
					const Real expected_entry = expected[j*t_cols + i];
					const Real value = res[i * 2 + j];

					M_TEST_ASSERT_DOUBLES_EQ(expected_entry, value, 1e-15);
				}
			}

			

			std::vector<Real> res_dx, res_dy;
			TensorProductBSpline dx, dy;
			spline.derivative(dx,dy);


			dx.interpolate(t, res_dx);
			dy.interpolate(t, res_dy);

			path = "../data/tp_derivative.txt";
			M_TEST_ASSERT( read_matrix(path, expected, rows, cols) );
			M_TEST_ASSERT( cols == t_cols * 2);

			for(Integer i = 0; i < t_cols; ++i) {
				for(Integer j=0; j < 2; ++j){
					const Real expected_entry_dx = expected[j*cols + 2*i];
					const Real expected_entry_dy = expected[j*cols + 2*i + 1];

					const Real dx_value = res_dx[i * 2 + j];
					const Real dy_value = res_dy[i * 2 + j];

					M_TEST_ASSERT_DOUBLES_EQ(expected_entry_dx, dx_value, 1e-14);
					M_TEST_ASSERT_DOUBLES_EQ(expected_entry_dy, dy_value, 1e-14);
				}
			}
		//! [tp_b_spline example]
		}



		M_TEST_CASE(ShapeTest, bezier_curve)
		{
		//! [bezier curve example]

			std::vector<Real> control(2*5);

			control[0]=0;
			control[1]=0;

			control[2]=10;
			control[3]=-10;

			control[4]=20;
			control[5]=0;

			control[6]=-10;
			control[7]=10;

			control[8]=40;
			control[9]=30;


			std::vector<Real> t;
			lin_space(0, 1, 10, t);

			BezierCurve curve;
			curve.init(control);

			std::vector<Real> res;
			curve.interpolate(t, res);

			BezierCurve deriv;
			curve.derivative(deriv);
			deriv.interpolate(t, res);

			

			lin_space(0, 1, 100, t);
			curve.interpolate(t, res);

			EPSCanvas canvas;
			canvas.set_line_width(0.1);
			canvas.set_color(1, 0.5, 0);
			canvas.stroke_poly_line(res);
			canvas.set_color(0, 0, 0);
			canvas.stroke_poly_line(control);

			canvas.write("test_bezier_curve.eps");
		//! [bezier curve example]
		}


		M_TEST_CASE(ShapeTest, bezier_surface)
		{
		//! [tp_b_spline example]
			const Integer n_control_u = 4;
			const Integer n_control_v = 3;

			TensorIndex ti(n_control_u, n_control_v, 2);

			std::vector<Real> control(ti.size());

			control[ti.index_for(0, 0, 0)]=-1.; control[ti.index_for(0, 0, 1)]=-2.;
			control[ti.index_for(1, 0, 0)]=1.; control[ti.index_for(1, 0, 1)]=-1.;
			control[ti.index_for(2, 0, 0)]=4.; control[ti.index_for(2, 0, 1)]=-1.5;
			control[ti.index_for(3, 0, 0)]=6.5; control[ti.index_for(3, 0, 1)]=1.;

			control[ti.index_for(0, 1, 0)]=-1.; control[ti.index_for(0, 1, 1)]=0.5;
			control[ti.index_for(1, 1, 0)]=1.; control[ti.index_for(1, 1, 1)]=0.5;
			control[ti.index_for(2, 1, 0)]=3.; control[ti.index_for(2, 1, 1)]=1.;
			control[ti.index_for(3, 1, 0)]=5.; control[ti.index_for(3, 1, 1)]=1.6;

			control[ti.index_for(0, 2, 0)]=-2.; control[ti.index_for(0, 2, 1)]=0.6;
			control[ti.index_for(1, 2, 0)]=0.; control[ti.index_for(1, 2, 1)]=1.;
			control[ti.index_for(2, 2, 0)]=2.; control[ti.index_for(2, 2, 1)]=2.;
			control[ti.index_for(3, 2, 0)]=4.; control[ti.index_for(3, 2, 1)]=2.;

			BezierSurface surf;			
			surf.init(control, n_control_u, n_control_v, 2);

			std::vector<Real> t;
			mesh_grid(
				0, 1, 10,
				0, 1, 10,
				t);

			std::vector<Real> res;
			surf.interpolate(t, res);
			

			std::vector<Real> res_dx, res_dy;
			BezierSurface dx, dy;
			surf.derivative(dx,dy);


			dx.interpolate(t, res_dx);
			dy.interpolate(t, res_dy);











			Mesh mesh;

			for(Integer j=0; j < ti[1]; ++j)
			{
				for(Integer i=0; i < ti[0]; ++i)
				{
					mesh.add_node(control[ti.index_for(i,j,0)]*10, control[ti.index_for(i,j,1)]*10);
				}
			}

			mesh.add_element({0, 1, 5, 4}, ElemType::QUAD4);
			mesh.add_element({1, 2, 6, 5}, ElemType::QUAD4);
			mesh.add_element({2, 3, 7, 6}, ElemType::QUAD4);

			mesh.add_element({4, 5, 9, 8}, ElemType::QUAD4);
			mesh.add_element({5, 6, 10, 9}, ElemType::QUAD4);
			mesh.add_element({6, 7, 11, 10}, ElemType::QUAD4);




			mesh_grid(
				0, 1, 100,
				0, 1, 100,
				t);
			surf.interpolate(t, res);


			EPSCanvas canvas;
			canvas.set_line_width(0.1);
			canvas.set_color(1, 0.5, 0);
			for(std::size_t i=0; i < res.size()/2; ++i)
			{
				canvas.fill_circle(res[2*i]*10, res[2*i+1]*10, 0.1);
			}
			
			canvas.set_color(0, 0, 0);
			canvas.draw_mesh(mesh);
			canvas.stroke();

			canvas.write("test_bezier_surface.eps");

		//! [tp_b_spline example]
		}


		M_TEST_CASE(ShapeTest, sederberg)
		{
		//! [sederberg example]
			Path src_path = "../data/Star0.poly";
			std::vector<Real> src_poly;
			M_TEST_ASSERT( read_poly(src_path, src_poly) );

			Path trg_path = "../data/Star1.poly";
			std::vector<Real> trg_poly;
			M_TEST_ASSERT( read_poly(trg_path, trg_poly) );

			std::vector<Real> poly;
			serderberg_interpolate(src_poly, trg_poly, 0.2, poly);


			EPSCanvas canvas;
			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);
			
			canvas.write("test_sederberg.eps");

		//! [sederberg example]
		}

		void check_bc_props(const std::vector<Real> &poly, const std::vector<Real> &pts, const std::vector<Real> &b)
		{
			const Integer n_boundary = poly.size()/2;
			const Integer n_points = pts.size()/2;


			std::vector<Real> part_unit(n_points);
			std::vector<Real> lin_repr(n_points*2);

			std::fill(part_unit.begin(), part_unit.end(),0);
			std::fill(lin_repr.begin(), lin_repr.end(),0);

			for(Integer j = 0; j < n_points; ++j) 
			{
				for(Integer i = 0; i < n_boundary; ++i)
				{
					part_unit[j] += b[j*n_boundary+i];

					lin_repr[j*2]   += poly[i*2]   * b[j*n_boundary+i];
					lin_repr[j*2+1] += poly[i*2+1] * b[j*n_boundary+i];
				}
			}

			for(std::size_t i=0; i < part_unit.size(); ++i)
				M_TEST_ASSERT_DOUBLES_EQ(1, part_unit[i], 1e-12);

			for(std::size_t i=0; i < lin_repr.size(); ++i){
				Real delta = pts[i] - lin_repr[i];
				M_TEST_ASSERT_DOUBLES_EQ(pts[i], lin_repr[i], 1e-12);
				delta =0;
			}
		}

		M_TEST_CASE(ShapeTest, meanvalue_coords)
		{
		//! [meanvalue example]

			const Integer coord = 2;

			Path path = "../data/Star0.poly";

			std::vector<Real> poly;
			M_TEST_ASSERT( read_poly(path, poly) );


			Mesh mesh, mesh_coarse;

			TriangulatorOptions opts;
			opts.maximum_area = 1;
			triangulate(poly, mesh, opts);


			opts.maximum_area = 10;
			triangulate(poly, mesh_coarse, opts);


			std::vector<Real> b, b1, grad_b, b_coarse;
			std::vector<Real> func(mesh.n_nodes());
			std::vector<Real> func_coarse(mesh_coarse.n_elements());

			meanvalue(poly, mesh.points, b);
			check_bc_props(poly, mesh.points, b);

			meanvalue(poly, mesh_coarse.points, b_coarse);
			check_bc_props(poly, mesh_coarse.points, b_coarse);

			meanvalue_derivative(poly, mesh.points, b1, grad_b);
			check_bc_props(poly, mesh.points, b1);

			for(std::size_t i=0; i < func.size(); ++i)
			{
				func[i]=b[i * poly.size() / 2 + coord];
			}

			for(Integer i = 0; i < mesh_coarse.n_elements(); ++i) {
				const Integer * el = mesh_coarse.element(i);
				const Integer n_nodes = mesh_coarse.n_nodes(i);

				Real val = 0;
				for(Integer k = 0; k < n_nodes; ++k) {
					val += b_coarse[el[k] * poly.size() / 2 + coord];
				}

				func_coarse[i] = val / n_nodes;
			}

			std::vector<Real> isolines(11);
			for(Integer i=0; i<=10; ++i)
				isolines[i]=i/10.0;


			EPSCanvas canvas;
			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);
			canvas.set_color(0, 0, 0);
			draw_countours(mesh, func, isolines, canvas);

			canvas.write("test_meanvalue.eps");
			canvas.clear();

			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);

			std::vector<Real> red;
			func_to_rgb(func_coarse, 0, 1,
				1, 0, 0,
				1, 1, 1,
				red);

			canvas.fill_mesh(mesh_coarse, red);
			canvas.set_color(0, 0, 0);
			draw_countours(mesh, func, isolines, canvas);

			canvas.write("test_meanvalue_red.eps");
			canvas.clear();

			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);

			std::vector<Real> hsv;
			func_to_hsv(func_coarse, 0, 1, hsv);
			canvas.fill_mesh(mesh_coarse, hsv);
			canvas.set_color(0, 0, 0);
			draw_countours(mesh, func, isolines, canvas);

			canvas.write("test_meanvalue_hsv.eps");
			canvas.clear();

			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);

			std::vector<Real> fire;
			func_to_fire(func_coarse, 0, 1, fire);
			canvas.fill_mesh(mesh_coarse, fire);
			canvas.set_color(0, 0, 0);
			draw_countours(mesh, func, isolines, canvas);

			canvas.write("test_meanvalue_fire.eps");

			M_TEST_ASSERT(b == b1);


		//! [meanvalue example]
		}


		M_TEST_CASE(ShapeTest, maximum_entropy_coords)
		{
		//! [maximum_entropy example]

			const Integer coord = 2;

			Path path = "../data/Star0.poly";

			std::vector<Real> poly;
			M_TEST_ASSERT( read_poly(path, poly) );


			Mesh mesh;

			TriangulatorOptions opts;
			opts.maximum_area = 1;
			triangulate(poly, mesh, opts);


			std::vector<Real> b, b1, grad_b;
			std::vector<Real> func(mesh.n_nodes());

			maximum_entropy(poly, mesh.points, b);
			// check_bc_props(poly, mesh.points, b);


			for(std::size_t i=0; i < func.size(); ++i)
			{
				func[i]=b[i*poly.size()/2+coord];
			}

			std::vector<Real> isolines(11);
			for(Integer i=0; i<=10; ++i)
				isolines[i]=i/10.0;


			EPSCanvas canvas;
			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);
			draw_countours(mesh,func, isolines, canvas);

			canvas.write("test_maximum_entropy.eps");


		//! [maximum_entropy example]
		}


		M_TEST_CASE(ShapeTest, metric_coords)
		{
		//! [metric example]

			const Integer coord = 2;

			Path path = "../data/Star0.poly";

			std::vector<Real> poly;
			M_TEST_ASSERT( read_poly(path, poly) );


			Mesh mesh;

			TriangulatorOptions opts;
			opts.maximum_area = 1;
			triangulate(poly, mesh, opts);


			std::vector<Real> b, b1, grad_b;
			std::vector<Real> func(mesh.n_nodes());

			metric(poly, mesh.points, b);
			check_bc_props(poly, mesh.points, b);


			for(std::size_t i=0; i < func.size(); ++i)
			{
				func[i]=b[i*poly.size()/2+coord];
			}

			std::vector<Real> isolines(11);
			for(Integer i=0; i<=10; ++i)
				isolines[i]=i/10.0;


			EPSCanvas canvas;
			canvas.set_line_width(0.5);
			canvas.stroke_polygon(poly);
			draw_countours(mesh,func, isolines, canvas);

			canvas.write("test_metric.eps");


		//! [metric example]
		}

	};

	M_TEST_SUITE_REGISTRATION(ShapeTest);
