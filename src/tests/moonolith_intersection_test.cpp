

#include <sstream>

#include "moonolith_test.hpp"
#include "moonolith_intersection_2.hpp"

using namespace moonolith;

M_TEST_CLASS(IntersectionTest) {
	M_TEST_SUITE_BEGIN( IntersectionTest );		
	M_TEST_CASE_ADD( polygon_intersection );
	M_TEST_SUITE_END( IntersectionTest );

	M_TEST_CASE(IntersectionTest, polygon_intersection)
	{
		//! [polygon_intersection example]
		std::vector<Real> polygon_1{
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0
		};

		std::vector<Real> polygon_2{
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0
		};

		std::vector<Real> expected{
			0.0, 0.0,
			1.0, 0.0,
			0.5, 0.5
		};

		std::vector<Real> result;

		const Real tol = 1e-8;
		M_TEST_ASSERT( intersect_convex_polygons(polygon_1, polygon_2, result, tol) );

		for(std::size_t i = 0; i < result.size(); ++i) {
			M_TEST_ASSERT_DOUBLES_EQ(expected[i], result[i], tol);
		}
		//! [polygon_intersection example]
	}

};

M_TEST_SUITE_REGISTRATION(IntersectionTest);
