#include <sstream>

#include "moonolith_test.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_triangulator.hpp"

#include "moonolith_poly_io.hpp"
#include "moonolith_obj_io.hpp"

#include "moonolith_eps_canvas.hpp"
#include "moonolith_svg_canvas.hpp"
#include "moonolith_eps_marching_triangles.hpp"
#include "moonolith_plotter.hpp"

#include "moonolith_path.hpp"

using namespace moonolith;

M_TEST_CLASS(EPSTest) {
	M_TEST_SUITE_BEGIN( EPSTest );
	M_TEST_CASE_ADD( simple_draw );
	M_TEST_CASE_ADD( simple_svg_draw );
	M_TEST_CASE_ADD( draw );
	M_TEST_CASE_ADD( draw_function );
	M_TEST_CASE_ADD( draw_svg );
	M_TEST_CASE_ADD( draw_svg_function );

	M_TEST_CASE_ADD( plotter );

	M_TEST_SUITE_END( EPSTest );

	M_TEST_CASE(EPSTest, simple_draw)
	{
		//! [draw example]
		EPSCanvas canvas;
		canvas.set_line_width(0.5);

		const Real x[]={50.,0., 100.};
		const Real y[]={0., 86.6, 86.6};
		const Real c[]={1.,0,0, 0.,1.,0., 0.,0.,1.};

		canvas.fill_triangle(x, y, c);



		canvas.set_dashing(0.3);
		canvas.begin_clip_rect(90,90,20,20);
		canvas.stroke_rect(100, 100, 30, 45);
		canvas.end_clip_rect();

		canvas.write("test_simple_draw.eps");
	}


	M_TEST_CASE(EPSTest, simple_svg_draw)
	{
		//! [draw example]
		SVGCanvas canvas;
		canvas.set_line_width(0.5);

		const Real x[]={50.,0., 100.};
		const Real y[]={0., 86.6, 86.6};
		const Real c[]={1.,0,0, 0.,1.,0., 0.,0.,1.};

		canvas.fill_triangle(x, y, c);



		canvas.set_dashing(0.3);
		canvas.begin_clip_rect(90,90,20,20);
		canvas.stroke_rect(100, 100, 30, 45);
		canvas.end_clip_rect();

		canvas.write("test_simple_draw.svg");
	}




	M_TEST_CASE(EPSTest, draw)
	{
		//! [draw example]
		Path path = "../data/Star0.poly";

		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );


		Mesh m;

		TriangulatorOptions opts;
		opts.maximum_area = 10;
		triangulate(poly,m,opts);


		EPSCanvas canvas;
		canvas.set_line_width(0.5);

		canvas.stroke_polygon(poly);

		canvas.set_color(1,0.5,0);
		canvas.fill_polygon(poly);


		canvas.set_color(0,0.5,1);
		canvas.set_dashing(0.1);

		canvas.stroke_mesh(m);

		canvas.write("test_canvas.eps");
	}



	M_TEST_CASE(EPSTest, draw_function)
	{
		//! [draw_function example]
		Path path = "../data/Star0.poly";

		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );


		Mesh mesh;

		TriangulatorOptions opts;
		opts.maximum_area = 5;
		triangulate(poly,mesh,opts);

		std::vector<Real> cols(mesh.n_nodes()*3);
		std::fill(cols.begin(),cols.end(),1);

		for(Integer i=0; i < mesh.n_nodes(); ++i)
		{
			const Real x = mesh.points[i*mesh.dim];
			const Real y = mesh.points[i*mesh.dim+1];

			const Real func=sin(1.0/140*3*3.14*x)*cos(1.0/280*3*3.14*y);
			cols[i*3] = (func+1)/2;
		}

		EPSCanvas canvas;
		canvas.set_line_width(0.5);
		canvas.fill_mesh(mesh, cols);

		canvas.stroke_polygon(poly);



		opts.maximum_area = 1;
		triangulate(poly,mesh,opts);


		std::vector<Real> func(mesh.n_nodes());
		for(std::size_t i=0; i < func.size(); ++i)
		{
			const Real x = mesh.points[i*mesh.dim];
			const Real y = mesh.points[i*mesh.dim+1];

			func[i]=sin(1.0/140*3*3.14*x)*cos(1.0/280*3*3.14*y);
		}

		std::vector<Real> isolines(11);
		for(Integer i=0; i<=10; ++i)
			isolines[i]=2*i/10.0-1;


		draw_countours(mesh,func, isolines, canvas);

		canvas.write("test_canvas_func.eps");




		//! [draw_function example]
	}

	M_TEST_CASE(EPSTest, draw_svg)
	{
		//! [draw example]
		Path path = "../data/Star0.poly";

		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );


		Mesh m;

		TriangulatorOptions opts;
		opts.maximum_area = 10;
		triangulate(poly,m,opts);


		SVGCanvas canvas;
		canvas.set_line_width(0.5);

		canvas.stroke_polygon(poly);

		canvas.set_color(1,0.5,0);
		canvas.fill_polygon(poly);


		canvas.set_color(0,0.5,1);
		canvas.set_dashing(0.1);

		canvas.stroke_mesh(m);

		canvas.write("test_canvas.svg");
	}

	M_TEST_CASE(EPSTest, draw_svg_function)
	{
		//! [draw_function example]
		Path path = "../data/Star0.poly";

		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );


		Mesh mesh;

		TriangulatorOptions opts;
		opts.maximum_area = 5;
		triangulate(poly,mesh,opts);

		std::vector<Real> cols(mesh.n_nodes()*3);
		std::fill(cols.begin(),cols.end(),1);

		for(Integer i=0; i < mesh.n_nodes(); ++i)
		{
			const Real x = mesh.points[i*mesh.dim];
			const Real y = mesh.points[i*mesh.dim+1];

			const Real func=sin(1.0/140*3*3.14*x)*cos(1.0/280*3*3.14*y);
			cols[i*3] = (func+1)/2;
		}

		SVGCanvas canvas;
		canvas.set_line_width(0.5);
		canvas.fill_mesh(mesh, cols);

		canvas.stroke_polygon(poly);



		opts.maximum_area = 1;
		triangulate(poly,mesh,opts);


		std::vector<Real> func(mesh.n_nodes());
		for(std::size_t i=0; i < func.size(); ++i)
		{
			const Real x = mesh.points[i*mesh.dim];
			const Real y = mesh.points[i*mesh.dim+1];

			func[i]=sin(1.0/140*3*3.14*x)*cos(1.0/280*3*3.14*y);
		}

		std::vector<Real> isolines(11);
		for(Integer i=0; i<=10; ++i)
			isolines[i]=2*i/10.0-1;


		draw_countours(mesh,func, isolines, canvas);

		canvas.write("test_canvas_func.svg");




		//! [draw_function example]
	}


	M_TEST_CASE(EPSTest, plotter)
	{
		//! [plotter example]
		
		std::vector<Real> x;
		lin_space(0.1,3,10, x);
		std::vector<Real> y(x.size());
		for(std::size_t i=0; i< y.size(); ++i)
			y[i]=exp(x[i]);

		plot_data data;
		
		data.x = &x[0];
		data.y = &y[0];
		data.n_data = x.size();
		data.name = "Exponential";

		data.line_color[0] = 1;
		data.line_color[1] = 0;
		data.line_color[2] = 0;

		data.show_marker = true;

		const Integer n_data = 2;
		plot_data datas[] = { data, data };

		axis_data axis;
		axis.axis[0]=x.front();
		axis.axis[1]=x.back();

		axis.axis[2]=y.front();
		axis.axis[3]=y.back();

		axis.init_ticks(10, 10);
		axis.log_y = true;

		legend legend;
		legend.show = true;
		legend.text_width = 80;
		legend.position = legend::NORTH;

		plot_options opts;

		opts.fit_to_size(axis, 150, 150);

		{
			EPSCanvas canvas;
			plot(axis, legend, datas, n_data, canvas, opts);

			canvas.write("test_plotter.eps");
		}


		{
			SVGCanvas canvas;
			plot(axis, legend, datas, n_data, canvas, opts);

			canvas.write("test_plotter.svg");
		}




		//! [plotter example]
	}

};

M_TEST_SUITE_REGISTRATION(EPSTest);
