


#include "moonolith_test.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_triangulator.hpp"
#include "moonolith_tetrahedralizer.hpp"
#include "moonolith_dual_graph.hpp"

#include "moonolith_poly_io.hpp"
#include "moonolith_obj_io.hpp"
#include "moonolith_mesh_io.hpp"

#include "moonolith_path.hpp"
#include "moonolith_hybrid_mesh.hpp"


#include <sstream>
#include <cmath>

using namespace moonolith;

M_TEST_CLASS(MeshingTest) {
	M_TEST_SUITE_BEGIN( MeshingTest );		
	M_TEST_CASE_ADD( build_mesh );
	M_TEST_CASE_ADD( triangulation );
	M_TEST_CASE_ADD( tetrahedralizer );
	M_TEST_CASE_ADD( dual_graph );
	M_TEST_CASE_ADD( hybrid_mesh );
	M_TEST_SUITE_END( MeshingTest );

	static void build_test_mesh(Mesh &m)
	{
		//! [build_mesh example]

		const Integer dim = 2;
		const Integer n_nodes = 4;
		const Integer n_elem = 2;

		//for efficiency reserve space
		m.el_ptr.reserve(n_elem + 1);
		m.el_index.reserve(n_elem * 3);
		m.points.reserve(n_nodes * dim);

		//corner of unit square
		const Integer v1 = m.add_node(0, 0);
		const Integer v2 = m.add_node(1, 0);
		const Integer v3 = m.add_node(1, 1);
		const Integer v4 = m.add_node(0, 1);

		//2 triangles dividing the square in 2
		m.add_element({v1, v2, v3}, TRI3); //without tagging: m.add_element({v1, v2, v3});
		m.add_element({v2, v3, v4}, TRI3); //without tagging: m.add_element({v2, v3, v4});

		//! [build_mesh example]
	}

	M_TEST_CASE(MeshingTest, build_mesh)
	{
		Mesh m;
		build_test_mesh(m);

		M_TEST_ASSERT_EQ(2, m.n_elements());
		M_TEST_ASSERT_EQ(4, m.n_nodes());

		const Integer n_elements = m.n_elements();
		for(Integer i = 0; i < n_elements; ++i) {
			const Integer n_nodes = m.n_nodes(i);

			M_TEST_ASSERT_EQ(3, n_nodes);

			const Integer *e = m.element(i);

			for(Integer j = 0; j < n_nodes; ++j) {
				M_TEST_ASSERT( e[j] >= 0 );
				M_TEST_ASSERT( e[j] <  4 );
			}
		}
	}


	M_TEST_CASE(MeshingTest, triangulation)
	{
		//! [triangulation example]
		Path path = "../data/Star0.poly";

		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );

		Mesh m;

		triangulate(poly,m);
		write_obj_mesh("test.obj",m);

		
		TriangulatorOptions opts;

		opts.maximum_area = 5;
		triangulate(poly,m,opts);
		write_obj_mesh("test1.obj",m);


		opts.convex_hull = true;
		triangulate(poly,m, opts);
		write_obj_mesh("test2.obj",m);
		//! [triangulation example]
	}

	M_TEST_CASE(MeshingTest, tetrahedralizer)
	{
		//! [tetrahedralizer example]

		Path path = "../data/star0.tri";
		
		Mesh mesh;
		M_TEST_ASSERT( read_mesh(path, mesh) );

		Mesh out;
		TetrahedralizerOptions opts;
		opts.maximum_volume = 5;
		// opts.verbose = true;
		opts.convex_hull = true;
		opts.mark_bounday = true;
		tetrahedralize(mesh, out, opts);

		write_mesh("test_tetrahedralizer.node", out);
		//! [tetrahedralizer example]
	}

	M_TEST_CASE(MeshingTest, dual_graph)
	{
		Mesh m;
		build_test_mesh(m);

		DualGraph dg;
		dg.init_unordered(m);

		M_TEST_ASSERT(dg.are_adjacent(0, 1));
		M_TEST_ASSERT(dg.are_adjacent(1, 0));
	}

	M_TEST_CASE(MeshingTest, hybrid_mesh)
	{
		Mesh m;
		Integer n_points = 20;
		const Real c_x = 0.0;
		const Real c_y = 0.0;

		const Real r = 20.0;
		const Real d_angle = 2 * M_PI/(n_points);


		const Real r_2 = r/8.;
		const Real freq_2 = 8;

		//allocate space for efficiency
		m.points.reserve(n_points*2);
		m.el_ptr.reserve(n_points + 1);
		m.el_index.reserve(n_points * 2);
		m.elem_type.reserve(n_points);

		for(Integer i = 0; i < n_points; ++i) {
			const Real angle = d_angle * i;

			const Real x = c_x + r * cos(angle) + r_2 * cos(freq_2 * angle);
			const Real y = c_y + r * sin(angle) + r_2 * sin(freq_2 * angle);

			m.add_node(x, y);
		}


		for(Integer i = 0; i < n_points - 1; ++i) {
			const Real *p1 = m.point(i);
			const Real *p2 = m.point(i + 1);
			const Real dx = p2[0]-p1[0];
			const Real dy = p2[1]-p1[1];
			const Real l  = sqrt(dx*dx+dy*dy);

			m.add_normal(dy/l, -dx/l);

			m.add_element({i, i + 1}, {i, i}, {}, ElemType::EDGE2);
			
		}

		const Real *p1 = m.point(n_points-1);
		const Real *p2 = m.point(0);

		const Real dx = p2[0]-p1[0];
		const Real dy = p2[1]-p1[1];
		const Real l  = sqrt(dx*dx+dy*dy);

		m.add_normal(dy/l, -dx/l);

		m.add_element({n_points-1, 0}, {n_points-1, n_points-1}, {}, ElemType::EDGE2);



		HybridMesh hm;
		hm.build({50, 50}, m);
	}

};

M_TEST_SUITE_REGISTRATION(MeshingTest);
