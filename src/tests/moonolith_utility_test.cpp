
#include "moonolith_chrono.hpp"
#include "moonolith_path.hpp"
#include "moonolith_command_line.hpp"
#include "moonolith_test.hpp"


using namespace moonolith;

inline static std::string str(const char *arr)
{
	return arr;
}

void example_callback(const int, const char**)
{
	//do something
}

M_TEST_CLASS(UtilityTest) {
	M_TEST_SUITE_BEGIN( UtilityTest );		
		M_TEST_CASE_ADD( chrono );
		M_TEST_CASE_ADD( path );
		M_TEST_CASE_ADD( command_line );
	M_TEST_SUITE_END( UtilityTest );

	M_TEST_CASE(UtilityTest, chrono)
	{
		//! [chrono example]
		Chrono c;
		c.start();
		//some code
		c.stop();
		double elapsed_seconds = c.get_seconds();
		(void) elapsed_seconds;
		//! [chrono example]
	}

	M_TEST_CASE(UtilityTest, path)
	{
		//! [path example]
		Path parent = "Desktop/adirectory";
		Path child  = "./somefile.txt";

		Path path = parent / child;
		Path path_no_ext = path.without_extension();

		std::string name 	  = path.file_name();
		std::string extension = path.extension();

		M_TEST_ASSERT_EQ(str("somefile"), name);
		M_TEST_ASSERT_EQ(str("txt"), extension);
		M_TEST_ASSERT_EQ(str("Desktop/adirectory/./somefile.txt"), path.to_string());
		M_TEST_ASSERT_EQ(str("Desktop/adirectory/."), path.parent().to_string());
		M_TEST_ASSERT_EQ(str("Desktop/adirectory/./somefile"), path_no_ext.to_string());
		M_TEST_ASSERT_EQ(str("Desktop/adirectory/./somefile.jpg"), (path_no_ext + ".jpg").to_string());

		for(Path::Iterator it = parent.iter(); it; ++it) {
			Path file = *it;
			//do something
			(void) file;
		} 
		//! [path example]
	}



	M_TEST_CASE(UtilityTest, command_line)
	{
		//! [command_line example]
		double double_opt = 0.0;
		std::string str_opt = "default-value";
		bool bool_true_opt = false;
		bool bool_false_opt = true;
		int int_opt = 0;

		static const int argc = 11;
		const char* argv[argc] = {
			"exec_name",
			"-d", "1.0",
			"-s", "user-value",
			"-k",
			"-no-j",
			"-i", "2",
			"-example_callback",
			"-example_callback_2"
		};

		OptArgs opts;
		
		//options are all parsed at the beginning 
		opts.add_option("-d", double_opt);
		opts.add_option("-s", str_opt);
		opts.add_option("-k", "-no-k", bool_true_opt);
		opts.add_option("-j", "-no-j", bool_false_opt);
		opts.add_option("-i", int_opt);

		// callback respect the order given by argv
		int called = 0;
		opts.add_callback("-example_callback", [&called](const int, const char**) -> void {
			called = 1;
		});

		opts.add_callback("-example_callback_2", [&called](const int, const char**) -> void {
			M_TEST_ASSERT_EQ(1, called);
			called = 2;
		});

		opts.add_callback("-example_not_called", example_callback);
		opts.parse(argc, argv);

		M_TEST_ASSERT_EQ(1.0, double_opt);
		M_TEST_ASSERT_EQ(str("user-value"), str_opt);
		M_TEST_ASSERT_EQ(true, bool_true_opt);
		M_TEST_ASSERT_EQ(false, bool_false_opt);
		M_TEST_ASSERT_EQ(2, called);
		
		//! [command_line example]
	}

};

M_TEST_SUITE_REGISTRATION(UtilityTest);
