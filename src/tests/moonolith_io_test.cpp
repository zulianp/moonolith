

#include <sstream>

#include "moonolith_test.hpp"
#include "moonolith_matrix_io.hpp"
#include "moonolith_tri_io.hpp"

#include "moonolith_obj_io.hpp"
#include "moonolith_off_io.hpp"
#include "moonolith_poly_io.hpp"
#include "moonolith_ele_io.hpp"
#include "moonolith_command_line.hpp"
#include "moonolith_path.hpp"

//FIXME (make module dependent config file)
#define MOONOLITH_WITH_EXODUS_II 

#ifdef MOONOLITH_WITH_EXODUS_II
#include "moonolith_exodus_io.hpp"
#endif //MOONOLITH_WITH_EXODUS_II


using namespace moonolith;

M_TEST_CLASS(IOTest) {
	M_TEST_SUITE_BEGIN( IOTest );
	M_TEST_CASE_ADD( matrix_io );
	M_TEST_CASE_ADD( tri_io );
	M_TEST_CASE_ADD( obj_io );
	M_TEST_CASE_ADD( off_io );
	M_TEST_CASE_ADD( poly_io );
	M_TEST_CASE_ADD( ele_io );
	
#ifdef MOONOLITH_WITH_EXODUS_II
	M_TEST_CASE_ADD( exodus_io );
#endif //MOONOLITH_WITH_EXODUS_II
	
	M_TEST_SUITE_END( IOTest );
	
	M_TEST_CASE(IOTest, matrix_io)
	{
		//! [matrix_io example]
		
		Integer rows, cols;
		std::vector<Real> values;
		std::vector<Real> expected{0.1, 2.0, 3.0,
			0.4, 5.0, 6000,
			7, 8, 9,
			-1, -2, -3};
		
		
		std::string mat_string = "0.1,2.0,3.0\n0.4,5.0,6000\n7,8,9\n-1,-2,-3\n";
		std::istringstream ss(mat_string);
		
		// for reading from disk use a path instead of mat_string
		// Path path = "path_to_matrix.txt";
		
		M_TEST_ASSERT( read_matrix(ss, values, rows, cols) );
		
		M_TEST_ASSERT_EQ(Integer(4), rows);
		M_TEST_ASSERT_EQ(Integer(3), cols);
		
		M_TEST_ASSERT_EQ(std::size_t(rows * cols), values.size());
		
		for(std::size_t i = 0; i < values.size(); ++i) {
			M_TEST_ASSERT_EQ(expected[i], values[i]);
		}
		
		std::stringstream sos;
		write_matrix(sos, values, rows, cols);
		ss.str(sos.str());
		ss.clear();
		
		M_TEST_ASSERT( read_matrix(ss, values, rows, cols) );
		
		M_TEST_ASSERT_EQ(Integer(4), rows);
		M_TEST_ASSERT_EQ(Integer(3), cols);
		
		M_TEST_ASSERT_EQ(std::size_t(rows * cols), values.size());
		
		for(std::size_t i = 0; i < values.size(); ++i) {
			M_TEST_ASSERT_EQ(expected[i], values[i]);
		}
		
		//! [matrix_io example]
	}
	
	static void make_example(Mesh &mesh)
	{
		mesh.dim = 2;
		mesh.points =
		{
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0
		};
		
		mesh.el_index = { 0, 1, 2 };
	}
	
	
	M_TEST_CASE(IOTest, tri_io)
	{
		//! [tri_io example]
		
		Mesh ex_m; make_example(ex_m);
		Mesh m;
		
		
		//example string from .tri files
		std::string points_string = "0.0 0.0\n1.0 0.0\n0.0 1.0\n";
		std::string tri_string  = "1 2 3\n";
		std::istringstream points_ss(points_string);
		std::istringstream tri_ss(tri_string);
		
		// for reading from disk use a path instead of mat_string
		// Path path = "path_to_matrix.txt";
		
		
		M_TEST_ASSERT( read_tri_mesh(points_ss, tri_ss, m) );
		M_TEST_ASSERT_EQ(Integer(2), m.dim);
		M_TEST_ASSERT_EQ(std::size_t(3), m.el_index.size());
		
		for(std::size_t i = 0; i < ex_m.points.size(); ++i) {
			M_TEST_ASSERT_EQ(ex_m.points[i], m.points[i]);
		}
		
		for(std::size_t i = 0; i < ex_m.el_index.size(); ++i) {
			M_TEST_ASSERT_EQ(ex_m.el_index[i], m.el_index[i]);
		}
		
		
		//! [tri_io example]
	}
		
	M_TEST_CASE(IOTest, obj_io)
	{
		//! [obj_io example]
		Path obj_path = "../data/cyl.obj";
		
		Mesh mesh;
		M_TEST_ASSERT( read_obj_mesh(obj_path, mesh) );
		
		std::ostringstream stream;
		
		M_TEST_ASSERT(write_obj_mesh(stream, mesh));
		
		// std::cout<<stream.str()<<std::endl;
		
		Mesh mesh1;
		std::istringstream io(stream.str());
		
		M_TEST_ASSERT(read_obj_mesh(io,mesh1));
		
		M_TEST_ASSERT_EQ(mesh, mesh1);
		//! [obj_io example]
	}
	
	M_TEST_CASE(IOTest, off_io)
	{
		//! [off_io example]
		Path path = "../data/square0.off";
		
		Mesh mesh;
		M_TEST_ASSERT( read_off_mesh(path, mesh) );
		
		std::ostringstream stream;
		
		M_TEST_ASSERT(write_off_mesh(stream, mesh));
		
		// std::cout<<stream.str()<<std::endl;
		
		Mesh mesh1;
		std::istringstream io(stream.str());
		
		M_TEST_ASSERT(read_off_mesh(io,mesh1));
		
		M_TEST_ASSERT_EQ(mesh, mesh1);
		//! [off_io example]
	}
	
	
	M_TEST_CASE(IOTest, poly_io)
	{
		//! [poly_io example]
		Path path = "../data/Star0.poly";
		
		std::vector<Real> poly;
		M_TEST_ASSERT( read_poly(path, poly) );
		
		std::ostringstream stream;
		
		M_TEST_ASSERT(write_poly(stream, poly));
		
		// std::cout<<stream.str()<<std::endl;
		
		std::vector<Real> poly1;
		std::istringstream io(stream.str());
		
		M_TEST_ASSERT(read_poly(io, poly1));
		
		M_TEST_ASSERT(poly == poly1);
		
		//! [poly_io example]
	}
	
	M_TEST_CASE(IOTest, ele_io)
	{
		Path path = "../data/tetrahedron.ele";
		Mesh expected, actual;
		std::stringstream ss;
		
		M_TEST_ASSERT( read_ele_mesh(path, expected) );
		M_TEST_ASSERT( write_ele_mesh(ss, expected) );
		M_TEST_ASSERT( read_ele_mesh(ss, actual) );
		M_TEST_ASSERT_EQ(expected, actual);
	}


#ifdef MOONOLITH_WITH_EXODUS_II
	M_TEST_CASE(IOTest, exodus_io)
	{
		//! [exodus_io example]
		
		Path exodus_path = "../data/exodus_io_test_mesh.e";
		
		Mesh m;
		M_TEST_ASSERT( read_exodus_mesh(exodus_path, m) );
		
		//! [exodus_io example]
	}
#endif //MOONOLITH_WITH_EXODUS_II
	
};

M_TEST_SUITE_REGISTRATION(IOTest);
