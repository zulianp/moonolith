

#include "moonolith_hash_grid.hpp"

#include <assert.h>
#include <cmath>

namespace moonolith {
	void HashGrid::describe(std::ostream &os) const
	{
		os << "box:\n";
		box_.describe(os);

		os << "range:\n";
		for(auto r : range_) 
			os << r << " ";
		os << "\n";

		os << "dims: ";
		for(auto d : dims_) {
			os << d << " ";
		}

		os << "\n";
		os << "n_cells: " << n_cells_ << "\n";
	}

	Integer HashGrid::hash(const std::vector<Real> &point) const
	{
		Real x = (point[0] - box_.get_min(0))/range_[0];
		Integer result = floor(x * dims_[0]);

		Integer total_dim = dims_[0];

		for(std::size_t i = 1; i < range_.size(); ++i) {
			result *= dims_[i];

			x = (point[i] - box_.get_min(i))/range_[i];
			result += floor(x * dims_[i]);
			total_dim *= dims_[i];
		}

		if(result >= total_dim || result < 0) {
			printf("error -> %ld\n", result);
		}

		return result;
	}

	Integer HashGrid::hash(const std::vector<Integer> &coord) const
	{
		Integer result   = coord[0];
		Integer total_dim = dims_[0];

		for(std::size_t i = 1; i < range_.size(); ++i) {
			result *= dims_[i];
			result += coord[i];
			total_dim *= dims_[i];
		}

		assert(result >= 0 && "Integer overflow: change type of Integer to larger representation");
		return result;
	}

	void HashGrid::hash_range(const std::vector<Real> &min, const std::vector<Real> &max, std::vector<Integer> &hashes)
	{
		const Integer dim = min.size();
		std::vector<Integer> imin(dim), imax(dim);

		//generate tensor indices
		for(Integer i = 0; i < dim; ++i) {
			Real x = (min[i] - box_.get_min(i))/range_[i];
			imin[i] = floor(x * dims_[i]);
		}

		for(Integer i = 0; i < dim; ++i) {
			Real x = (max[i] - box_.get_min(i))/range_[i];
			imax[i] = floor(x * dims_[i]);
		}

		std::vector<Integer> offsets(dim);
		for(Integer i = 0; i < dim; ++i) {
			offsets[i] = imax[i] - imin[i];
		}

		//FIXME make more general for greater dim
		if(dim == 1) {
			std::vector<Integer> coord(1);
			for(Integer i = imin[0]; i <= imax[0]; ++i) {
				coord[0] = i;
				hashes.push_back(hash(coord)); 
			}

		} else if(dim == 2) {
			std::vector<Integer> coord(2);
			for(Integer i = imin[0]; i <= imax[0]; ++i) {
				// if(i < 0) continue;
				if(i < 0 || i >= dims_[0]) continue;

				for(Integer j = imin[1]; j <= imax[1]; ++j) {
					// if(j < 0) continue;
					if(j < 0 || j >= dims_[1]) continue;

					coord[0] = i;
					coord[1] = j;
					hashes.push_back(hash(coord)); 
				}
			}
		} else if(dim == 3) {
			std::vector<Integer> coord(3);
			for(Integer i = imin[0]; i <= imax[0]; ++i) {
				for(Integer j = imin[1]; j <= imax[1]; ++j) {
					for(Integer k = imin[2]; k <= imax[2]; ++k) {
						coord[0] = i;
						coord[1] = j;
						coord[2] = k;
						hashes.push_back(hash(coord)); 
					}
				}
			}
		} else {
			assert(false && "dim > 3 not supported yet!");
		}

		// assert(!hashes.empty());
	}

	HashGrid::HashGrid(const Box &box, const std::vector<Integer> &dims)
	: box_(box), dims_(dims), n_cells_(1)
	{
		box_.enlarge(1e-8);
		range_ = box_.get_max();
		
		for(std::size_t i = 0; i < dims_.size(); ++i) {
			n_cells_ *= dims_[i];
			range_ [i] -= box_.get_min(i);
		}
	}

	void HashGrid::reset(const Box &box, const std::vector<Integer> &dims)
	{
		box_ = box;
		dims_ = dims;
		n_cells_ = 1;

		box_.enlarge(1e-8);
		range_ = box_.get_max();
		
		for(std::size_t i = 0; i < dims_.size(); ++i) {
			n_cells_ *= dims_[i];
			range_ [i] -= box_.get_min(i);
		}
	}

	HashGrid::HashGrid()
	: n_cells_(0)
	{}

	void HashGrid::hash_to_tensor_index(const Integer &hash, std::vector<Integer> &index) const
	{
		index.resize(dims_.size());
		std::fill(index.begin(), index.end(), 0);

		const Integer dim = range_.size();
		
		Integer current = hash;
		for(Integer i = dim - 1; i >= 0; --i) {
			const Integer next = current / dims_[i];
			index[i] = current - next * dims_[i];
			current = next;	
		}
	}


	void HashGrid::coord(const std::vector<Integer> &tensor_index, std::vector<Real> &coord) const
	{
		coord.resize(tensor_index.size());

		for(std::size_t i = 0; i < tensor_index.size(); ++i) {
			coord[i] = box_.get_min(i) + (tensor_index[i] * range_[i]/dims_[i]);
		}	
	}

	void HashGrid::coord(const Integer &hash, std::vector<Real> &coord) const
	{
		std::vector<Integer> tensor_index;
		hash_to_tensor_index(hash, tensor_index);
		this->coord(tensor_index, coord);
	}

	bool HashGrid::find_intersecting_cells(
		const std::vector<Box> &boxes, 
		std::vector< std::vector<Integer> > &box_to_cell_hashes)
	{
		box_to_cell_hashes.clear();
		box_to_cell_hashes.resize(boxes.size());

		bool found_intersections = false;
		std::vector<Integer> hashes;

		for(std::size_t i = 0; i < boxes.size(); ++i) {
			const Box &b = boxes[i];
			hashes.clear();
			hash_range(b.get_min(), b.get_max(), box_to_cell_hashes[i]);

			found_intersections |= !box_to_cell_hashes[i].empty();
		}

		return found_intersections;
	}

}
