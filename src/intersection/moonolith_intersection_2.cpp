#include "moonolith_intersection_2.hpp"
#include "moonolith_vec.hpp"

#include <vector>
#include <cmath>

#define INSIDE  1
#define ON_EDGE 2
#define OUTSIDE 0

// #define print_isect_query(macro_x) 											\
// { 																			\
// 	const char * macro_symbols[3] = { "OUTSIDE", "INSIDE", "ON_EDGE" };		\
// 	printf("%s\n", macro_symbols[(macro_x)]);								\
// }
#define print_isect_query(x)


namespace moonolith {


	void points_to_plane_distance(const std::vector<Real> &normal, 
								  const Real d, 
								  const std::vector<Real> &points, 
							 	  std::vector<Real> &distances)
	{
		Vector2 n, p;

		const Integer dim = normal.size();
		const Integer n_points = points.size()/dim;

		distances.resize(n_points);

		n.x = normal[0];
		n.y = normal[1];
		
		assert(dim == 2 && "only 2d supported");

		for(Integer i = 0; i < n_points; ++i) {
			const Integer i_d = i * dim;
			p.x = points[i_d];
			p.y = points[i_d + 1];

			distances[i] = dot(n, p) - d;
		}
	}

	bool polygon_intersects_plane(const std::vector<Real> &normal, 
								 const Real d,
								 const std::vector<Real> &polygon)
	{
		std::vector<Real> distances;
		points_to_plane_distance(normal, d, polygon, distances);

		for(std::size_t i = 0; i < distances.size(); ++i) {
			std::size_t ip1 = (i+1) % distances.size();

			if(std::abs(distances[i]) < 1e-16) {
				return true;
			}

			if(std::signbit(distances[i]) != std::signbit(distances[ip1])) {
				return true;
			}
		}

		return false;
	}

	bool intersect_lines(const std::vector<Real> &line_1, 
						 const std::vector<Real> &line_2, 
						 std::vector<Real> &result)
	{
		//first line
		const Vector2 l_1a(line_1[0], line_1[1]); 
		const Vector2 l_1b(line_1[2], line_1[3]);

		//second line
		const Vector2 l_2a(line_2[0], line_2[1]);
		const Vector2 l_2b(line_2[2], line_2[3]);

		const Vector2 u = l_1b - l_1a;
		const Vector2 v = l_2b - l_2a;
		const Vector2 w = l_2b - l_1a;

		const Real len_u = length(u);

		const bool are_collinear = std::abs(dot(u, v)/(len_u * length(v)) - 1) < 1e-8;

		if(are_collinear) {
			Vector2 n_1(u.y/len_u, -u.x/len_u);
			if(std::abs(dot(n_1, w)) > 1e-8) {
				//aligned but not interecting
				return false;
			}

			result.resize(4);
			result[0] = std::min(std::min(l_1a.x, l_1b.x), std::min(l_2a.x, l_2b.x));
			result[1] = std::min(std::min(l_1a.y, l_1b.y), std::min(l_2a.y, l_2b.y));

			result[2] = std::max(std::max(l_1a.x, l_1b.x), std::max(l_2a.x, l_2b.x));
			result[3] = std::max(std::max(l_1a.y, l_1b.y), std::max(l_2a.y, l_2b.y));
			return true;
		}

		const Real denom = (u.x * v.y - u.y * v.x);
		const Real num   =  -(-w.x * v.y + w.y * v.x);

		const Real lambda = num/denom;
		const Vector2 temp = l_1a + (lambda * u);
		
		result.resize(2);
		result[0] = temp.x;
		result[1] = temp.y;
		return true;
	}

	Vector2 intersect_lines(const Vector2 line_1a, const Vector2 line_1b, const Vector2 line_2a, const Vector2 line_2b)
	{
		const Vector2 u = line_1b - line_1a;
		const Vector2 v = line_2b - line_2a;
		const Vector2 w = line_2b - line_1a;
		
		const Real lambda = -(-w.x * v.y + w.y * v.x)/(u.x * v.y - u.y * v.x);
		return line_1a + (lambda * u);
	}
		
	Integer collapse_quasi_equal_points(const Integer current_n_points, std::vector<Real> &points, Real tol, IntersectConvexPolygonsWork &work)
	{
		auto &keep = work.keep;
		keep.resize(current_n_points);
		
		keep[0] = true;
		
		Integer result_n_points = current_n_points;
		for(Integer i = 1; i < result_n_points; ++i) {
			const Integer ix = i * 2;
			const Integer iy = ix + 1;
			
			const Integer im1x = ix   - 2;
			const Integer im1y = im1x + 1;
			
			if( distance( vec_2(points[ix], points[iy]), vec_2(points[im1x], points[im1y]) ) <= tol ) {
				result_n_points--;
				keep[i] = false;
			} else {
				keep[i] = true;
			}
		}
		
		const Integer last = (current_n_points-1) * 2;
		
		if( distance( vec_2(points[0], points[1]),
					 vec_2(points[last], points[last+1]) ) <= tol ) {
			result_n_points--;
			keep[current_n_points-1] = false;
			
		} else {
			keep[current_n_points-1] = true;
		}
		
		Integer range_to_fill_begin = -1;
		bool must_shift = false;
		
		for(Integer i = 1; i < current_n_points; ++i) {
			if(keep[i] && must_shift) {
				assert(range_to_fill_begin > 0);
				
				const Integer to = range_to_fill_begin * 2;
				const Integer from = i * 2;
				
				std::copy(&points[from], &points[from] + 2, &points[to]);
				
				keep[i] = false;
				
				if(keep[++range_to_fill_begin]) {
					range_to_fill_begin = -1;
					must_shift = false;
				}
			}
			
			if(!keep[i] && !must_shift) {
				must_shift = true;
				range_to_fill_begin = i;
			}
		}
		
		points.resize(result_n_points * 2);
		
		return result_n_points;
	}
	
	short inside_half_plane(const Vector2 e1, const Vector2 e2, const Vector2 point, const Real tol)
	{
		const Vector2 u = e1 - e2;
		const Vector2 v = point - e2;
		
		const Real dist = (u.x * v.y) - (v.x * u.y);
		
		if(dist < -tol) {
			return INSIDE;
		}
		
		if(dist > tol) {
			return OUTSIDE;
		}
		
		return ON_EDGE;
	}
	
	bool intersect_convex_polygons(const std::vector<Real> &polygon_1,
								   const std::vector<Real> &polygon_2,
								   std::vector<Real> &result,
								   const Real tol,
								   IntersectConvexPolygonsWork &work)
	{
		work.reset();
		result.resize(0);

		auto &is_inside     = work.is_inside;
		auto &input_buffer  = work.input_buffer;
		
		Integer n_output_points = 0;
		
		std::vector<Real> * input  = &input_buffer;
		std::vector<Real> * output = &result;
		
		input->resize(polygon_2.size());
		std::copy(polygon_2.begin(), polygon_2.end(), input->begin());
		
		Vector2 e1, e2, p, s, e;
		
		const Integer n_vertices_1 = polygon_1.size() / 2;
		for(Integer i = 0; i < n_vertices_1; ++i) {
			if(i > 0) {
				std::swap(input, output);
				output->clear();
				n_output_points = 0;
			}
			
			const Integer i2x = i * 2;
			const Integer i2y = i2x + 1;
			
			const Integer i2p1x = 2 * (((i + 1) == n_vertices_1)? 0 : (i + 1));
			const Integer i2p1y = i2p1x + 1;
			
			e1 = vec_2(polygon_1[i2x],   polygon_1[i2y]);
			e2 = vec_2(polygon_1[i2p1x], polygon_1[i2p1y]);
			
			const Integer n_input_points = input->size()/2;
			if(static_cast<Integer>(is_inside.size()) < n_input_points) {
				is_inside.resize(n_input_points);
			}
			
			Integer n_outside = 0;
			for(Integer j = 0; j < n_input_points; ++j) {
				const Integer jx = j * 2;
				const Integer jy = jx + 1;
				
				p = vec_2(input->at(jx), input->at(jy));
				is_inside[j] = inside_half_plane(e1, e2, p, tol);
				n_outside += is_inside[j] != INSIDE;
			}
			
			if(n_input_points - n_outside == 0) return false;
			
			for(Integer j = 0; j < n_input_points; ++j) {
				const Integer jx = j * 2;
				const Integer jy = jx + 1;
				
				const Integer jp1 = (j + 1 == n_input_points)? 0 : (j + 1);
				const Integer jp1x = jp1 * 2;
				const Integer jp1y = jp1x + 1;
				
				s = vec_2(input->at(jx), input->at(jy));
				e = vec_2(input->at(jp1x), input->at(jp1y));
				
				if(is_inside[j]) {
					
					output->push_back(s.x);
					output->push_back(s.y);
					++n_output_points;
					
					if( ( is_inside[j] != ON_EDGE ) && ( !is_inside[jp1] ) ) {
						auto isect = intersect_lines(e1, e2, s, e);
						
						output->push_back(isect.x);
						output->push_back(isect.y);
						++n_output_points;
					}
				} else if(is_inside[jp1]) {
					auto isect = intersect_lines(e1, e2, s, e);
					
					output->push_back(isect.x);
					output->push_back(isect.y);
					++n_output_points;
				}
			}
			
			if(n_output_points < 3) {
				return false;
			}
			
			n_output_points = collapse_quasi_equal_points(n_output_points, *output, tol, work);
			
			if(n_output_points < 3) {
				return false;
			}
		}
		
		if(output != &result) {
			std::swap(*output, result);
		}

		result.resize(n_output_points * 2);
		
		return n_output_points >= 3;
	}

	bool clip_polygon(const std::vector<Real> &polygon,
					  const std::vector<Real> &clipping_polygon,
					  std::vector<Real> &result,
					  const Real tol,
					  IntersectConvexPolygonsWork &work)
	{
		work.reset();
		result.resize(0);

		auto &is_inside     = work.is_inside;
		auto &input_buffer  = work.input_buffer;
		
		Integer n_output_points = 0;
		
		std::vector<Real> * input  = &input_buffer;
		std::vector<Real> * output = &result;
		
		input->resize(polygon.size());
		std::copy(polygon.begin(), polygon.end(), input->begin());
		
		Vector2 e1, e2, p, s, e;
		
		const Integer n_vertices_1 = clipping_polygon.size() / 2;
		for(Integer i = 0; i < n_vertices_1; ++i) {
			if(i > 0) {
				std::swap(input, output);
				output->clear();
				n_output_points = 0;
			}
			
			const Integer i2x = i * 2;
			const Integer i2y = i2x + 1;
			
			const Integer i2p1x = 2 * (((i + 1) == n_vertices_1)? 0 : (i + 1));
			const Integer i2p1y = i2p1x + 1;
			
			e1 = vec_2(clipping_polygon[i2x],   clipping_polygon[i2y]);
			e2 = vec_2(clipping_polygon[i2p1x], clipping_polygon[i2p1y]);
			
			const Integer n_input_points = input->size()/2;
			if(static_cast<Integer>(is_inside.size()) < n_input_points) {
				is_inside.resize(n_input_points);
			}
			
			Integer n_outside = 0;
			for(Integer j = 0; j < n_input_points; ++j) {
				const Integer jx = j * 2;
				const Integer jy = jx + 1;
				
				p = vec_2(input->at(jx), input->at(jy));
				is_inside[j] = inside_half_plane(e1, e2, p, tol);
				n_outside += is_inside[j] != INSIDE;
			}
			
			if(n_input_points - n_outside == 0) return false;
			
			for(Integer j = 0; j < n_input_points; ++j) {
				const Integer jx = j * 2;
				const Integer jy = jx + 1;
				
				const Integer jp1 = (j + 1 == n_input_points)? 0 : (j + 1);
				const Integer jp1x = jp1 * 2;
				const Integer jp1y = jp1x + 1;
				
				s = vec_2(input->at(jx), input->at(jy));
				e = vec_2(input->at(jp1x), input->at(jp1y));
				
				if(is_inside[j]) {
					
					output->push_back(s.x);
					output->push_back(s.y);
					++n_output_points;
					
					if( ( is_inside[j] != ON_EDGE ) && ( !is_inside[jp1] ) ) {
						auto isect = intersect_lines(e1, e2, s, e);
						
						output->push_back(isect.x);
						output->push_back(isect.y);
						++n_output_points;
					}
				} else if(is_inside[jp1]) {
					auto isect = intersect_lines(e1, e2, s, e);
					
					output->push_back(isect.x);
					output->push_back(isect.y);
					++n_output_points;
				}
			}
			
			n_output_points = collapse_quasi_equal_points(n_output_points, *output, tol, work);
		}
		
		if(output != &result) {
			std::swap(*output, result);
		}

		result.resize(n_output_points * 2);
		return n_output_points > 0;
	}

}

//clean up macros
#undef INSIDE
#undef ON_EDGE
#undef OUTSIDE
#undef print_isect_query

