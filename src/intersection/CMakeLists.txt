
set(LOCAL_HEADERS
	moonolith_box.hpp
	moonolith_hash_grid.hpp
	moonolith_intersection_2.hpp
	)

set(LOCAL_SOURCES
	moonolith_box.cpp
	moonolith_hash_grid.cpp
	moonolith_intersection_2.cpp
	)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ${MOONOLITH_DEV_FLAGS}")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_GLIBCXX_DEBUG -g")

add_library(intersection STATIC ${LOCAL_SOURCES})
target_include_directories(intersection PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(intersection utility)	

install(TARGETS intersection
	RUNTIME DESTINATION bin 
	LIBRARY DESTINATION lib 
	ARCHIVE DESTINATION lib 
	)

install(FILES ${LOCAL_HEADERS} DESTINATION include)

set(MOONOLITH_LIBRARIES "${MOONOLITH_LIBRARIES};intersection" PARENT_SCOPE)

