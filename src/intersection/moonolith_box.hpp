#ifndef MOONOLITH_BOX_HPP
#define MOONOLITH_BOX_HPP 

#include <vector>
#include <iostream>

namespace moonolith {
	
	class Box {
	public:

		Box(const Integer n);
		Box();
		
		virtual ~Box();

		void reset(const Integer n);
		void reset();

		Box & operator += (const std::vector<Real> &point);
		Box & operator += (const Box &box);
		
		bool intersects(const Box &other) const;
		bool intersects(const Box &other, const Real tol) const;
		
		void enlarge(const Real value);

		void describe(std::ostream &os = std::cout) const;

		inline Real get_min(const Integer index) const
		{
			return min_[index];
		}

		inline Real get_max(const Integer index) const
		{
			return max_[index];
		}

		inline const std::vector<Real> &get_min() const
		{
			return min_;
		}

		inline const std::vector<Real> &get_max() const
		{
			return max_;
		}
		
		inline std::vector<Real> &get_min() 
		{
			return min_;
		}

		inline std::vector<Real> &get_max() 
		{
			return max_;
		}
		
		inline Integer dims() const
		{
			return min_.size();
		}

		inline bool empty() const
		{
			if(min_.empty()) return true;
			return get_min(0) > get_max(0);
		}

	private:
		std::vector<Real> min_;
		std::vector<Real> max_;
	};

}

#endif //MOONOLITH_BOX_HPP
