#ifndef MOONOLITH_HASH_GRID_HPP
#define MOONOLITH_HASH_GRID_HPP 

#include "moonolith_box.hpp"
#include <vector>
#include <iostream>

namespace moonolith {

	class HashGrid {
	public:
		Integer hash(const std::vector<Real> &point) const;
		Integer hash(const std::vector<Integer> &coord) const;
		void hash_range(const std::vector<Real> &min, 
						const std::vector<Real> &max, 
						std::vector<Integer> &hashes);
		
		void hash_to_tensor_index(const Integer &hash, std::vector<Integer> &index) const;
		void coord(const Integer &hash, std::vector<Real> &coord) const;
		void coord(const std::vector<Integer> &tensor_index, std::vector<Real> &coord) const;
		
		HashGrid(const Box &box, const std::vector<Integer> &dims);
		HashGrid();

		void reset(const Box &box, const std::vector<Integer> &dims);

		inline Integer n_cells() const
		{
			return n_cells_;
		}

		inline Integer n_dims() const
		{
			return dims_.size();
		}

		void describe(std::ostream &os = std::cout) const;

		bool find_intersecting_cells(const std::vector<Box> &boxes, 
									 std::vector< std::vector<Integer> > &box_to_cell_hashes);
	private:
		Box box_;
		std::vector<Real> range_;
		std::vector<Integer> dims_;
		Integer n_cells_;
	};									   
}

#endif //MOONOLITH_HASH_GRID_HPP
