

#include "moonolith_box.hpp"
#include <assert.h>
#include <algorithm>
#include <functional>
#include <cmath>

namespace moonolith {

	Box::Box(const Integer n)
	: min_(n), max_(n)
	{
		reset();
	}

	Box::Box() {}
	Box::~Box() {}

	void Box::reset(const Integer n)
	{
		min_.resize(n);
		max_.resize(n);
		reset();
	}

	void Box::reset()
	{
		std::fill(min_.begin(), min_.end(),  std::numeric_limits<Real>::max());
		std::fill(max_.begin(), max_.end(), -std::numeric_limits<Real>::max());
	}

	Box & Box::operator += (const std::vector<Real> &point)
	{	
		using std::min;
		using std::max;

		const Integer n = dims();
		assert(n > 0);

		for(Integer i = 0; i < n; ++i) {
			min_[i] = min(point[i], min_[i]);
			max_[i] = max(point[i], max_[i]);
		}

		return *this;
	}

	Box & Box::operator +=(const Box &box)
	{
		using std::min;
		using std::max;

		if(empty()) {
			*this = box;
			return *this;
		}

		const Integer n = dims();

		for(Integer i = 0; i < n; ++i) {
			min_[i] = min(min_[i], box.min_[i]);
			max_[i] = max(max_[i], box.max_[i]);
		}

		return *this;
	}
	
	bool Box::intersects(const Box &other) const
	{
		const Integer n = dims();
		
		assert(n == other.dims() && "must have same dimensions");
		
		for(Integer i = 0; i < n; ++i) {
			if(other.get_max(i) < get_min(i) || get_max(i) < other.get_min(i)) {
				return false;
			}
		}

		return true;
	}

	bool Box::intersects(const Box &other, const Real tol) const
	{
		const Integer n = dims();
		
		assert(n == other.dims() && "must have same dimensions");
		
		for(Integer i = 0; i < n; ++i) {
			if(other.get_max(i) + tol < get_min(i) || get_max(i) + tol < other.get_min(i)) {
				return false;
			}
		}

		return true;
	}

	void Box::enlarge(const Real value)
    {  
		const Real abs_value = std::abs(value);
		const Integer n = dims();
		for(Integer i = 0; i < n; ++i) {
			min_[i] -= abs_value;
			max_[i] += abs_value;
		}
	}

	void Box::describe(std::ostream &os) const
	{
		os << "[\n";
		for(Integer i = 0; i < dims(); ++i) {
			os << "\t" << get_min(i) << ", " << get_max(i) << "\n";
		}
		os << "]\n";
	}
}

