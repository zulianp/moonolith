#ifndef MOONOLITH_INTERSECTION_2_HPP
#define MOONOLITH_INTERSECTION_2_HPP

#include <vector>

namespace moonolith {

	/**
		@brief data structure to allocate outside intersect_convex_polygons
		in the case the routine is called multiple times for saving the cost of 
	    repeated allocations.
	*/
	typedef struct {
		std::vector<short> is_inside;
		std::vector<Real> input_buffer;
		std::vector<bool> keep;
		
		/**
		 * @brief decide in advance how much space is to be reserved
		 */
		inline void reserve(const std::size_t &size)
		{
			is_inside.reserve(size);
			input_buffer.reserve(size * 2);
			keep.reserve(size);
		}

		inline void reset()
		{
			is_inside.resize(0);
			input_buffer.resize(0);
			keep.resize(0);
		}

		inline void clear()
		{
			is_inside.clear();
			input_buffer.clear();
			keep.clear();
		}
		
	} IntersectConvexPolygonsWork;

	bool intersect_convex_polygons(
		const std::vector<Real> &polygon_1, 
		const std::vector<Real> &polygon_2, 
		std::vector<Real> &result, 
		const Real tol,
		IntersectConvexPolygonsWork &work);

	inline bool intersect_convex_polygons(
		const std::vector<Real> &polygon_1, 
		const std::vector<Real> &polygon_2, 
		std::vector<Real> &result, 
		const Real tol)
	{
		IntersectConvexPolygonsWork work;
		return intersect_convex_polygons(polygon_1, polygon_2, result, tol, work);
	}

	bool clip_polygon(const std::vector<Real> &polygon,
					  const std::vector<Real> &clipping_polygon,
					  std::vector<Real> &result,
					  const Real tol,
					  IntersectConvexPolygonsWork &work);

	inline bool clip_polygon(const std::vector<Real> &polygon,
					  const std::vector<Real> &clipping_polygon,
					  std::vector<Real> &result,
					  const Real tol)
	{
		IntersectConvexPolygonsWork work;
		return clip_polygon(polygon, clipping_polygon, result, tol, work);
	}
	
	void points_to_plane_distance(const std::vector<Real> &normal, 
								  const Real d, 
								  const std::vector<Real> &points, 
							 	  std::vector<Real> &distances);

	bool polygon_intersects_plane(const std::vector<Real> &normal, 
								 const Real d,
								 const std::vector<Real> &polygon);

}

#endif //MOONOLITH_INTERSECTION_2_HPP
