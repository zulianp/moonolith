#ifndef MOONOLITH_MATRIX_IO_HPP
#define MOONOLITH_MATRIX_IO_HPP 

#include <vector>
#include <sstream>
#include <cctype>
#include <fstream>
#include <iostream>
#include <assert.h>

#include "moonolith_path.hpp"

namespace moonolith {
	class Path;

	bool read_matrix_structure(std::istream &is, Integer &rows, Integer &cols);
	
	template<typename T>
	bool read_matrix(std::istream &is, std::vector<T> &values, Integer &rows, Integer &cols)
	{
		static const std::string valid_value_separators = "\t ,;";
		static const char line_separator = '\n';
		std::string buffer, num;


		if(!read_matrix_structure(is, rows, cols)) {
			return false;
		}
		
		values.clear();
		values.reserve(rows * cols);

		while(is.good()) {
			std::getline(is, buffer, line_separator);

			std::size_t found = 0;
			std::size_t  cols_in_line = 0;
			std::size_t  next = 0;
			std::size_t  current = 0;
			T value = 0;

			//remove leading white space
			for(; found < buffer.size(); ++found) {
				if(!std::isspace(buffer[found])) {
					break;
				}
			}

			if(found == buffer.size()) continue;
			if(buffer.find_first_of(valid_value_separators) == std::string::npos) continue;

			found -= 1;

			while(cols_in_line < std::size_t(cols)) {
				current = found + 1;
				next = buffer.find_first_of(valid_value_separators, current);
				num = buffer.substr(current, next - current);

				std::istringstream numstream(num);
				numstream >> value;

				values.push_back(value);
				cols_in_line++;

				//empty line
				if(next == std::string::npos) break;

				// for(; next < buffer.size(); ++next) {
				// 	if(!isspace(buffer[next])) {
				// 		break;
				// 	}
				// }	

				found = next;			
			}
			//end while

		} 
		//end while

		return true;
	}

	template<typename T>
	bool read_matrix(const Path &path, std::vector<T> &values, Integer &rows, Integer &cols)
	{
		std::ifstream is;
		is.open(path.c_str());

		if(!is.good()) {
			is.close();
			std::cerr << "unable to open file at: "  << path << std::endl;
			return false;
		}

		bool ok = read_matrix(is, values, rows, cols);
		is.close();
		return ok;
	}

	template<typename T>
	bool write_matrix(std::ostream &os, const std::vector<T> &values, const Integer &rows, const Integer &cols)
	{
		for(Integer i = 0; i < rows; ++i) {
			for(Integer j = 0; j < cols; ++j) {
				os << values[i * cols + j];
				if(j + 1 < cols) {
					os << " ";
				}
			}

			os << "\n";
		}

		return true;
	}

	template<typename T>
	bool write_matrix(const Path &path, const std::vector<T> &values, const Integer &rows, const Integer &cols)
	{
		std::ofstream os;
		os.open(path.c_str());

		if(!os.good()) {
			os.close();
			std::cerr << "unable to open file at: "  << path << std::endl;
			return false;
		}

		bool ok = write_matrix(os, values, rows, cols);
		os.close();
		return ok;
	}
}

#endif //MOONOLITH_MATRIX_IO_HPP
