#ifndef MOONOLITH_TRI_MESH_IO_HPP
#define MOONOLITH_TRI_MESH_IO_HPP 

#include <sstream>
#include <vector>

namespace moonolith {
	class Path;
	class Mesh;

	bool read_tri_mesh(const Path &path, Mesh &mesh);
	bool write_tri_mesh(const Path &path, const Mesh &mesh);

	bool read_tri_mesh(std::istream &vert_is, std::istream &tri_is, Mesh &mesh);
	bool write_tri_mesh(std::ostream &vert_os, std::ostream &tri_os, const Mesh &mesh);
}

#endif //MOONOLITH_TRI_MESH_IO_HPP
