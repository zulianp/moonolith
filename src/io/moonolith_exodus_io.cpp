#include "moonolith_exodus_io.hpp"
#include "moonolith_path.hpp"
#include "moonolith_netcdf_io.hpp"
#include "moonolith_mesh.hpp"

#include <sstream>
#include <vector>
#include <iostream>

namespace moonolith {

	class ElemString2Enum {
	public:
		static ElemType convert(const std::string &name)
		{
			static const ElemString2Enum instance;
			auto it = instance.mapping.find(name);
			if(it == instance.mapping.end()) {
				return INVALID_ELEM;
			}

			return it->second;
		}

	private:
		ElemString2Enum() {
			mapping["EDGE2"] = ElemType::EDGE2;
			mapping["EDGE3"] = ElemType::EDGE3;
			mapping["EDGE4"] = ElemType::EDGE4;
			mapping["TRI3"]  = ElemType::TRI3;
			mapping["TRI"]  = ElemType::TRI3;

			mapping["TRI6"]  = ElemType::TRI6;
			mapping["QUAD4"] = ElemType::QUAD4;
			mapping["QUAD8"] = ElemType::QUAD8;
			mapping["QUAD9"] = ElemType::QUAD9;

			mapping["TET4"]  = ElemType::TET4;
			mapping["TET10"] = ElemType::TET10;
			mapping["HEX8"]  = ElemType::HEX8;
			mapping["HEX20"] = ElemType::HEX20;

			mapping["HEX27"]   = ElemType::HEX27;
			mapping["PRISM6"]  = ElemType::PRISM6;
			mapping["PRISM15"] = ElemType::PRISM15;
			mapping["PRISM18"] = ElemType::PRISM18;

			mapping["PYRAMID5"]  = ElemType::PYRAMID5;
			mapping["PYRAMID13"] = ElemType::PYRAMID13;
			mapping["PYRAMID14"] = ElemType::PYRAMID14;
			mapping["INFEDGE2"]  = ElemType::INFEDGE2;

			// mapping["TRISHELL3"] = ElemType::TRISHELL3;
			// mapping["QUADSHELL4"] = ElemType::QUADSHELL4;
			// mapping["SHELL3"] = ElemType::TRISHELL3;
			// mapping["SHELL4"] = ElemType::QUADSHELL4;

			mapping["TRISHELL3"] = ElemType::TRI3;
			mapping["QUADSHELL4"] = ElemType::QUAD4;
			mapping["SHELL3"] = ElemType::TRI3;
			mapping["SHELL4"] = ElemType::QUAD4;
		}

		std::map<std::string, ElemType> mapping;
	};


	static bool read_node_sets(NetCDFReader &ncdf, std::vector<Integer> &node_sets)
	{
		static const std::string node_set_index = "node_ns";
		static const std::string node_set_prop  = "ns_prop1";

		assert(!node_sets.empty());

		const Integer num_node_sets = ncdf.get_dim("num_node_sets");
		if(!num_node_sets)
			return false;

		Integer dummy1 = 0, dummy2 = 0;

		std::vector<std::vector<Integer> > nodes_to_tag;
		std::vector<Integer> tags;

		nodes_to_tag.resize(num_node_sets);
		tags.resize(num_node_sets);
		std::fill(tags.begin(), tags.end(), 0);

		for(Integer i = 0; i < num_node_sets; ++i) {
			std::string node_set_name = node_set_index + std::to_string(i+1);

			NetCDFVariable * var = ncdf.get_variable(node_set_name);
			if(!var) {
				assert(false && "reading node-set failed");
				continue;
			}

			var->read_matrix(nodes_to_tag[i], dummy1, dummy2);
		}

		NetCDFVariable * var = ncdf.get_variable(node_set_prop);
		if(var) {
			NetCDFAttribute * att = var->get_attribute("name");
			if(att->to_string() == "ID") {
				var->read_matrix(tags, dummy1, dummy2);
			} else {
				std::cerr << "[Warning]  nodesets not read" << std::endl;
				return false;
			}
		}

		for(Integer i = 0; i < num_node_sets; ++i) {
			const Integer tag = tags[i];
			auto &ns 		  = nodes_to_tag[i];
			for(auto &n : ns) { 
				node_sets[n - 1] = tag; 
			}
		}

		return true;
	}

	static bool read_side_sets(NetCDFReader &ncdf, std::vector<SideSet> &side_sets)
	{
		static const std::string side_set_index = "side_ss";
		static const std::string elem_side_set  = "elem_ss";
		static const std::string side_set_prop  = "ss_prop1";

		const Integer num_side_sets = ncdf.get_dim("num_side_sets");
		if(!num_side_sets) {
			return false;
		}
		std::vector<Integer> tags;

		side_sets.resize(num_side_sets);
		Integer dummy1 = 0, dummy2 = 0;

		for(Integer i = 0; i < num_side_sets; ++i) {
			std::string ss_i_name = side_set_index + std::to_string(i + 1);
			std::string e_ss_name = elem_side_set  + std::to_string(i + 1);
			NetCDFVariable * var = ncdf.get_variable(ss_i_name);

			if(!var) {
				std::cerr << "[Warning] sidesets not read" << std::endl;
				return false;
			}

			var->read_matrix(side_sets[i].side_index, dummy1, dummy2);

			var = ncdf.get_variable(e_ss_name);

			if(!var) {
				assert(false);
			}

			var->read_matrix(side_sets[i].element, dummy1, dummy2);
		}

		NetCDFVariable * var = ncdf.get_variable(side_set_prop);




		if(var) {
			NetCDFAttribute * att = var->get_attribute("name");
			if(att->to_string() == "ID") {
				var->read_matrix(tags, dummy1, dummy2);
			} else {
				std::cerr << "[Warning] failed to read sidests " << std::endl;
				return false;
			}
		}

		for(Integer i = 0; i < num_side_sets; ++i) {
			side_sets[i].tag = tags[i];
		}

		return true;
	}

	static bool read_connectivity(
		NetCDFReader &ncdf, 
		// const Integer &dim,
		std::vector<Integer> &el_ptr, 
		std::vector<Integer> &el_index,
		std::vector<Integer> &elem_type,
		std::vector<Integer> &blocks)
	{
		static const std::string el = "elem_map", con = "connect";
		const Integer num_el_blk = ncdf.get_dim("num_el_blk");
		// const Integer num_dim    = ncdf.get_dim("num_dim");

		if(!num_el_blk) {
			return false;
		}

		std::vector<NetCDFVariable *> vars;
		ncdf.get_variables_with_prefix(con, vars);

		Integer n_elements = 0;
		Integer n_local_nodes = 0;
		std::vector<int> connectivity;

		el_ptr.resize(1);
		el_ptr[0] = 0;

		Integer current_element = 0;


		for(std::size_t v = 0; v < vars.size(); ++v) {
			NetCDFVariable * var = vars[v];
			std::string postfix = var->name().substr(con.size(), var->name().size()-con.size());

			Integer elem_type_i = INVALID_ELEM;

			NetCDFAttribute * att = var->get_attribute("elem_type");
			if(att) {
				elem_type_i = ElemString2Enum::convert( att->to_string() );
			} else {
				std::cout << "elem_type not found!" << std::endl;
			}

			Integer block_id = 0;
			std::istringstream ss(postfix);
			ss >> block_id;

			// const Integer num_nod_per_el = ncdf.get_dim("num_nod_per_el" + postfix);

			if(!var) {
				std::cerr << "ExodusMeshReader: could not read variable " << var->name() << std::endl;
				return false;
			}

			std::size_t index_offset = el_index.size();

			var->read_matrix(connectivity, n_elements, n_local_nodes);
			el_index.resize(el_index.size() + n_elements * n_local_nodes);
			el_ptr.resize(el_ptr.size() + n_elements);
			blocks.resize(el_ptr.size() - 1);
			elem_type.resize(blocks.size());

			//remove +1 offset 
			for(auto &c : connectivity) {
				c -= 1;
			}

			el_index.insert(el_index.begin() + index_offset, connectivity.begin(), connectivity.end());
			for(Integer i = 0; i < n_elements; ++i, ++current_element) {
				el_ptr[current_element + 1] += el_ptr[current_element] + n_local_nodes;
				blocks[current_element] = block_id;
				elem_type[current_element] = elem_type_i;
			}
		}

		return true;
	}


	static bool read_coordinate(NetCDFReader &ncdf, const std::string &name, std::vector<Real> &coord, Integer &n_points)
	{
		NetCDFVariable * var = ncdf.get_variable(name);

		if(!var) {
			return false;
		}

		Integer one;
		return var->read_matrix(coord, n_points, one);
	}

	static bool read_points(NetCDFReader &ncdf, Integer &dim, std::vector<Real> &points)
	{
		static_assert( sizeof(Real) == sizeof(double), "Real must be double precision");

		static const std::string coord = "coord";
		NetCDFVariable * var = ncdf.get_variable(coord);

		// points are represented as x-y-z
		Integer n_points;
		if(var) {
			std::vector<Real> buffer;
			const bool ok = var->read_matrix(buffer, dim, n_points);
			
			//transpose data-layout from column-major to row major
			points.resize(buffer.size());
			for(Integer i = 0; i < n_points; ++i) {
				for(Integer j = 0; j < dim; ++j) {
					points[i*dim + j] = buffer[i + n_points * j];
				}
			}

			return ok;
		}

		std::vector<Real> x, y, z;
		if(!read_coordinate(ncdf, "coordx", x, n_points)) {
			return false;
		}

		if(!read_coordinate(ncdf, "coordy", y, n_points)) {
			return false;
		}

		if(!read_coordinate(ncdf, "coordz", z, n_points)) { 
			dim = 2;
		} else {
			dim = 3;
		}

		points.resize(n_points * dim);

		for(std::size_t i = 0; i < x.size(); ++i) {
			const std::size_t i_x_dim = i * dim;

			points[i_x_dim] 	= x[i];
			points[i_x_dim + 1] = y[i];

			if(dim > 2) {
				points[i_x_dim + 2] = z[i];
			}
		}

		return true;
	}

	static bool read_exodus_mesh(
		const Path &path, 
		Integer &dim,
		std::vector<Integer> &el_ptr, 
		std::vector<Integer> &el_index,
		std::vector<Real> &points,
		std::vector<Integer> &elem_type,
		std::vector<Integer> &blocks,
		std::vector<SideSet> &side_sets,
		std::vector<Integer> &node_sets
		)
	{
		NetCDFReader ncdf;
		if(!ncdf.open(path)) {
			return false;
		}

		if(!read_points(ncdf, dim, points)) {
			ncdf.close();
			return false;
		}

		if(!read_connectivity(ncdf, el_ptr, el_index, elem_type, blocks)) {
			ncdf.close();
			return false;
		}

		node_sets.resize(points.size()/dim);
		read_node_sets(ncdf, node_sets);
		read_side_sets(ncdf, side_sets);
		return ncdf.close();
	}

	bool read_exodus_mesh(const Path &path, Mesh &mesh)
	{
		if(!read_exodus_mesh(
			path, 
			mesh.dim, 
			mesh.el_ptr, 
			mesh.el_index, 
			mesh.points, 
			mesh.elem_type, 
			mesh.blocks,
			mesh.side_sets, 
			mesh.node_sets)) {
			return false;
		}

		mesh.set_uniform_elem_type_flags();
		return true;
	}
}
