#include "moonolith_mesh_io.hpp"
#include "moonolith_mesh.hpp"

#include "moonolith_tri_io.hpp"
#include "moonolith_obj_io.hpp"
#include "moonolith_off_io.hpp"
#include "moonolith_ele_io.hpp"
#include "moonolith_path.hpp"
#include "moonolith_exodus_io.hpp"

#include <map>
#include <string>
#include <functional>

namespace moonolith {

	class MeshReader {
	public:
		typedef bool(*F)(const Path &, Mesh &);

		bool read(const Path &path, Mesh &mesh) const
		{
			auto r_it = readers_.find(path.extension());
			if(r_it == readers_.end()) return false;
			return r_it->second(path, mesh);
		}

		MeshReader()
		{
			readers_["tri"]  = static_cast<F>(&read_tri_mesh);
			readers_["obj"]  = static_cast<F>(&read_obj_mesh);
			readers_["ele"]  = static_cast<F>(&read_ele_mesh);
			readers_["node"] = static_cast<F>(&read_ele_mesh);
			readers_["e"]    = static_cast<F>(&read_exodus_mesh);
		}

	private:

		std::map<std::string, std::function<bool(const Path &, Mesh &)> > readers_;
	};


	class MeshWriter {
	public:
		typedef bool(*F)(const Path &, const Mesh &);


			bool write(const Path &path, const Mesh &mesh) const
			{
				auto r_it = writers_.find(path.extension());
				if(r_it == writers_.end()) return false;
				return r_it->second(path, mesh);
			}

			MeshWriter()
			{
				writers_["tri"]  = static_cast<F>(&write_tri_mesh);
				writers_["obj"]  = static_cast<F>(&write_obj_mesh);
				writers_["ele"]  = static_cast<F>(&write_ele_mesh);
				writers_["node"] = static_cast<F>(&write_ele_mesh);
				// writers_["e"]    = static_cast<F>(&write_exodus_mesh);
			}

		private:
			std::map<std::string, std::function<bool(const Path &, const Mesh &)> > writers_;
	};

	bool read_mesh(const Path &path, Mesh &mesh)
	{
		static const MeshReader reader;
		if(!reader.read(path, mesh)) {
			std::cerr << "[Warning] " << path.extension() << " not supported" << std::endl;
			return false;
		}

		return true;
	}

	bool write_mesh(const Path &path, const Mesh &mesh)
	{
		static const MeshWriter writer;
		if(!writer.write(path, mesh)) {
			std::cerr << "[Warning] " << path.extension() << " not supported" << std::endl;
			return false;
		}

		return true;
	}

}