
#ifndef MOONOLITH_EXODUS_IO_H
#define MOONOLITH_EXODUS_IO_H

namespace moonolith {
	class Path;
	class Mesh;
	bool read_exodus_mesh(const Path &path, Mesh &mesh);
}

#endif //MOONOLITH_EXODUS_IO_H
