#include "moonolith_obj_io.hpp"
#include "moonolith_string.hpp"

#include <cassert>
#include <iostream>
#include <map>
#include <sstream>
#include <fstream>

namespace moonolith {

	class ObjLineParser
	{
	public:
		ObjLineParser(Mesh &mesh)	
		: mesh_(mesh)
		{
			parsers_["v"] = &ObjLineParser::process_vertex;
			parsers_["vn"] = &ObjLineParser::process_normal;
			parsers_["vt"] = &ObjLineParser::process_texture;
			
			parsers_["f"] = &ObjLineParser::process_face;
		}

		bool parse(const int line_number, const std::vector<std::string> &data)
		{
			const std::string &element = data[0];

			Parser parser = parsers_[element];
			if (parser == NULL)
			{
				std::cerr<<"Unsupported element "<< element<<std::endl;
				return true;
			}

			if(!(this->*parser)(data)) {
				std::cerr << "at line " << line_number << std::endl;
				return false;
			}

			return true;
		}
	private:
		typedef bool (ObjLineParser::*Parser)(const std::vector<std::string> &data); 
		std::map<std::string, Parser> parsers_;

		Mesh &mesh_;
		
		bool process_vertex(const std::vector<std::string> &data)
		{
			assert(data.size() >= std::size_t(4));

			if (data.size() < 4)
				return false;

			mesh_.add_node(atof(data[1].c_str()),atof(data[2].c_str()),atof(data[3].c_str()));

			return true;
		}


		bool process_normal(const std::vector<std::string> &data)
		{
			assert(data.size() >= std::size_t(4));

			if (data.size() < 4)
				return false;

			mesh_.add_normal(atof(data[1].c_str()), atof(data[2].c_str()), atof(data[3].c_str()));
			
			return true;
		}



		bool process_texture(const std::vector<std::string> &data)
		{
			assert(data.size() >= std::size_t(3));

			if (data.size() < 3)
				return false;

			mesh_.add_texture(atof(data[1].c_str()), atof(data[2].c_str()));

			if(data.size() > 3)
				std::cout << "[Warning] obj reader, ignoring w coordinate" << std::endl;

			return true;
		}


		bool process_face(const std::vector<std::string> &data)
		{
			assert(data.size() >= std::size_t(4));

			std::vector<Integer> vertices(data.size()-1);
			std::vector<Integer> textures(data.size()-1);
			std::vector<Integer> normals(data.size()-1);

			std::vector<std::string> components;

			for (std::size_t i = 1; i < data.size(); ++i) {
				split(data[i],'/',components);

				vertices[i-1] = -1;
				textures[i-1] = -1;
				normals[i-1]  = -1;

				Integer vertexIndex = atoi(components[0].c_str()) - 1;
				if(vertexIndex < 0) {
					vertexIndex = mesh_.n_nodes() + vertexIndex + 1;
				}

				assert(vertexIndex >= 0);
				if(vertexIndex < 0) return false;

				vertices[i-1] = vertexIndex;

				if(components.size() > 1) {
					if(!components[1].empty()) {
						Integer texture_index = atoi(components[1].c_str()) - 1;
						if(texture_index < 0) {
							texture_index = mesh_.n_textures() + texture_index + 1;
						}
						assert(texture_index >= 0);
						if(texture_index < 0) return false;

						textures[i-1] = texture_index;
					}

					if(components.size() > 2) {
						Integer normal_index = atoi(components[2].c_str()) - 1;
						if(normal_index < 0) {
							normal_index = mesh_.n_normals() + normal_index + 1;
						}

						assert(normal_index >=0);
						if(normal_index < 0) return false;
						normals[i-1] = normal_index;
					}
				}   
			}

			if(normals[0]==-1) normals.clear();
			if(textures[0]==-1) textures.clear();


			ElemType type = ElemType::POLYGON;
			switch(vertices.size()) {
				case 3: type = ElemType::TRI3; break;
				case 4: type = ElemType::QUAD4; break;
				default: break;
			}

			mesh_.add_element(vertices, normals, textures, type);
			return true;
		}
	};









	bool write_obj_mesh(const Path &path, const Mesh &mesh)
	{
		assert(mesh.valid());

		std::ofstream os;
		os.open(path.c_str());
		if(!os.good()) {
			os.close();
			std::cerr << "Stream not good for .obj" << std::endl;
			return false;
		}


		const bool ok = write_obj_mesh(os,mesh);

		os.close();

		return ok;
	}

	bool write_obj_mesh(std::ostream &os, const Mesh &mesh)
	{
		assert(mesh.valid());

		const Integer n_points = mesh.n_nodes();

		for(Integer i=0; i < n_points; ++i)
		{
			os<<"v";
			for(Integer j = 0; j < mesh.dim; ++j)
				os<<" "<<mesh.points[i*mesh.dim+j];

			for(Integer j = mesh.dim; j < 3; ++j) //obj meshes are valid only in 3D
				os<<"  0";

			os<<"\n";
		}

		os<<"\n\n";

		for(Integer i=0; i < mesh.n_normals(); ++i)
		{
			os<<"vn";
			for(Integer j=0; j < 3; ++j)
				os<<" "<<mesh.normals[i*3+j];
			os<<"\n";
		}

		os<<"\n\n";

		for(Integer i=0; i < mesh.n_textures(); ++i)
		{
			os<<"vt";
			for(Integer j=0; j < 2; ++j)
				os<<" "<<mesh.textures[i*2+j];
			os<<"\n";
		}

		os<<"\n\n";


		const Integer n_faces = mesh.n_elements();
		const bool has_normals = !mesh.normal_index.empty();
		const bool has_textures = !mesh.texture_index.empty();

		for(Integer i = 0; i < n_faces; ++i)
		{
			os<<"f";
			for(Integer j=mesh.el_ptr[i]; j < mesh.el_ptr[i+1]; ++j)
			{
				const Integer p_index=mesh.el_index[j];
				os<<" "<<p_index+1;

				if(has_textures)
				{
					const Integer t_index=mesh.texture_index[j];

					os<<"/"<<t_index+1;
					if(has_normals)
					{
						const Integer n_index=mesh.normal_index[j];
						os<<"/"<<n_index+1;
					}
				}
				else if(has_normals)
				{
					const Integer n_index=mesh.normal_index[j];
					os<<"//"<<n_index+1;
				}
			}

			os<<"\n";
		}

		return true;
	}



	bool read_obj_mesh(const Path &path, Mesh &mesh)
	{
		std::ifstream is;
		is.open(path.c_str());

		if(!is.good()) {
			is.close();
			std::cerr << "unable to open file at: "  << path << std::endl;
			return false;
		}

		bool ok = read_obj_mesh(is,mesh);

		is.close();

		return ok;
	}

	bool read_obj_mesh(std::istream &os, Mesh &mesh)
	{
		std::vector<std::string> components;
		std::string line;


		int line_number = 0;

		ObjLineParser parser(mesh);

		bool success = true;

		while (os.good())
		{
			++line_number;
			std::getline (os, line);
			line = trim(line);
			if (line.length() == 0 || line[0] == '#')
				continue;

			split(line, ' ', components);

			success = success && parser.parse(line_number, components);
		}

		mesh.set_uniform_elem_type_flags();

		return success;
	}
}

