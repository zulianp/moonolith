#ifndef MOONLITH_OBJ_IO_HPP
#define MOONLITH_OBJ_IO_HPP

#include "moonolith_path.hpp"
#include "moonolith_mesh.hpp"

#include <vector>

namespace moonolith {
	

	bool read_obj_mesh(std::istream &os, Mesh &mesh);
	bool read_obj_mesh(const Path &path, Mesh &mesh);

	bool write_obj_mesh(std::ostream &os, const Mesh &mesh);
	bool write_obj_mesh(const Path &path, const Mesh &mesh);
}


#endif // MOONLITH_OBJ_IO_HPP
