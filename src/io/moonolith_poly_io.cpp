#include "moonolith_poly_io.hpp"

#include <fstream>
#include <string>
#include <iostream>

namespace moonolith {

	bool read_poly(std::istream &is, std::vector<Real> &polygon, const bool skip_z)
	{
		std::string line;
		std::getline(is,line);
		if (line != "POLY"){
			std::cerr<<"invalid file"<<std::endl;
			return false;
		}

		Integer n_vertices;
		is >> n_vertices;
		const Integer n_dims = (skip_z ? 2 : 3);

		polygon.resize(n_vertices * n_dims) ;

		int dummy1;
		double x, y, z;

		for (int i = 0; i < n_vertices; ++i)
		{
			is >> dummy1 >> x >> y >> z;
			
			polygon[n_dims * i] = x;
			polygon[n_dims * i + 1] = y;

			if(!skip_z) {
				polygon[n_dims * i + 2] = z;
			}
		}

		return true;
	}


	bool read_poly(const Path &path, std::vector<Real> &polygon, const bool skip_z)
	{
		std::ifstream is;
		is.open(path.c_str());

		if(!is.good()) {
			is.close();
			std::cerr << "unable to open file at: "  << path << std::endl;
			return false;
		}

		bool ok = read_poly(is, polygon, skip_z);

		is.close();

		return ok;
	}

	bool write_poly(std::ostream &os, std::vector<Real> &polygon, const bool is_2d)
	{

		const Integer n_dims = (is_2d? 2 : 3);

		os << "POLY\n";
		os << polygon.size()/n_dims << "\n";

		for(std::size_t i = 0; i < polygon.size(); i += n_dims)
		{
			os << (i / n_dims + 1) << " " << polygon[i] << " " << polygon[i + 1];
			
			if(is_2d) {
			 	os << " 0\n";
			} else {
				os << " " << polygon[i + 2] << "\n";
			}
		}

		return true;
	}

	bool write_poly(const Path &path, std::vector<Real> &polygon, const bool is_2d)
	{
		std::ofstream os;
		os.open(path.c_str());
		if(!os.good()) {
			os.close();
			std::cerr << "Stream not good for .obj" << std::endl;
			return false;
		}


		const bool ok = write_poly(os, polygon, is_2d);

		os.close();

		return ok;
	}
	
}

