#ifndef MOONOLITH_POLY_IO
#define MOONOLITH_POLY_IO

#include "moonolith_path.hpp"

#include <vector>

namespace moonolith {
	bool read_poly(std::istream &is, std::vector<Real> &polygon, const bool skip_z = true);
	bool read_poly(const Path &path, std::vector<Real> &polygon, const bool skip_z = true);

	bool write_poly(std::ostream &os, std::vector<Real> &polygon, const bool is_2d = true);
	bool write_poly(const Path &path, std::vector<Real> &polygon, const bool is_2d = true);
}

#endif //MOONOLITH_POLY_IO
