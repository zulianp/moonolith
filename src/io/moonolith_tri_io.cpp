#include "moonolith_tri_io.hpp"
#include "moonolith_matrix_io.hpp"
#include "moonolith_path.hpp"
#include "moonolith_mesh.hpp"

namespace moonolith {
	static void offset_triangulation(const std::vector<Integer> &triangulation, std::vector<Integer> &out)
	{
		out.resize(triangulation.size());

		for(std::size_t i = 0; i < triangulation.size(); ++i) 
			out[i]=triangulation[i]+1;
	}
	
	static bool finalize_tri_mesh(Mesh &mesh)
	{
		mesh.elem_type.resize(mesh.el_index.size()/3);
		std::fill(mesh.elem_type.begin(), mesh.elem_type.end(), ElemType::TRI3);
		mesh.uniform_elem_type = ElemType::TRI3;
		mesh.has_uniform_elem_type = true;

		mesh.el_ptr.resize(mesh.elem_type.size() + 1);

		mesh.el_ptr[0] = 0;
		for(std::size_t i = 1; i < mesh.el_ptr.size(); ++i) {
			mesh.el_ptr[i] = mesh.el_ptr[i - 1] + 3;
		}

		for(std::size_t i = 0; i < mesh.el_index.size(); ++i) {
			--mesh.el_index[i];
		}

		return true;
	}
	
	static bool read_tri_mesh(
		const Path &vert_path, 
		const Path &tri_path,
		Integer &dim, 
		std::vector<Real> &points, 
		std::vector<Integer> &triangulation)
	{

		Integer n_points = 0;
		Integer s_dim = 0;
		if(!read_matrix(vert_path, points, n_points, s_dim)) {
			return false;
		}

		dim = s_dim;

		Integer n_triangles = 0;
		return read_matrix(tri_path, triangulation, n_triangles, s_dim);
	}

	static bool read_tri_mesh(
		std::istream &vert_stream, 
		std::istream &tri_stream,
		Integer &dim, 
		std::vector<Real> &points, 
		std::vector<Integer> &triangulation)
	{
		Integer n_points = 0;
		Integer s_dim = 0;
		if(!read_matrix(vert_stream, points, n_points, s_dim)) {
			return false;
		}

		dim = s_dim;

		Integer n_triangles = 0;
		return read_matrix(tri_stream, triangulation, n_triangles, s_dim);
	}

	bool read_tri_mesh(std::istream &vert_is, std::istream &tri_is, Mesh &mesh)
	{
		if(!read_tri_mesh(vert_is, tri_is, mesh.dim, mesh.points, mesh.el_index)) {
			return false;
		}

		return finalize_tri_mesh(mesh);
	}

	bool read_tri_mesh(const Path &path, Mesh &mesh)
	{
		const Path no_ext = path.without_extension();
		const Path tri_path = no_ext + ".tri";
		const Path vert_path = no_ext + ".vert";

		if(!read_tri_mesh(vert_path, tri_path, mesh.dim, mesh.points, mesh.el_index)) {
			return false;
		}

		return finalize_tri_mesh(mesh);
	}

	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////

	static bool write_tri_mesh(
		const Path &vert_path, 
		const Path &tri_path,
		const Integer dim, 
		const std::vector<Real> &points, 
		const std::vector<Integer> &triangulation)
	{
		std::vector<Integer> tmp;
		offset_triangulation(triangulation, tmp);

		return write_matrix(vert_path, points, points.size()/dim, dim) && 
		write_matrix(tri_path, tmp, tmp.size()/3, 3);
	}

	static bool write_tri_mesh(
		std::ostream &vert_stream, 
		std::ostream &tri_stream,
		const Integer dim, 
		const std::vector<Real> &points, 
		const std::vector<Integer> &triangulation)
	{
		std::vector<Integer> tmp;
		offset_triangulation(triangulation, tmp);

		return write_matrix(vert_stream, points, points.size()/dim, dim) && 
		write_matrix(tri_stream, tmp, tmp.size()/3, 3);
	}

	bool write_tri_mesh(const Path &path, const Mesh &mesh)
	{
		const Path no_ext = path.without_extension();
		const Path tri_path = no_ext + ".tri";
		const Path vert_path = no_ext + ".vert";

		return write_tri_mesh(
			vert_path, 
			tri_path,
			mesh.dim, 
			mesh.points, 
			mesh.el_index);
	}

	bool write_tri_mesh(
		std::ostream &vert_os, 
		std::ostream &tri_os, 
		const Mesh &mesh)
	{
		return write_tri_mesh(
			vert_os, 
			tri_os,
			mesh.dim, 
			mesh.points, 
			mesh.el_index);
	}
}
