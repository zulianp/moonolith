#ifndef MOONOLITH_MESH_IO_HPP
#define MOONOLITH_MESH_IO_HPP 



namespace moonolith {
	class Path;
	class Mesh;
	
	bool read_mesh(const Path &path, Mesh &mesh);
	bool write_mesh(const Path &path, const Mesh &mesh);
}

#endif //MOONOLITH_MESH_IO_HPP
