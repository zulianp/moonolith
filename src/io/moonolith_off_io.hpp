#ifndef MOONOLITH_MESH_IO
#define MOONOLITH_MESH_IO

#include "moonolith_path.hpp"
#include "moonolith_mesh.hpp"


namespace moonolith {
	bool read_off_mesh(std::istream &is, Mesh &mesh);
	bool read_off_mesh(const Path &path, Mesh &mesh);

	bool write_off_mesh(std::ostream &os, Mesh &mesh);
	bool write_off_mesh(const Path &path, Mesh &mesh);
}

#endif //MOONOLITH_MESH_IO
