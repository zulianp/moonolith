#ifndef MOONOLITH_ELE_IO_HPP
#define MOONOLITH_ELE_IO_HPP 

#include <istream>
#include <ostream>

namespace moonolith {
	class Path;
	class Mesh;

	/**    @defgroup io IO
	 *     @brief Mesh readers and writers
	 */


	/** \addtogroup io
	 *  @{
	 */


	/** 
	 * Reads the format used by tetgen (.node, .ele)
	 * from a stream. The .node data has to come before the .ele data 
	 */
	bool read_ele_mesh(std::istream &is, Mesh &mesh);

	/** 
	 *  Reads meshes with the format used by tetgen (.node, .ele)
	 */
	bool read_ele_mesh(const Path &path, Mesh &mesh);

	/** 
	 *  Writes meshes to the format used by tetgen (.node, .ele)
	 * to a stream. The .node data is written before the .ele data 
	 */
	bool write_ele_mesh(std::ostream &os, const Mesh &mesh);
	
	/** 
	 *  Writes meshes to the format used by tetgen (.node, .ele)
	 */
	bool write_ele_mesh(const Path &path, const Mesh &mesh);

	  /** @}*/
}

#endif //MOONOLITH_ELE_IO_HPP
