#include "moonolith_netcdf_io.hpp"
#include "moonolith_path.hpp"

#ifdef WITH_NETCDF_4

#include <netcdf>
#include <ncType.h>



namespace moonolith {

	const std::string NetCDFAttribute::to_string() const
	{
		std::string ret;
		att_.get_values(ret);
		return ret;
	}


	NetCDFAttribute::NetCDFAttribute()
	{}

	NetCDFAttribute::~NetCDFAttribute() 
	{}

	void NetCDFAttribute::initialize(NcAtt &att)
	{
		using netCDF::NcType;

		att_ = att;
		switch(att.getType().getTypeClass()) {
		case NcType::nc_DOUBLE:
			type_ = "double";
			break;
		case NcType::nc_FLOAT:
			type_ = "float";
			break;
		case NcType::nc_INT64: //Why didn't they put LONG too?
			type_ = "long"; 
			break;
		case NcType::nc_CHAR:
			type_ = "char";
			break;
		default:
			std::cerr << "NetCDFAttribute: Warning unsupported type" << std::endl;
			std::cout << "IMPLEMENT ME" << std::endl;
			break;
		}
		name_ = att.getName();
	}

	void NetCDFVariable::get_values(double *values)
	{
		try {
			var_.getVar(values);
		} catch(const netCDF::exceptions::NcException &ex) {
			std::cerr << ex.what() << std::endl;
			assert(false);
		}
	}

	void NetCDFVariable::get_values(long *values)
	{
		try {
			var_.getVar(values);
		} catch(const netCDF::exceptions::NcException &ex) {
			std::cerr << ex.what() << std::endl;
			std::cerr << "Var is:\n" << *this << std::endl;
			assert(false);
		}
	}

	void NetCDFVariable::get_values(int *values)
	{
		try {
			var_.getVar(values);
		} catch(const netCDF::exceptions::NcException &ex) {
			std::cerr << ex.what() << std::endl;
			std::cerr << "Var is:\n" << *this << std::endl;
			assert(false);
		}
	}

	void NetCDFVariable::get_values(char *values)
	{
		try {
			var_.getVar(values);
		} catch(const netCDF::exceptions::NcException &ex) {
			std::cerr << ex.what() << std::endl;
			std::cerr << "Var is:\n" << *this << std::endl;
			assert(false);
		}
	}

	void NetCDFVariable::get_values(float *values)
	{
		try {
			var_.getVar(values);
		} catch(const netCDF::exceptions::NcException &ex) {
			std::cerr << ex.what() << std::endl;
			std::cerr << "Var is:\n" << *this << std::endl;
			assert(false);
		}
	}

	void NetCDFVariable::initialize(NcVar &var)
	{
		using netCDF::NcVarAtt;
		using netCDF::NcType;

		var_ = var;
		name_ = var.getName();
		dims_.resize(var.getDimCount());

		for(Integer i = 0; i < dims_.size(); ++i) {
			dims_[i] = var.getDim(i).getSize();
		}

		std::map<std::string, NcVarAtt> atts = var.getAtts();

		for(std::map< std::string, NcVarAtt >::iterator it = atts.begin(); it != atts.end(); ++it) {
			attributes_[it->first].initialize(it->second);
		}
	}


	NetCDFVariable::NetCDFVariable() : var_() {}
	NetCDFVariable::NetCDFVariable(NcVar &var) { initialize(var); }

	NetCDFVariable::~NetCDFVariable()
	{}



	bool NetCDFReader::open(const Path &path)
	{
		close();
		file_ = new NcFile(path.c_str(), NcFile::read);
		if(file_->isNull()) {
			std::cerr << "Could not open netcdf file at " << path << std::endl;
			return false;
		}


		init();
		return true;
	}


	NetCDFReader::NetCDFReader()
		: file_(NULL)
	{}


	bool NetCDFReader::close()
	{
		if (!file_) {
			return true;
		}

		variables_.clear();
		dimensions_.clear();


		delete file_;
		file_ = NULL;
	
		return true;
	}

	void NetCDFReader::init()
	{
		using netCDF::NcDim;

		const Integer n_dims = file_->getDimCount();
		const Integer n_vars = file_->getVarCount();

		std::multimap< std::string, NcDim > dims = file_->getDims();

		std::multimap< std::string, NcDim >::iterator dimIt;
		for(dimIt = dims.begin(); dimIt != dims.end(); ++dimIt) {
			dimensions_[dimIt->first] = dimIt->second.getSize();
		}

		std::multimap< std::string, NcVar > vars = file_->getVars();
		std::multimap< std::string, NcVar >::iterator varIt;

		for(varIt = vars.begin(); varIt != vars.end(); ++varIt) {
			variables_[String::Trim(varIt->first)] = NetCDFVariable(varIt->second);
		}
	}
}


#else //WITH_NETCDF_4
//Legacy C++ version 

#include <netcdfcpp.h>

namespace moonolith {

	const std::string NetCDFAttribute::to_string() const
	{
		return att_->as_string(0);
	}

	NetCDFAttribute::~NetCDFAttribute() {}
		NetCDFAttribute::NetCDFAttribute()
		: att_(NULL)
		{}

	void NetCDFAttribute::initialize(NcAtt &att)
	{
		att_ = &att;
		switch(att.type()) {
		case ncDouble:
			type_ = "double";
			break;
		case ncFloat:
			type_ = "float";
			break;
		case ncLong:
			type_ = "long";
			break;
		case ncChar:
			type_ = "char";
			break;
		default:
			std::cerr << "NetCDFAttribute: Warning unsupported type" << std::endl;
			std::cout << "IMPLEMENT ME" << std::endl;
			break;
		}
		name_ = att.name();
	}

	void NetCDFVariable::get_values(double *values)
	{
		long * edges = var_->edges();
		var_->get(values, edges);
		delete[] edges;
	}
	
	void NetCDFVariable::get_values(long *values)
	{
		long * edges = var_->edges();
		var_->get(values, edges);
		delete[] edges;
	}
	
	void NetCDFVariable::get_values(int *values)
	{
		long * edges = var_->edges();
		var_->get(values, edges);
		delete[] edges;
	}
	
	void NetCDFVariable::get_values(char *values)
	{
		long * edges = var_->edges();
		var_->get(values, edges);
		delete[] edges;
	}
	
	void NetCDFVariable::get_values(float *values)
	{
		long * edges = var_->edges();
		var_->get(values, edges);
		delete[] edges;
	}
	
	bool NetCDFVariable::write_matrix(const float *result, const Integer &rows, const Integer &cols)
	{
		dims_.resize(2);
		dims_[0] = rows;
		dims_[1] = cols;
		return var_->put(result, rows, cols, 1, 1);
	}
	
	bool NetCDFVariable::write_matrix(const int *result, const Integer &rows, const Integer &cols)
	{
		dims_.resize(2);
		dims_[0] = rows;
		dims_[1] = cols;
		return var_->put(result, rows, cols, 1, 1);
	}
	
	bool NetCDFVariable::write_matrix(const long *result, const Integer &rows, const Integer &cols)
	{
		dims_.resize(2);
		dims_[0] = rows;
		dims_[1] = cols;
		return var_->put(result, rows, cols, 1, 1);
	}
	
	bool NetCDFVariable::write_matrix(const double *result, const Integer &rows, const Integer &cols)
	{
		dims_.resize(2);
		dims_[0] = rows;
		dims_[1] = cols;
		return var_->put(result, rows, cols, 1, 1);
	}

	void NetCDFVariable::initialize(NcVar &var)
	{
		var_ = &var;
		long * edges = var.edges();
		name_ = var.name();
		dims_.resize(var.num_dims());
		std::copy(edges, edges + var.num_dims(), dims_.begin());

		for(int i = 0; i < var.num_atts(); ++i) {
			NcAtt * att = var.get_att(i);
			attributes_[att->name()].initialize(*att);
		}

		delete[] edges;
	}

	NetCDFVariable::~NetCDFVariable() {}

	NetCDFVariable::NetCDFVariable() : var_(NULL) {}
	NetCDFVariable::NetCDFVariable(NcVar &var) : var_(&var) { initialize(var); }

	NetCDFReader::NetCDFReader()
		: file_(NULL)
	{}


	bool NetCDFReader::open(const Path &path)
	{
		close();
		file_ = new NcFile(path.c_str(), NcFile::ReadOnly);
		if(!file_->is_valid()) {
			std::cerr << "Could not open netcdf file at " << path << std::endl;
			return false;
		}


		init();
		return true;
	}

	bool NetCDFReader::close()
	{
		if (!file_) {
			return true;
		}

		variables_.clear();
		dimensions_.clear();

		bool good = file_->close();
		delete file_;
		file_ = NULL;

		return good;
	}

	void NetCDFReader::init()
	{
		const Integer n_dims = file_->num_dims();
		const Integer n_vars = file_->num_vars();

		for (Integer i = 0; i < n_dims; ++i) {
			dimensions_[file_->get_dim(i)->name()] = file_->get_dim(i)->size();
		}

		for(Integer i = 0; i < n_vars; ++i) {
			NcVar * ncVar = file_->get_var(i);
			variables_[trim(ncVar->name())] = NetCDFVariable(*ncVar);
		}
	}
}

#endif //WITH_NETCDF_4
