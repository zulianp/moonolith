
#ifndef MOONOLITH_NETCDF_READER_H
#define MOONOLITH_NETCDF_READER_H

#include <map>
#include <set>
#include <vector>
#include <string>
#include <ostream>
#include <iostream>
#include <assert.h>

#include "moonolith_string.hpp"

#define MOONOLITH_WITH_NETCDF


#ifdef WITH_NETCDF_4

#include <ncVarAtt.h>
#include <ncVar.h>




namespace netCDF {
	//forward declarations
	class NcFile;
}

typedef netCDF::NcVarAtt NcAtt;
typedef netCDF::NcVar NcVar;
typedef netCDF::NcFile NcFile;

#else
//forward declarations
class NcAtt;
class NcVar;
class NcFile;

#endif //WITH_NETCDF_4


namespace moonolith {
	class Path;
	
	class NetCDFAttribute {
	private:
		
#ifdef WITH_NETCDF_4
		NcAtt att_;
#else
		NcAtt * att_;
#endif
		
		std::string name_;
		std::string type_;
	public:
		NetCDFAttribute();
		~NetCDFAttribute();
		NetCDFAttribute(NcAtt &att) { initialize(att); }
		void initialize(NcAtt &att);
		
		friend std::ostream & operator<<(std::ostream &os, const NetCDFAttribute &att)
		{
			os << "\tATTRIBUTE: " << att.type_ << " " << att.name_ << "\n";
			return os;
		}
		
		const std::string to_string() const;
		
	};
	
	class NetCDFVariable {
	private:
		
		
#ifdef WITH_NETCDF_4
		NcVar var_;
#else
		NcVar * var_;
#endif
		std::string name_;
		std::map<std::string, NetCDFAttribute> attributes_;
		std::vector<Integer> dims_;
		
		void get_values(double *values);
		void get_values(long *values);
		void get_values(int *values);
		void get_values(char *values);
		void get_values(float *values);
		
		void set_values(double *values);
		
	public:
		~NetCDFVariable();
		NetCDFVariable();
		NetCDFVariable(NcVar &var);
		
		
		void initialize(NcVar &var);
		
		friend std::ostream &operator<<(std::ostream &os, const NetCDFVariable &var)
		{
			os << "VARIABLE: " << var.name_ << "\n";
			os << "DIMS: ";
			
			for(auto d : var.dims_) {
				os << d << " ";
			}
			
			os << "\n";
			
			for(std::map<std::string, NetCDFAttribute>::const_iterator it = var.attributes_.begin(); it != var.attributes_.end(); ++it) {
				os << it->second;
			}
			
			return os;
		}
		
		std::vector<Integer> &dims() { return dims_; }
		
		inline const std::string &name() const
		{
			return name_;
		}
		
		bool write_matrix(const double *result, const Integer &rows, const Integer &cols);
		bool write_matrix(const float  *result, const Integer &rows, const Integer &cols);
		bool write_matrix(const int	   *result, const Integer &rows, const Integer &cols);
		bool write_matrix(const long   *result, const Integer &rows, const Integer &cols);
		
		template<class T>
		bool read_matrix(std::vector<T> &result, Integer &rows, Integer &cols)
		{
			if(dims_.size() > 2) {
				std::cerr << dims_.size() << std::endl;
				assert(dims_.size() <= 2);
				return false;
			}
			
			if(dims_.size() == 2) {
				rows = dims_[0];
				cols = dims_[1];
				
			}
			else {
				rows = dims_[0];
				cols = 1;
			}
			
			result.resize(rows * cols);
			get_values(&result[0]);
			return true;
		}
		
		inline NetCDFAttribute * get_attribute(const std::string &name)
		{
			NetCDFAttribute * att = nullptr;
			std::map<std::string, NetCDFAttribute>::iterator it = attributes_.find(name);
			if(it == attributes_.end()) {
				return att;
			}
			
			att = &it->second;
			return att;
		}
	};
	
	class NetCDFReader {
	public:
		NcFile *file_;
		std::map<std::string, NetCDFVariable> variables_;
		std::map<std::string, Integer> dimensions_;
		
		bool open(const Path &path);
		bool close();
		
		NetCDFReader();
		
		~NetCDFReader() { close(); }
		
		friend std::ostream &operator<<(std::ostream &os, const NetCDFReader &reader)
		{
			for(std::map<std::string, NetCDFVariable>::const_iterator it = reader.variables_.begin(); it != reader.variables_.end(); ++it) {
				os << it->first << ": ";
				os << it->second;
			}
			
			return os;
		}
		
		Integer get_dim(const std::string &name) {
			std::map<std::string, Integer>::const_iterator it = dimensions_.find(name);
			if(it == dimensions_.end())
				return 0;
			return it->second;
		}
		
		void get_variables_with_prefix(const std::string &prefix, std::vector<NetCDFVariable *> &vars)
		{
			std::map<std::string, NetCDFVariable>::iterator it = variables_.begin();
			for(; it != variables_.end(); ++it) {
				if(it->first.compare(0, prefix.size(), prefix) == 0) {
					vars.push_back(&it->second);
				}
			}
		}
		
		NetCDFVariable * get_variable(const std::string &name)
		{
			NetCDFVariable * var = nullptr;
			std::map<std::string, NetCDFVariable>::iterator vIt = variables_.find(trim(name));
			if(vIt == variables_.end())
				return var;
			
			var = &vIt->second;
			return var;
		}
		
	private:
		void init();
		
	};
}

#endif //MOONOLITH_NETCDF_READER_H
