#include "moonolith_matrix_io.hpp"

namespace moonolith {

	bool read_matrix_structure(std::istream &is, Integer &rows, Integer &cols)
	{
		std::string buffer;
		
		static const std::string valid_value_separators = "\t ,;";
		static const char line_separator = '\n';

		rows = 0;
		cols = 0;

		bool is_first_line = true;

		while(is.good()) {
			std::getline(is, buffer, line_separator);
			
			std::size_t  found = 0;
			std::size_t  cols_in_line = 0;

			//remove leading white space
			for(; found < buffer.size(); ++found) {
				if(!std::isspace(buffer[found])) {
					break;
				}
			}

			if(found == buffer.size()) continue;

			while(true) {
				found = buffer.find_first_of(valid_value_separators, found + 1);
				
				//empty line
				if(found == std::string::npos) break;


				//FIXME not robust enough for ugly files
				//avoid spurios white spacing
				for(; found < buffer.size(); ++found) {
					if(!isspace(buffer[found])) {
						break;
					}

					// std::cout << "skip" << std::endl;
				}

				// std::cout << found << ": \"" << buffer[found] << "\"";
				
				++cols_in_line;
			}

			// std::cout << "\n";

			//count first entry
			cols_in_line += cols_in_line > 0;

			// std::cout <<  cols_in_line << ": " << buffer << std::endl;

			if(cols_in_line > 0 && is_first_line) {	
				is_first_line = false;
				cols = cols_in_line;
			} 

			if(cols_in_line > 0) {
				assert( cols_in_line == std::size_t(cols) && "number of columns has to be consitent among rows");
			}

			rows += (cols_in_line > 0);
		}

		is.clear();
		is.seekg(0, is.beg);
		return true;
	}
}
