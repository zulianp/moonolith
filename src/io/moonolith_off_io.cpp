#include "moonolith_off_io.hpp"
#include "moonolith_string.hpp"

#include <fstream>
#include <string>
#include <iostream>

namespace moonolith {
	bool read_off_mesh(std::istream &is, Mesh &mesh)
	{
		mesh.clear();

		std::string line;

		Integer index=0;
		Integer vertex_index=0;
		Integer face_index=0;

		Integer vertex_count=-1;
		Integer face_count=-1;

		std::vector<Integer> face;

		std::vector< std::string > components;


		while ( is.good() ){
			std::getline (is,line);
			line = trim(line);

			if (line.length() == 0 || line[0] == '#' || line[0]=='\r')
				continue;

			if(index==0)
				assert(line.length()>=3 && line[0]=='O' && line[1]=='F' && line[2]=='F');
			else if(index==1) 
			{
				split(line,' ', components);
				if(components.size()<=1)
					split(line,'\t', components);

				assert(components.size()==3);

				vertex_count=atoi(components[0].c_str());
				face_count=atoi(components[1].c_str());
				//components[2] is the number of edges, ignored
			}
			else
			{
				split(line, ' ', components);

				if(components.size()<=1)
					split(line,'\t', components);

				if(vertex_index<vertex_count)
				{
					assert(components.size()==3);
					mesh.add_node(
						atof(components[0].c_str()),
						atof(components[1].c_str()),
						atof(components[2].c_str())
						);
					++vertex_index;
				}
				else if(face_index<face_count)
				{
					assert(components.size()>=4);

					const Integer face_size = atoi(components[0].c_str());

					face.resize(face_size);

					for(Integer i=0; i<face_size; ++i)
						face[i] = atoi(components[i+1].c_str());


					ElemType type = ElemType::POLYGON;
					switch(face.size()) {
						case 3: type = ElemType::TRI3; break;
						case 4: type = ElemType::QUAD4; break;
						default: break;
					}

					mesh.add_element(face, type);
					++face_index;
				}
			}

			++index;
		}

		mesh.set_uniform_elem_type_flags();
		return true;
	}


	bool read_off_mesh(const Path &path, Mesh &mesh)
	{
		std::ifstream is;
		is.open(path.c_str());

		if(!is.good()) {
			is.close();
			std::cerr << "unable to open file at: "  << path << std::endl;
			return false;
		}

		bool ok = read_off_mesh(is,mesh);

		is.close();

		return ok;
	}


	bool write_off_mesh(std::ostream &os, Mesh &mesh)
	{
		os << "OFF\n";
		os << mesh.n_nodes() << " " << mesh.n_elements() << " 0\n";

		os << "\n\n#Vertices\n";
		for(Integer i=0; i < mesh.n_nodes(); ++i)
		{
			for(Integer d=0; d<mesh.dim; ++d)
				os << mesh.points[i*mesh.dim+d] << " ";
			os << "\n";
		}

		os << "\n\n#Faces\n";
		for(Integer i=0; i < mesh.n_elements(); ++i)
		{
			os << mesh.el_ptr[i+1] - mesh.el_ptr[i];
			for(Integer j=mesh.el_ptr[i]; j < mesh.el_ptr[i+1]; ++j)
			{
				const Integer p_index=mesh.el_index[j];
				os << " " << p_index;
			}

			os << "\n";
		}

		return true;
	}

	bool write_off_mesh(const Path &path, Mesh &mesh)
	{
		std::ofstream os;
		os.open(path.c_str());
		if(!os.good()) {
			os.close();
			std::cerr << "Stream not good for .off" << std::endl;
			return false;
		}


		const bool ok = write_off_mesh(os,mesh);

		os.close();

		return ok;
	}

}

