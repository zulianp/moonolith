#include "moonolith_ele_io.hpp"
#include "moonolith_path.hpp"
#include "moonolith_mesh.hpp"

#include <fstream>
#include <sstream>
#include <assert.h>

namespace moonolith {
	
	bool read_ele_mesh_nodes(std::istream &is, Integer &node_offset, Mesh &mesh)
	{
		Integer n_nodes, n_dims, n_attributes, n_boundary_markers;
		std::string buffer;
		
		while (is.good()) {
			std::getline(is, buffer);
			
			std::istringstream ss(buffer);
			
			ss >> n_nodes; 		  	  if(!ss.good()) continue;
			ss >> n_dims; 			  if(!ss.good()) continue;
			ss >> n_attributes;		  if(!ss.good()) continue;
			ss >> n_boundary_markers; if(ss.fail())  continue;
			
			assert( n_dims == 3 );
			break;
		}
		
		mesh.dim = n_dims;
		
		//failed read (no nodes)
		if(n_nodes == 0) return false;
		
		mesh.points.resize(n_nodes * n_dims);
		
		if(n_boundary_markers > 0) {
			mesh.node_sets.resize(n_nodes);
		}
		
		if(n_boundary_markers > 1) {
			std::cerr << "\n[Warning] only using first boundary marker (point set) out of " << n_boundary_markers << std::endl;
		}
		
		Real x, y, z;
		
		Integer index = -1, boundary_marker = 0;
		Integer node_count = 0;
		bool first = true;
		
		node_offset = 0;
		
		while(is.good() && node_count < n_nodes) {
			std::getline(is, buffer);
			
			std::istringstream ss(buffer);
			
			ss >> index; if(!ss.good()) continue;
			ss >> x; 	 if(!ss.good()) continue;
			ss >> y;	 if(!ss.good()) continue;
			ss >> z;	 if(ss.fail())  continue;
			
			if(n_attributes > 0) {
				Integer attribute_count = 0;
				//We just throw away the attributes
				Integer attribute = 0;
				while(ss.good() && attribute_count++ < n_attributes) {
					ss >> attribute;
				}
			}
			
			if(ss.good() && n_boundary_markers > 0) {
				ss >> boundary_marker;
			}
			
			if(first) {
				node_offset = index;
				first = false;
			}
			
			index -= node_offset;
			
			assert(index < n_nodes);
			
			const Integer index_d = index * n_dims;
			
			mesh.points[index_d] 	 = x;
			mesh.points[index_d + 1] = y;
			mesh.points[index_d + 2] = z;
			
			if(n_boundary_markers > 0) {
				mesh.node_sets[index] = boundary_marker;
			}
			
			++node_count;
		}
		
		assert(node_count == n_nodes);
		const bool ok = node_count == n_nodes;
		if(!ok) {
			std::cerr << "\n[Error] node_count not consistent with header." << std::endl;
		}
		
		return ok;
	}
	
	bool read_ele_mesh_elements(std::istream &is, const Integer node_offset, Mesh &mesh)
	{
		std::string buffer;
		Integer n_elements, n_nodes_x_elements, n_attributes;
		
		bool is_hexahedral_mesh = false;
		while (is.good()) {
			std::getline(is, buffer);
			
			std::istringstream ss(buffer);
			
			ss >> n_elements; 		  if(!ss.good()) continue;
			ss >> n_nodes_x_elements; if(!ss.good()) continue;
			ss >> n_attributes;		  if(ss.fail())  continue;
			
			is_hexahedral_mesh = (n_nodes_x_elements == 8);
			
			assert( n_nodes_x_elements == 4 || n_nodes_x_elements == 8 );
			break;
		}
		
		mesh.el_ptr.reserve(n_elements + 1);
		mesh.el_index.reserve(n_elements * n_nodes_x_elements);
		
		Integer element_count = 0;
		
		std::vector<Integer> nodes(n_nodes_x_elements);
		
		while (is.good() && element_count < n_elements) {
			
			std::getline(is, buffer);
			
			std::istringstream ss(buffer);
			
			Integer n_values = 0;
			Integer element_index = 0;
			ss >> element_index;
			
			while(ss.good()) {
				ss >> nodes[n_values++];
				if(ss.fail()) {
					std::cerr << "[Error] in reading element " << element_count << std::endl;
					return false;
				}
				
				if(n_values == n_nodes_x_elements) break;
			}
			
			if(n_values < n_nodes_x_elements) {
				continue;
			}
			
			assert(n_values == 4 || (n_values == 8 && is_hexahedral_mesh) );
			
			for(Integer i = 0; i < n_values; ++i) {
				nodes[i] -= node_offset;
			}
			
			mesh.add_element(nodes, (is_hexahedral_mesh? ElemType::HEX8 : ElemType::TET4));
			++element_count;
		}
		
		mesh.has_uniform_elem_type = true; 
		mesh.uniform_elem_type = (is_hexahedral_mesh? ElemType::HEX8 : ElemType::TET4);
		return n_elements == element_count;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool read_ele_mesh(std::istream &is, Mesh &mesh)
	{
		Integer node_offset = 0;
		return read_ele_mesh_nodes(is, node_offset, mesh) && read_ele_mesh_elements(is, node_offset, mesh);
	}
	
	bool read_ele_mesh(const Path &path, Mesh &mesh)
	{
		const Path no_ext = path.without_extension();
		
		const Path smesh_path = no_ext + ".smesh";
		const Path ele_path   = no_ext + ".ele";
		const Path node_path  = no_ext + ".node";
		
		bool must_read_ele_is  = true;
		bool must_read_node_is = true;
		
		Integer node_offset = 0;
		
		std::ifstream is;
		if(path.extension() == "smesh") {
			is.open(smesh_path);
			
			if(is.good()) {
				must_read_node_is = !read_ele_mesh_nodes(is, node_offset, mesh);
				must_read_ele_is = false;
				
				std::cerr << "\n[Warning] The PLC is not read. Only the nodes are read." << std::endl;
				std::cerr << "[Warning] Future releases will implent this feature." << std::endl;
			} else {
				std::cerr << "[Warning] Unable to read from file at: " << path << std::endl;
				std::cerr << "[Warning] Looking for .ele and .node files ..." << std::endl;
			}
			
			is.close();
		}
		
		if(must_read_node_is) {
			is.open(node_path);
			
			if(!is.good() || !read_ele_mesh_nodes(is, node_offset, mesh)) {
				std::cerr << "[Error] Unable to read from file at: " << node_path << std::endl;
				return false;
			}
			
			is.close();
		}
		
		if(must_read_ele_is) {
			is.open(ele_path);
			
			if(!is.good() || !read_ele_mesh_elements(is, node_offset, mesh)) {
				std::cerr << "[Error] Unable to read from file at: " << ele_path << std::endl;
				return false;
			}
			
			is.close();
		}
		
		return true;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool write_ele_mesh_nodes(std::ostream &os, const Mesh &mesh)
	{
		const Integer n_boundary_markers = !mesh.node_sets.empty();
		const Integer n_nodes = mesh.n_nodes();
		
		os << n_nodes << " " << mesh.dim << " 0 " << n_boundary_markers << "\n";
		
		for(Integer i = 0; i < n_nodes; ++i) {
			os << i << " ";
			
			const Integer i_d = i * mesh.dim;
			
			for(Integer d = 0; d < mesh.dim; ++d) {
				os << mesh.points[i_d + d] << " ";
			}
			
			if(!mesh.node_sets.empty()) {
				os << mesh.node_sets[i];
			}
			
			os << "\n";
		}
		
		return true;
	}
	
	bool write_ele_mesh_elements(std::ostream &os, const Mesh &mesh)
	{
		const Integer n_elements = mesh.n_elements();
		
		if(n_elements == 0) {
			os << "0 0 0\n";
			return true;
		}
		
		const Integer n_nodes_x_element = mesh.n_nodes(0);
		
		os << n_elements << " " << n_nodes_x_element << " 0\n";
		
		for(Integer i = 0; i < n_elements; ++i) {
			assert( n_nodes_x_element == mesh.n_nodes(i) );
			
			os << i << " ";
			
			const Integer * element = mesh.element(i);
			
			if(!element) {
				std::cerr << "[Error] unable to write element " << i << std::endl;
				return false;
			}
			
			for(Integer k = 0; k < n_nodes_x_element; ++k) {
				os << element[k] << " ";
			}
			
			os << "\n";
		}
		
		return true;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool write_ele_mesh(std::ostream &os, const Mesh &mesh)
	{
		return write_ele_mesh_nodes(os, mesh) && write_ele_mesh_elements(os, mesh);
	}
	
	bool write_ele_mesh(const Path &path, const Mesh &mesh)
	{
		const Path no_ext = path.without_extension();
		
		const Path ele_path   = no_ext + ".ele";
		const Path node_path  = no_ext + ".node";
		
		std::ofstream os(node_path);
		if(!os.good() || !write_ele_mesh_nodes(os, mesh)) {
			std::cerr << "\n[Error] Unable to write in file at: " << node_path << std::endl;
			os.close();
			return false;
		}
		os.close();
		
		os.open(ele_path);
		if(!os.good() || !write_ele_mesh_elements(os, mesh)) {
			std::cerr << "\n[Error] Unable to write in file at: " << ele_path << std::endl;
			os.close();
			return false;
		}
		os.close();
		return true;
	}
}
