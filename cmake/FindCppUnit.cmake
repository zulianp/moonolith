# - Find CppUnit
# This module finds an installed CppUnit package.
#
# It sets the following variables:
#  CPPUNIT_FOUND       - Set to false, or undefined, if CppUnit isn't found.
#  CPPUNIT_INCLUDE_DIR - The CppUnit include directory.
#  CPPUNIT_LIBRARY     - The CppUnit library to link against.

find_library(CPPUNIT_LIBRARY  
		NAMES
		cppunit
		HINTS $ENV{CPPUNIT_PACKAGEROOT}/lib
			  /opt/local/lib
			  /usr/local/lib
		DOC "The cppunit library to link against"
	)

get_filename_component(_LIBDIR ${CPPUNIT_LIBRARY} PATH)

find_path(CPPUNIT_INCLUDE_DIR cppunit/Test.h
		HINT $ENV{CPPUNIT_PACKAGEROOT}/include 
			 ${_LIBDIR}/../include
		DOC "The cppunit includes"
	)
	
if(CPPUNIT_INCLUDE_DIR AND CPPUNIT_LIBRARY)
   set(CPPUNIT_FOUND TRUE)
endif()

if(CPPUNIT_FOUND)
	mark_as_advanced(CPPUNIT_INCLUDE_DIR CPPUNIT_LIBRARY)
else()
    message(WARNING "Could not find CppUnit.")
endif ()
