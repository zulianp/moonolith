# - Find MicrosoftUnitTest
# This module finds an installed MicrosoftUnitTest package.
#
# It sets the following variables:
#  MICROSOFT_CPPUNIT_TEST_FOUND       - Set to false, or undefined, if MicrosoftUnitTest isn't found.
#  MICROSOFT_CPPUNIT_TEST_LIBRARY - The MicrosoftUnitTest include directory.
#  MICROSOFT_CPPUNIT_TEST_LIBRARY     - The MicrosoftUnitTest library to link against.

SET(EXPRESS_MICROSOFT_CPPUNIT_HINT_PATH $ENV{VSINSTALLDIR}VC/UnitTest/lib
"$ENV{WINDIR}/../Program Files (x86)/Microsoft Visual Studio 11.0/VC/UnitTest/lib"
)


FIND_LIBRARY(MICROSOFT_CPPUNIT_TEST_LIBRARY  
	Microsoft.VisualStudio.TestTools.CppUnitTestFramework
		HINTS ${EXPRESS_MICROSOFT_CPPUNIT_HINT_PATH}
		DOC "The MicrosoftUnitTest library to link against"
	)

MESSAGE(STATUS ${MICROSOFT_CPPUNIT_TEST_LIBRARY})

GET_FILENAME_COMPONENT(_LIBDIR ${MICROSOFT_CPPUNIT_TEST_LIBRARY} PATH)

FIND_PATH(MICROSOFT_CPPUNIT_TEST_INCLUDE_DIR 
		CppUnitTest.h
		HINT ${EXPRESS_MICROSOFT_CPPUNIT_HINT_PATH}\\include
			 ${_LIBDIR}../../include
		DOC "The MicrosoftUnitTest includes"
	)

MESSAGE(STATUS ${MICROSOFT_CPPUNIT_TEST_INCLUDE_DIR})

	
IF(MICROSOFT_CPPUNIT_TEST_INCLUDE_DIR AND MICROSOFT_CPPUNIT_TEST_LIBRARY)
   SET(MICROSOFT_CPPUNIT_TEST_FOUND TRUE)
ENDIF()

IF(MICROSOFT_CPPUNIT_TEST_FOUND)
   # show which MicrosoftUnitTest was found only if not quiet
	MESSAGE(STATUS "Found MicrosoftUnitTest:\n\tINCLUDE: ${MICROSOFT_CPPUNIT_TEST_INCLUDE_DIR}\n\tLIBRARY: ${MICROSOFT_CPPUNIT_TEST_LIBRARY}")
ELSE()
   # fatal error if MicrosoftUnitTest is required but not found
    MESSAGE(WARNING "Could not find MicrosoftUnitTest")
ENDIF()