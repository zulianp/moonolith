# Features #

## Language ##
* C99 for kernels (math, algorithms)
* C++11 (interfaces)
* (parallel opencl kernels)?

## Design ##
* algorithms are written in C99 style
* C++ Wrappers for usuability

## Utilities ##
* Clock 
* Path
* Command-line handler (arg, opt-arg + launcher)
* Stacktrace
* Testing
* HSV, RGB conversions
* input/output matrix, sparse-matrix (aij) [with matlab correspondent]

## Geometry ##
* math
	* shapes
		* basis-functions (fun, grad)
		* geom-transofrmation (fun, grad)
		* bc (mean-value, max-entropy, harmonic, wachspress, three-point family, metric, blended, composite-maps), 
		* lagr (p0, p1, p2, pk), b-splines, bezier
		* shape utilties

	* (intersections)?	
		* ray-volume
		* volume-volume (sutherland-hodgman clipping, general polygons?)
		* point-volume
	* (coarsening/refinement)?
	* polygon interpolation (sederberg, linear)
	* mesh-generation (box, grid, type 1, closed-surfs)
	* quadrature-rules (points, weights (0, 1), ref-volume)

* classes
	* (broad-phase intersection detection -> octree, spatial-hashing)?
	* bounding-volumes (k-dop, aabb, bounding-sphere) 
	* eps-canvas (polygon, image, ...)
	* triangulator/tetrahedralizer
	* mesh 
	* mesh readers (compressed-storage, exodus, json, obj, off, node, stl, tri, quad, ply, vtk, msh, ugx)
	* mesh writers (node, tri, quad, obj, vtu, ply) [with matlab correspondent]

## Apps ##
* (coarsen/decimate)?
* warping
* meshing (triangle, tetgen)
* convert mesh format (+ mesh2eps, poly2eps)?


## Extra ##
* svg plotter (c++, matlab)